package au.net.causal.peach.integrationtest;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.swing.JFrame;

import org.junit.After;
import org.junit.Before;

import au.net.causal.peach.Desktop;
import au.net.causal.peach.Peach;

public abstract class AbstractDesktopTestCase
{
	private static final Random random = new Random();
		
	private final List<JFrame> createdFrames = Collections.synchronizedList(new ArrayList<>());
	
	private Desktop desktop;
	
	@Before
	public void setUpDesktop()
	{
		desktop = new Peach().createDesktopForPlatform();
	}
	
	public Desktop getDesktop()
	{
		return(desktop);
	}
	
	@After
	public void disposeAnyCreatedFramesAtEndOfTest()
	{
		createdFrames.forEach(JFrame::dispose);
	}
	
	@After
	public void removeAllRegisteredEventListeners()
	{
		desktop.getOn().clear();
	}
	
	protected String generateRandomTitle()
	{
		byte[] bytes = new byte[64];
		random.nextBytes(bytes);
		String randomTitle = Base64.getEncoder().withoutPadding().encodeToString(bytes);
		
		return(randomTitle);
	}
	
	protected JFrame createWindowWithTitle(String title)
	{
		JFrame frame = new JFrame(title);
		createdFrames.add(frame);
		
		frame.setSize(100, 100);
		frame.setVisible(true);
		
		return(frame);
	}
	
	/**
	 * Create a JFrame with a random title.  We do this so we are guaranteed a unique window name which we can find
	 * again without conflicts.  The window will be displayed on screen, visible and in normal state.
	 * 
	 * @return the created window.
	 */
	protected JFrame createWindowWithRandomTitle()
	{
		return(createWindowWithTitle(generateRandomTitle()));
	}
}

