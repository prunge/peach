package au.net.causal.peach.integrationtest;

import java.io.IOError;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.script.Compilable;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import org.junit.Before;

import jdk.nashorn.api.scripting.URLReader;
import au.net.causal.peach.Peach;

@SuppressWarnings("restriction")
public class AbstractJavascriptTestCase extends AbstractDesktopTestCase
{
	private Peach peach = new Peach();
	private ScriptEngine engine;
	
	@Before
	public void setUpEngine()
	{
		engine = peach.createJavascriptEngine();
	}
	
	@SuppressWarnings("unchecked")
	protected <T extends ScriptEngine & Compilable & Invocable> T getEngine()
	{
		return((T)engine);
	}
	
	protected URL readScriptResource(String name)
	{
		URL resource = getClass().getResource(name);
		if (resource == null)
			throw new Error("Missing test resource: " + name);
		
		return(resource);
	}
	
	protected <T> T runScriptFunction(String scriptName, String functionName, Class<T> returnType, Object... parameters)
	{
		Object result = runScriptFunction(scriptName, functionName, parameters);
		return(returnType.cast(result));
	}
	
	protected Object runScriptFunction(String scriptName, String functionName, Object... parameters)
	{
		URL scriptUrl = readScriptResource(scriptName);
		try (URLReader r = new URLReader(scriptUrl, StandardCharsets.UTF_8))
		{
			getEngine().eval(r);
			return(getEngine().invokeFunction(functionName, parameters));
		}
		catch (IOException e)
		{
			throw new IOError(e);
		}
		catch (ScriptException e)
		{
			throw new RuntimeException(e);
		}
		catch (NoSuchMethodException e)
		{
			throw new RuntimeException("Function " + functionName + " not found");
		}
	}
	
	protected void runScript(String name)
	{
		URL scriptUrl = readScriptResource(name);
		
		try (URLReader r = new URLReader(scriptUrl, StandardCharsets.UTF_8))
		{
			getEngine().eval(r);
		}
		catch (IOException e)
		{
			throw new IOError(e);
		}
		catch (ScriptException e)
		{
			throw new RuntimeException(e);
		}
	}
}
