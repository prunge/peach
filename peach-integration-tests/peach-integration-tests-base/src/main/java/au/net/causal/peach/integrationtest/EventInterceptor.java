package au.net.causal.peach.integrationtest;

import java.awt.AWTException;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Robot;
import java.awt.Window;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelListener;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import au.net.causal.peach.Key;
import au.net.causal.peach.KeyPosition;
import au.net.causal.peach.KeyboardControl;
import au.net.causal.peach.MouseControl;
import au.net.causal.peach.event.MouseButton;

import com.google.common.collect.ImmutableSet;

import static org.junit.Assert.*;

public class EventInterceptor
{
	private static final Logger log = Logger.getLogger(EventInterceptor.class.getName());
	
	private static final int DEFAULT_DELAY = 50;
	
	private final Robot robot;
	private MouseControl mouseControl;
	private KeyboardControl keyboardControl;
	
	final private Window eventInterceptionWindow;
	
	private int pendingMouseUps;
	private final Set<Integer> pendingKeyUps = new LinkedHashSet<>();
	
	public final int safeX;
	public final int safeY;
	
	public EventInterceptor()
	throws AWTException
	{
		this.robot = new Robot();
		eventInterceptionWindow = new Frame();
		eventInterceptionWindow.setBounds(200, 200, 500, 500);
		safeX = 300;
		safeY = 350;
	}
	
	/**
	 * Use this instead of the robot, might be necessary on platforms with not great {@link Robot} implementations.
	 * 
	 * @param mouseControl mouse control to set, or use null to revert to using robot.
	 */
	public void setMouseControl(MouseControl mouseControl)
	{
		this.mouseControl = mouseControl;
	}
	
	public void setKeyboardControl(KeyboardControl keyboardControl)
	{
		this.keyboardControl = keyboardControl;
	}
	
	/**
	 * Shows as close as possible to a system-modal window with the specified bounds.  We show this so that any robot clicking wont actually do anything
	 * to other windows in the operating system running the tests.  This window is automatically disposed when the test completes.
	 * 
	 * @return the actual window shown.
	 */
	public Window showEventInterceptionWindow()
	{
		eventInterceptionWindow.setAlwaysOnTop(true);
		
		Runnable r = new Runnable()
		{
			@Override
			public void run()
			{
				eventInterceptionWindow.setVisible(true);
			}
		};
		try
		{
			EventQueue.invokeAndWait(r);
		}
		catch (InterruptedException | InvocationTargetException e)
		{
			throw new RuntimeException(e);
		}
			
		final CountDownLatch latch = new CountDownLatch(1);
		FocusListener focusWaiter = new FocusAdapter()
		{
			@Override
			public void focusGained(FocusEvent e)
			{
				latch.countDown();
			}
		};
		
		eventInterceptionWindow.addFocusListener(focusWaiter);
		try
		{
			latch.await(5, TimeUnit.SECONDS);
		}
		catch (InterruptedException e)
		{
			fail("Interrupted waiting for focus");
		}
		
		//And finally a delay because some platforms say their windows are showing when they really aren't
		try
		{
			Thread.sleep(400L);
		}
		catch (InterruptedException e)
		{
			fail("Interrupted waiting");
		}
		
		return(eventInterceptionWindow);
	}
	
	private Key translateFromKeyEvent(int keyEventCode)
	{
		switch (keyEventCode)
		{
			case KeyEvent.VK_A:
				return new Key(KeyPosition.MAIN, "a", 0x00);
			case KeyEvent.VK_S:
				return new Key(KeyPosition.MAIN, "s", 0x01);
			case KeyEvent.VK_D:
				return new Key(KeyPosition.MAIN, "d", 0x02);
			case KeyEvent.VK_F:
				return new Key(KeyPosition.MAIN, "f", 0x03);
			case KeyEvent.VK_SHIFT:
				return new Key(KeyPosition.LEFT, "shift", 0x38);
			case KeyEvent.VK_CONTROL:
				return new Key(KeyPosition.LEFT, "control", 0x3b);
			default:
				throw new Error("No key event translation for " + keyEventCode);
		}
	}
	
	public void robotKeyDown(int key)
	{
		if (keyboardControl != null)
			keyboardControl.press(translateFromKeyEvent(key));
		else
		{
			robot.waitForIdle();
			robot.delay(DEFAULT_DELAY);
			robot.keyPress(key);
			robot.waitForIdle();
		}
		pendingKeyUps.add(key);
	}
	
	public void robotKeyUp(int key)
	{
		if (keyboardControl != null)
			keyboardControl.release(translateFromKeyEvent(key));
		else
		{
			robot.waitForIdle();
			robot.delay(DEFAULT_DELAY);
			robot.keyRelease(key);
			robot.waitForIdle();
		}
		pendingKeyUps.remove(key);
	}
	
	public void robotKeyType(int key)
	{
		robot.delay(DEFAULT_DELAY);
		robotKeyDown(key);
		robotKeyUp(key);
	}
	
	public void robotClickSafe(int buttons)
	{
		robotClick(safeX, safeY, buttons);
	}
	
	public void robotClick(int x, int y, int buttons)
	{
		if (mouseControl != null)
		{
			mouseControl.move(x, y);
			if ((InputEvent.getMaskForButton(1) & buttons) != 0)
				mouseControl.click(MouseButton.LEFT);
			if ((InputEvent.getMaskForButton(2) & buttons) != 0)
				mouseControl.click(MouseButton.MIDDLE);
			if ((InputEvent.getMaskForButton(3) & buttons) != 0)
				mouseControl.click(MouseButton.RIGHT);
			if ((InputEvent.getMaskForButton(4) & buttons) != 0)
				mouseControl.click(new MouseButton(4));
			if ((InputEvent.getMaskForButton(5) & buttons) != 0)
				mouseControl.click(new MouseButton(5));
		}
		else
		{
			robot.waitForIdle();
			robot.delay(DEFAULT_DELAY);
			robot.mouseMove(x, y);
			robot.mousePress(buttons);
			robot.delay(DEFAULT_DELAY);
			robot.mouseRelease(buttons);
			robot.waitForIdle();
		}
		
		pendingMouseUps = pendingMouseUps & ~buttons;
	}
	
	public void robotMouseDownSafe(int buttons)
	{
		robotMouseDown(safeX, safeY, buttons);
	}
	
	public void robotMouseDown(int x, int y, int buttons)
	{
		if (mouseControl != null)
		{
			mouseControl.move(x, y);
			if ((InputEvent.getMaskForButton(1) & buttons) != 0)
				mouseControl.buttonDown(MouseButton.LEFT);
			if ((InputEvent.getMaskForButton(2) & buttons) != 0)
				mouseControl.buttonDown(MouseButton.MIDDLE);
			if ((InputEvent.getMaskForButton(3) & buttons) != 0)
				mouseControl.buttonDown(MouseButton.RIGHT);
			if ((InputEvent.getMaskForButton(4) & buttons) != 0)
				mouseControl.buttonDown(new MouseButton(4));
			if ((InputEvent.getMaskForButton(5) & buttons) != 0)
				mouseControl.buttonDown(new MouseButton(5));
		}
		else
		{
			robot.waitForIdle();
			robot.delay(DEFAULT_DELAY);
			robot.mouseMove(x, y);
			robot.mousePress(buttons);
			robot.delay(DEFAULT_DELAY);
			robot.waitForIdle();
		}
		
		pendingMouseUps = pendingMouseUps | buttons;
	}
	
	public void robotMouseUpSafe(int buttons)
	{
		robotMouseUp(safeX, safeY, buttons);
	}

	public void robotMouseUp(int x, int y, int buttons)
	{
		if (mouseControl != null)
		{
			mouseControl.move(x, y);
			if ((InputEvent.getMaskForButton(1) & buttons) != 0)
				mouseControl.buttonUp(MouseButton.LEFT);
			if ((InputEvent.getMaskForButton(2) & buttons) != 0)
				mouseControl.buttonUp(MouseButton.MIDDLE);
			if ((InputEvent.getMaskForButton(3) & buttons) != 0)
				mouseControl.buttonUp(MouseButton.RIGHT);
			if ((InputEvent.getMaskForButton(4) & buttons) != 0)
				mouseControl.buttonUp(new MouseButton(4));
			if ((InputEvent.getMaskForButton(5) & buttons) != 0)
				mouseControl.buttonUp(new MouseButton(5));
		}
		else
		{
			robot.waitForIdle();
			robot.delay(DEFAULT_DELAY);
			robot.mouseMove(x, y);
			robot.mouseRelease(buttons);
			robot.delay(DEFAULT_DELAY);
			robot.waitForIdle();
		}
		
		pendingMouseUps = pendingMouseUps & ~buttons;
	}
	
	public void robotMouseWheel(int x, int y, int amount)
	{
		if (mouseControl != null)
		{
			mouseControl.move(x, y);
			mouseControl.scrollWheel(amount);
		}
		else
		{
			robot.waitForIdle();
			robot.delay(DEFAULT_DELAY);
			robot.mouseMove(x, y);
			robot.mouseWheel(amount);
			robot.delay(DEFAULT_DELAY);
			robot.waitForIdle();			
		}
	}
	
	public void robotMouseWheelSafe(int amount)
	{
		robotMouseWheel(safeX, safeY, amount);
	}
	
	public void addMouseListener(MouseListener listener)
	{
		eventInterceptionWindow.addMouseListener(listener);
	}
	
	public void addMouseWheelListener(MouseWheelListener listener)
	{
		eventInterceptionWindow.addMouseWheelListener(listener);
	}
	
	public void addKeyListener(KeyListener listener)
	{
		eventInterceptionWindow.addKeyListener(listener);
	}
	
	public void focus()
	{
		eventInterceptionWindow.toFront();
		try
		{
			Thread.sleep(100L);
		}
		catch (InterruptedException e)
		{
			
		}
	}
	
	public void close()
	{
		if (pendingMouseUps != 0)
		{
			log.fine("Clearing pending mouse ups: " + pendingMouseUps);
			robotMouseUp(safeX, safeY, pendingMouseUps);
		}
		
		if (!pendingKeyUps.isEmpty())
			log.fine("Clearing pending key ups: " + pendingKeyUps);
		
		for (Integer pendingKeyUp : ImmutableSet.copyOf(pendingKeyUps))
		{
			robotKeyUp(pendingKeyUp);
		}
		pendingKeyUps.clear();
		
		cleanUpEventInterceptionWindow();
	}
	
	private void cleanUpEventInterceptionWindow()
	{
		eventInterceptionWindow.dispose();
	}
}

