package au.net.causal.peach.integrationtest;

import org.junit.internal.AssumptionViolatedException;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import com.google.common.base.StandardSystemProperty;

public class IgnoreTestsOnMacRule implements TestRule
{
	@Override
	public Statement apply(Statement base, Description description)
	{
		if (StandardSystemProperty.OS_NAME.value().equalsIgnoreCase("Mac OS X"))
		{
			return(new IgnoreStatement());
		}
		
		return base;
	}
	
	private static class IgnoreStatement extends Statement
	{
		@Override
		public void evaluate() throws Throwable
		{
			throw new AssumptionViolatedException("Running on Mac platform.");
		}
	}

}
