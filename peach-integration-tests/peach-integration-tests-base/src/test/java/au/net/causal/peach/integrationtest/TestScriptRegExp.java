package au.net.causal.peach.integrationtest;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests that Nashorn regexp works with scripts.
 */
public class TestScriptRegExp extends AbstractJavascriptTestCase
{
	/**
	 * Simple regexp test.
	 */
	@Test
	public void testRegExp()
	{
		createWindowWithTitle("My window - java75galah82 - hello");
		String result = runScriptFunction("regexp/regexp1.js", "test", String.class);
		assertEquals("Wrong title.", "My window - java75galah82 - hello", result);
	}

	/**
	 * Tests that the regexp case insensitity option in the JS source file takes effect.
	 */
	@Test
	public void testRegExpCaseInsensitive()
	{
		createWindowWithTitle("My window - Java75Galah82 - hello");
		String result = runScriptFunction("regexp/regexp1.js", "test", String.class);
		assertEquals("Wrong title.", "My window - Java75Galah82 - hello", result);
	}
	
	/**
	 * Tests that the regexp case insensitity option in the JS source file takes effect.
	 */
	@Test
	public void testRegExpCaseSensitiveNotFind()
	{
		createWindowWithTitle("My window - Java75Galah82 - hello");
		String result = runScriptFunction("regexp/regexp2.js", "test", String.class);
		assertNull("Should not find.", result);
	}
	
	/**
	 * Tests that the regexp case insensitity option in the JS source file takes effect.
	 */
	@Test
	public void testRegExpCaseSensitiveFind()
	{
		createWindowWithTitle("My window - java75galah82 - hello");
		String result = runScriptFunction("regexp/regexp2.js", "test", String.class);
		assertEquals("Wrong title.", "My window - java75galah82 - hello", result);
	}
}
