package au.net.causal.peach.integrationtest.common;

import java.awt.Rectangle;

import javax.swing.JFrame;

import org.junit.Test;

import au.net.causal.peach.NativeWindow;
import au.net.causal.peach.integrationtest.AbstractDesktopTestCase;

import static org.junit.Assert.*;

/**
 * Tests retrieving window bounds.
 */
public class TestDesktopWindowBounds extends AbstractDesktopTestCase
{
	@Test
	public void testGetBounds()
	throws Exception
	{
		JFrame frame = createWindowWithRandomTitle();
		frame.setBounds(100, 150, 200, 300);
		
		NativeWindow window = getDesktop().findWindowWithTitle(frame.getTitle());
		assertEquals("Wrong bounds.", 
				new Rectangle(100, 150, 200, 300),
				window.getBounds());
	}
}
