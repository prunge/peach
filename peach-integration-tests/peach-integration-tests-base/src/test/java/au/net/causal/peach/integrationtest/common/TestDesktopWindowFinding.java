package au.net.causal.peach.integrationtest.common;

import java.util.List;

import javax.swing.JFrame;

import org.junit.Test;

import au.net.causal.peach.NativeWindow;
import au.net.causal.peach.integrationtest.AbstractDesktopTestCase;

import static org.junit.Assert.*;

/**
 * Tests finding windows on the desktop.
 * <p>
 * 
 * These tests basically create JFrames with randomized but known titles (to prevent conflict with existing windows) and 
 * then uses the native API to find them.
 */
public class TestDesktopWindowFinding extends AbstractDesktopTestCase
{
	@Test
	public void testFindWindowsWithTitle()
	throws Exception
	{
		JFrame frame = createWindowWithRandomTitle();
		List<? extends NativeWindow> nativeWindows = getDesktop().findWindowsWithTitle(frame.getTitle());
		assertEquals("Wrong window count.", 1, nativeWindows.size());
		NativeWindow window = nativeWindows.get(0);
		assertEquals("Wrong title.", frame.getTitle(), window.getTitle());
		
	}
	
	@Test
	public void testFindWindowWithTitle()
	throws Exception
	{
		JFrame frame = createWindowWithRandomTitle();
		NativeWindow window = getDesktop().findWindowWithTitle(frame.getTitle());
		assertEquals("Wrong title.", frame.getTitle(), window.getTitle());
	}
}
