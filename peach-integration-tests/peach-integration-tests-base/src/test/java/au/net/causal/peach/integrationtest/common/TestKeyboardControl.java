package au.net.causal.peach.integrationtest.common;

import java.awt.AWTException;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import au.net.causal.peach.Keyboard;
import au.net.causal.peach.KeyboardControl;
import au.net.causal.peach.integrationtest.AbstractDesktopTestCase;
import au.net.causal.peach.integrationtest.EventInterceptor;

import com.google.common.collect.ImmutableList;

import static org.junit.Assert.*;

public class TestKeyboardControl extends AbstractDesktopTestCase
{
	private EventInterceptor eventInterceptor;
	private Keyboard keyboard;
	private KeyboardControl control;
	
	@Before
	public void setUp()
	throws AWTException
	{
		eventInterceptor = new EventInterceptor();
		eventInterceptor.showEventInterceptionWindow();
		keyboard = getDesktop().getKeyboard();
		control = keyboard.getControl();
	}
	
	@After
	public void tearDown()
	{
		eventInterceptor.close();
	}
	
	@Test
	public void testTypeSingleKey()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		
		final List<java.awt.event.KeyEvent> awtEventList = new ArrayList<>();
		eventInterceptor.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(java.awt.event.KeyEvent e)
			{
				System.err.println("Key pressed: " + e);
				
				awtEventList.add(e);
				latch.countDown();
			}
			
			@Override
			public void keyReleased(KeyEvent e)
			{
				System.err.println("Key released: " + e);
			}
		});
		
		control.type(keyboard.getAny("shift"));
		
		//AWT window should have received event
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("AWT event not sent.", 1, awtEventList.size());
		assertEquals("Wrong key.", java.awt.event.KeyEvent.VK_SHIFT, awtEventList.get(0).getKeyCode());
	}
	
	@Test
	public void testTypeCombo()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(4);
		
		final List<java.awt.event.KeyEvent> awtEventList = new ArrayList<>();
		eventInterceptor.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(java.awt.event.KeyEvent e)
			{
				System.err.println(e.getModifiersEx());
				awtEventList.add(e);
				latch.countDown();
			}
			
			@Override
			public void keyReleased(KeyEvent e)
			{
				awtEventList.add(e);
				latch.countDown();
			}
		});
		
		control.typeCombo(keyboard.getAny("shift"), keyboard.getAny("1"));
		
		//AWT window should have received event
		latch.await(5, TimeUnit.SECONDS);
		assertEquals("Wrong keys.", 
				ImmutableList.of(KeyEvent.VK_SHIFT, KeyEvent.VK_1, KeyEvent.VK_1, KeyEvent.VK_SHIFT),
				awtEventList.stream().map(event -> event.getKeyCode()).collect(Collectors.toList()));

	}
	
	@Test
	public void testTypeSequence()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(4);
		
		final List<java.awt.event.KeyEvent> awtEventList = new ArrayList<>();
		eventInterceptor.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(java.awt.event.KeyEvent e)
			{
				awtEventList.add(e);
				latch.countDown();
			}
			
			@Override
			public void keyReleased(KeyEvent e)
			{
				awtEventList.add(e);
				latch.countDown();
			}
		});
		
		control.typeSequence(keyboard.getAny("1"), keyboard.getAny("2"));
		
		//AWT window should have received event
		latch.await(5, TimeUnit.SECONDS);
		assertEquals("Wrong keys.", 
				ImmutableList.of(KeyEvent.VK_1, KeyEvent.VK_1, KeyEvent.VK_2, KeyEvent.VK_2),
				awtEventList.stream().map(event -> event.getKeyCode()).collect(Collectors.toList()));
	}
}
