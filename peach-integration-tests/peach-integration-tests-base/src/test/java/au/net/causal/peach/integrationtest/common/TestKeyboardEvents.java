package au.net.causal.peach.integrationtest.common;

import java.awt.AWTException;
import java.awt.event.KeyAdapter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import au.net.causal.peach.DesktopEvents;
import au.net.causal.peach.Key;
import au.net.causal.peach.event.KeyEvent;
import au.net.causal.peach.integrationtest.AbstractDesktopTestCase;
import au.net.causal.peach.integrationtest.EventInterceptor;

import com.google.common.base.StandardSystemProperty;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestKeyboardEvents extends AbstractDesktopTestCase
{
	private DesktopEvents on;
	private volatile EventInterceptor eventInterceptor;
	
	@Before
	public void setUp()
	throws AWTException
	{
		on = getDesktop().getOn();
		eventInterceptor = new EventInterceptor();
		
		//Robot doesn't give back events to event taps, so need to do this on Mac platform
		if (StandardSystemProperty.OS_NAME.value().equalsIgnoreCase("Mac OS X"))
			eventInterceptor.setKeyboardControl(getDesktop().getKeyboard().getControl());
		
		eventInterceptor.showEventInterceptionWindow();
	}
	
	@After
	public void tearDown()
	{
		eventInterceptor.close();
	}
	
	@Test
	public void testKeyPress()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		
		List<KeyEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.keyDown(ev -> {eventList.add(ev); latch.countDown();});
		
		eventInterceptor.robotKeyDown(java.awt.event.KeyEvent.VK_A);
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 1, eventList.size());
		KeyEvent event = eventList.get(0);
		
		Key key = event.getKey();
		assertEquals("Wrong key.", "a", key.getName());
	}
	
	@Test
	public void testKeyRelease()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		
		List<KeyEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.keyUp(ev -> {eventList.add(ev); latch.countDown();});
		
		eventInterceptor.robotKeyType(java.awt.event.KeyEvent.VK_A);
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 1, eventList.size());
		KeyEvent event = eventList.get(0);
		
		Key key = event.getKey();
		assertEquals("Wrong key.", "a", key.getName());
	}
	
	@Test
	public void test0EventIsPassedThroughWhenNotConsumed()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		CountDownLatch awtLatch = new CountDownLatch(1);
		
		final List<java.awt.event.KeyEvent> awtEventList = Collections.synchronizedList(new ArrayList<>());
		eventInterceptor.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(java.awt.event.KeyEvent e)
			{
				awtEventList.add(e);
				awtLatch.countDown();
			}
		});
		
		List<KeyEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.keyDown(ev -> {eventList.add(ev); latch.countDown();});

		eventInterceptor.focus();
		
		eventInterceptor.robotKeyDown(java.awt.event.KeyEvent.VK_A);
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 1, eventList.size());
		KeyEvent event = eventList.get(0);
		
		Key key = event.getKey();
		assertEquals("Wrong key.", "a", key.getName());
		
		//Now make sure that AWT window actually got the event
		awtLatch.await(5, TimeUnit.SECONDS);
		assertEquals("AWT event not sent.", 1, awtEventList.size());
		assertEquals("Wrong key.", java.awt.event.KeyEvent.VK_A, awtEventList.get(0).getKeyCode());
	}
	
	@Test
	public void testEventConsumption()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		CountDownLatch awtLatch = new CountDownLatch(1);
		
		final List<java.awt.event.KeyEvent> awtEventList = Collections.synchronizedList(new ArrayList<>());
		eventInterceptor.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(java.awt.event.KeyEvent e)
			{
				awtEventList.add(e);
				awtLatch.countDown();
			}
		});
		
		List<KeyEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.keyDown(ev -> {ev.consume(); eventList.add(ev); latch.countDown();});
		on.keyUp(ev -> {ev.consume(); });
		
		eventInterceptor.robotKeyDown(java.awt.event.KeyEvent.VK_A);
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 1, eventList.size());
		KeyEvent event = eventList.get(0);
		
		Key key = event.getKey();
		assertEquals("Wrong key.", "a", key.getName());
		
		//Now make sure that AWT window actually got the event
		awtLatch.await(1, TimeUnit.SECONDS);
		assertEquals("AWT event not sent.", 0, awtEventList.size());
	}
	
	/**
	 * Lots of events in a row.  Had some issues with this before.
	 *  
	 * @throws InterruptedException if an error occurs.
	 */
	@Test
	public void multiAwtTest()
	throws InterruptedException
	{
		for (int i = 0; i < 16; i++)
		{
			//Thread.sleep(1000L);
			System.err.println("** Multi AWT iteration " + (i + 1));
			
			final CountDownLatch latch = new CountDownLatch(1);
			final CountDownLatch awtLatch = new CountDownLatch(1);
			
			final List<java.awt.event.KeyEvent> awtEventList = Collections.synchronizedList(new ArrayList<>());
			eventInterceptor.addKeyListener(new KeyAdapter()
			{
				@Override
				public void keyPressed(java.awt.event.KeyEvent e)
				{
					System.err.println("Got key press: " + e);
					awtEventList.add(e);
					awtLatch.countDown();
				}
			});
			
			List<KeyEvent> eventList = Collections.synchronizedList(new ArrayList<>());
			on.keyDown(ev -> {eventList.add(ev); latch.countDown();});
	
			eventInterceptor.focus();
			
			eventInterceptor.robotKeyType(java.awt.event.KeyEvent.VK_S);
			boolean got = latch.await(5, TimeUnit.SECONDS);
			System.err.println("Latch Got: " + got);
			
			assertEquals("Event was not registered.", 1, eventList.size());
			KeyEvent event = eventList.get(0);
			
			Key key = event.getKey();
			assertEquals("Wrong key.", "s", key.getName());
			
			//Now make sure that AWT window actually got the event
			got = awtLatch.await(5, TimeUnit.SECONDS);
			System.err.println("AWT latch got: " + got);
			assertEquals("AWT event not sent.", 1, awtEventList.size());
			assertEquals("Wrong key.", java.awt.event.KeyEvent.VK_S, awtEventList.get(0).getKeyCode());
			
			System.out.println("Iteration " + (i+1) + " successful!");
		}
	}
}
