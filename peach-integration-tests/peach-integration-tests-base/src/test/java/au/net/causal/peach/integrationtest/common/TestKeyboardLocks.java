package au.net.causal.peach.integrationtest.common;

import java.awt.AWTException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import au.net.causal.peach.Keyboard;
import au.net.causal.peach.KeyboardLocks;
import au.net.causal.peach.integrationtest.AbstractDesktopTestCase;
import au.net.causal.peach.integrationtest.IgnoreTestsOnMacRule;

import static org.junit.Assert.*;

public class TestKeyboardLocks extends AbstractDesktopTestCase
{
	private Keyboard keyboard;
	private KeyboardLocks locks;
	
	private boolean num;
	private boolean caps;
	private boolean scroll;
	private boolean kana;
	
	@Rule
	public final IgnoreTestsOnMacRule ignoreTestsOnMac = new IgnoreTestsOnMacRule();
	
	@Before
	public void setUp()
	throws AWTException
	{
		keyboard = getDesktop().getKeyboard();
		locks = keyboard.getLocks();
		
		num = locks.isNumLock();
		caps = locks.isCapsLock();
		scroll = locks.isScrollLock();
		kana = locks.isKanaLock();
	}
	
	@After
	public void restoreKeyboardState()
	{
		locks.setNumLock(num);
		locks.setCapsLock(caps);
		locks.setScrollLock(scroll);
		locks.setKanaLock(kana);
	}
	
	@Test
	public void testScrollLockState()
	{
		locks.setScrollLock(false);
		assertFalse("Scroll lock should be off.", locks.isScrollLock());
		locks.toggleScrollLock();
		assertTrue("Scroll lock should be on.", locks.isScrollLock());
		locks.setScrollLock(false);
		assertFalse("Scroll lock should be off.", locks.isScrollLock());
	}
	
	@Test
	public void testNumLockState()
	{
		locks.setNumLock(false);
		assertFalse("Num lock should be off.", locks.isNumLock());
		locks.toggleNumLock();
		assertTrue("Num lock should be on.", locks.isNumLock());
		locks.setNumLock(false);
		assertFalse("Num lock should be off.", locks.isNumLock());
	}
	
	@Test
	@Ignore("Some people like myself have utilities that keep caps lock off, which will cause this test to fail - might be better to not test this")
	public void testCapsLockState()
	{
		locks.setCapsLock(false);
		assertFalse("Caps lock should be off.", locks.isCapsLock());
		locks.toggleCapsLock();
		assertTrue("Caps lock should be on.", locks.isCapsLock());
		locks.setCapsLock(false);
		assertFalse("Caps lock should be off.", locks.isCapsLock());
	}
	
	@Test
	public void testKanaLockState()
	{
		locks.setKanaLock(false);
		assertFalse("Kana lock should be off.", locks.isKanaLock());
		locks.toggleKanaLock();
		assertTrue("Kana lock should be on.", locks.isKanaLock());
		locks.setKanaLock(false);
		assertFalse("Kana lock should be off.", locks.isKanaLock());
	}
}
