package au.net.causal.peach.integrationtest.common;

import java.awt.AWTException;
import java.awt.event.KeyEvent;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.StandardSystemProperty;

import static org.junit.Assert.*;

import au.net.causal.peach.Keyboard;
import au.net.causal.peach.KeyboardState;
import au.net.causal.peach.integrationtest.AbstractDesktopTestCase;
import au.net.causal.peach.integrationtest.EventInterceptor;

public class TestKeyboardState extends AbstractDesktopTestCase
{
	private EventInterceptor eventInterceptor;
	private Keyboard keyboard;
	
	@Before
	public void setUp()
	throws AWTException
	{
		eventInterceptor = new EventInterceptor();
		eventInterceptor.showEventInterceptionWindow();
		keyboard = getDesktop().getKeyboard();
		
		//Robot doesn't give back events to event taps, so need to do this on Mac platform
		if (StandardSystemProperty.OS_NAME.value().equalsIgnoreCase("Mac OS X"))
			eventInterceptor.setKeyboardControl(keyboard.getControl());
	}
	
	@After
	public void tearDown()
	{
		eventInterceptor.close();
	}
	
	/**
	 * Shift key pressed is getting picked up by keyboard state.
	 */
	@Test
	public void testShiftPress()
	{
		eventInterceptor.robotKeyDown(KeyEvent.VK_SHIFT);
		KeyboardState state = keyboard.getCurrentState();
		
		assertTrue("Shift not pressed.", state.areAnyDown(keyboard.getAll("shift")));
		assertFalse("Control should not be pressed.", state.areAnyDown(keyboard.getAll("control")));
		assertEquals("Wrong key count.", 1, state.getKeysDown().size());
	}
	
	/**
	 * Control key pressed is getting picked up by keyboard state.
	 */
	@Test
	public void testControlPress()
	{
		eventInterceptor.robotKeyDown(KeyEvent.VK_CONTROL);
		KeyboardState state = keyboard.getCurrentState();
		
		assertFalse("Shift should not be pressed.", state.areAnyDown(keyboard.getAll("shift")));
		assertTrue("Control should be pressed.", state.areAnyDown(keyboard.getAll("control")));
		assertEquals("Wrong key count: " + state.getKeysDown(), 1, state.getKeysDown().size());
	}
	
	/**
	 * Shift and control keys pressed are getting picked up by keyboard state.
	 */
	@Test
	public void testMultiPress()
	{
		eventInterceptor.robotKeyDown(KeyEvent.VK_SHIFT);
		eventInterceptor.robotKeyDown(KeyEvent.VK_CONTROL);
		KeyboardState state = keyboard.getCurrentState();
		
		assertTrue("Shift not pressed.", state.areAnyDown(keyboard.getAll("shift")));
		assertTrue("Control not pressed.", state.areAnyDown(keyboard.getAll("control")));
		assertEquals("Wrong key count.", 2, state.getKeysDown().size());
	}
	
	/**
	 * No key is pressed.
	 */
	@Test
	public void testNoPress()
	{
		KeyboardState state = keyboard.getCurrentState();
		assertEquals("Wrong key count.", 0, state.getKeysDown().size());
	}

}
