package au.net.causal.peach.integrationtest.common;

import java.awt.AWTException;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import au.net.causal.peach.MouseControl;
import au.net.causal.peach.event.MouseButton;
import au.net.causal.peach.event.MouseWheelDirection;
import au.net.causal.peach.integrationtest.AbstractDesktopTestCase;
import au.net.causal.peach.integrationtest.EventInterceptor;

import static org.junit.Assert.*;

public class TestMouseControl extends AbstractDesktopTestCase
{
	private EventInterceptor eventInterceptor;
	private MouseControl control;
	
	private int safeX;
	private int safeY;
	
	@Before
	public void setUp()
	throws AWTException, InterruptedException
	{
		eventInterceptor = new EventInterceptor();
		eventInterceptor.showEventInterceptionWindow();		
		control = getDesktop().getMouse().getControl();
		safeX = eventInterceptor.safeX;
		safeY = eventInterceptor.safeY;
		
		eventInterceptor.robotClickSafe(InputEvent.BUTTON2_DOWN_MASK);
		Thread.sleep(200L);
		eventInterceptor.robotClickSafe(InputEvent.BUTTON3_DOWN_MASK);
		Thread.sleep(200L);
		eventInterceptor.robotClickSafe(InputEvent.BUTTON1_DOWN_MASK);
		Thread.sleep(200L);
	}
	
	@After
	public void tearDown()
	{
		eventInterceptor.close();
	}
	
	@Test
	public void testLeftClick()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		
		final List<MouseEvent> awtEventList = new ArrayList<>();
		eventInterceptor.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mousePressed(MouseEvent e)
			{
				awtEventList.add(e);
				latch.countDown();
			}
		});
		
		control.move(safeX, safeY);
		control.click(MouseButton.LEFT);
		
		//AWT window should have received event
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("AWT event not sent.", 1, awtEventList.size());
		assertEquals("Wrong key.", MouseEvent.BUTTON1, awtEventList.get(0).getButton());
	}
	
	@Test
	public void testRightClick()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		
		final List<MouseEvent> awtEventList = new ArrayList<>();
		eventInterceptor.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mousePressed(MouseEvent e)
			{
				awtEventList.add(e);
				latch.countDown();
			}
		});
		
		control.move(safeX, safeY);
		control.click(MouseButton.RIGHT);
		
		//AWT window should have received event
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("AWT event not sent.", 1, awtEventList.size());
		assertEquals("Wrong key.", MouseEvent.BUTTON3, awtEventList.get(0).getButton());
	}
	
	@Test
	public void testWheelDown()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		
		final List<MouseWheelEvent> awtEventList = new ArrayList<>();
		eventInterceptor.addMouseWheelListener(new MouseAdapter()
		{
			@Override
			public void mouseWheelMoved(MouseWheelEvent e)
			{
				awtEventList.add(e);
				latch.countDown();
			}
		});
		
		control.move(safeX, safeY);
		control.scrollWheel(MouseWheelDirection.DOWN, 1);
		
		//AWT window should have received event
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("AWT event not sent.", 1, awtEventList.size());
		assertEquals("Wrong amount.", 1, awtEventList.get(0).getWheelRotation());
	}
	
	@Test
	public void testWheelUp()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		
		final List<MouseWheelEvent> awtEventList = new ArrayList<>();
		eventInterceptor.addMouseWheelListener(new MouseAdapter()
		{
			@Override
			public void mouseWheelMoved(MouseWheelEvent e)
			{
				awtEventList.add(e);
				latch.countDown();
			}
		});
		
		control.move(safeX, safeY);
		control.scrollWheel(MouseWheelDirection.UP, 1);
		
		//AWT window should have received event
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("AWT event not sent.", 1, awtEventList.size());
		assertEquals("Wrong amount.", -1, awtEventList.get(0).getWheelRotation());
	}
}
