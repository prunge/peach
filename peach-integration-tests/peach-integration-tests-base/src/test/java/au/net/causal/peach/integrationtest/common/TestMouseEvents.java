package au.net.causal.peach.integrationtest.common;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.base.StandardSystemProperty;

import au.net.causal.peach.DesktopEvents;
import au.net.causal.peach.event.MouseButton;
import au.net.causal.peach.event.MouseButtonEvent;
import au.net.causal.peach.event.MouseEventConstraints;
import au.net.causal.peach.event.MouseWheelDirection;
import au.net.causal.peach.event.MouseWheelEvent;
import au.net.causal.peach.event.MouseWheelEventConstraints;
import au.net.causal.peach.integrationtest.AbstractDesktopTestCase;
import au.net.causal.peach.integrationtest.EventInterceptor;
import static org.hamcrest.CoreMatchers.*;

import static org.junit.Assert.*;

/**
 * Test that listening for mouse events works.
 * <p>
 * 
 * Uses a {@link Robot} to generate events.  It is assumed that Peach's events will hear the robot's.  On Windows this is definitely the case. 
 */
public class TestMouseEvents extends AbstractDesktopTestCase
{
	private DesktopEvents on;
	private EventInterceptor eventInterceptor;
	
	private int safeX;
	private int safeY;
	
	@Before
	public void setUp()
	throws AWTException, InterruptedException
	{
		on = getDesktop().getOn();
		eventInterceptor = new EventInterceptor();
		safeX = eventInterceptor.safeX;
		safeY = eventInterceptor.safeY;
		eventInterceptor.showEventInterceptionWindow();
		
		//Robot doesn't give back events to event taps, so need to do this on Mac platform
		if (StandardSystemProperty.OS_NAME.value().equalsIgnoreCase("Mac OS X"))
			eventInterceptor.setMouseControl(getDesktop().getMouse().getControl());
		
		//Perform a dummy click in case the mouse down state is messed up
		eventInterceptor.robotClickSafe(InputEvent.BUTTON2_DOWN_MASK);
		Thread.sleep(200L);
		eventInterceptor.robotClickSafe(InputEvent.BUTTON3_DOWN_MASK);
		Thread.sleep(200L);
		eventInterceptor.robotClickSafe(InputEvent.getMaskForButton(4));
		Thread.sleep(200L);
		eventInterceptor.robotClickSafe(InputEvent.BUTTON1_DOWN_MASK);
		Thread.sleep(200L);
	}
	
	@After
	public void tearDown()
	throws InterruptedException
	{
		eventInterceptor.close();
	}
	
	@Test
	public void testLeftClick()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		
		List<MouseButtonEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseDown(ev -> {eventList.add(ev); latch.countDown(); });
		
		eventInterceptor.robotClickSafe(InputEvent.BUTTON1_DOWN_MASK);
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseButtonEvent event = eventList.get(0);
		
		assertEquals("Wrong button.", MouseButton.LEFT, event.getButton());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
	}

	@Test
	public void testMiddleClick()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		
		List<MouseButtonEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseDown(ev -> {eventList.add(ev); latch.countDown(); });
		
		eventInterceptor.robotClickSafe(InputEvent.BUTTON2_DOWN_MASK);
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseButtonEvent event = eventList.get(0);
		
		assertEquals("Wrong button.", MouseButton.MIDDLE, event.getButton());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
	}

	@Test
	public void testRightClick()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		
		List<MouseButtonEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseDown(ev -> {eventList.add(ev); latch.countDown(); });
		
		eventInterceptor.robotClickSafe(InputEvent.BUTTON3_DOWN_MASK);
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseButtonEvent event = eventList.get(0);
		
		assertEquals("Wrong button.", MouseButton.RIGHT, event.getButton());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
	}
	
	@Test
	public void testFourthClick()
	throws InterruptedException
	{CountDownLatch latch = new CountDownLatch(1);
		
		List<MouseButtonEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseDown(ev -> {eventList.add(ev); latch.countDown(); });
		
		eventInterceptor.robotClickSafe(InputEvent.getMaskForButton(4));
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseButtonEvent event = eventList.get(0);
		
		assertEquals("Wrong button.", new MouseButton(4), event.getButton());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
	}
	
	@Test
	public void testClick2ButtonsAtSameTime()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(2);
		
		List<MouseButtonEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseDown(ev -> {eventList.add(ev); latch.countDown(); });
		
		eventInterceptor.robotClickSafe(InputEvent.BUTTON1_DOWN_MASK | InputEvent.BUTTON3_DOWN_MASK);
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 2, eventList.size());
		MouseButtonEvent event1 = eventList.get(0);
		MouseButtonEvent event2 = eventList.get(1);
		
		assertThat(event1.getButton(), either(is(MouseButton.LEFT)).or(is(MouseButton.RIGHT)));
		assertThat(event2.getButton(), either(is(MouseButton.LEFT)).or(is(MouseButton.RIGHT)));
		assertEquals("Wrong x.", safeX, event1.getX());
		assertEquals("Wrong y.", safeY, event1.getY());
		assertEquals("Wrong x.", safeX, event2.getX());
		assertEquals("Wrong y.", safeY, event2.getY());
	}
	
	@Test
	public void testLeftMouseDownAndUp()
	throws InterruptedException
	{
		final CountDownLatch latch = new CountDownLatch(1);
		final CountDownLatch latch2 = new CountDownLatch(2);
		
		List<MouseButtonEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseDown(ev -> {eventList.add(ev); latch.countDown(); });
		on.mouseUp(ev -> {eventList.add(ev); latch2.countDown(); });
		
		eventInterceptor.robotMouseDownSafe(InputEvent.BUTTON1_DOWN_MASK);
		latch.await(5, TimeUnit.SECONDS);
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseButtonEvent event = eventList.get(0);
		
		assertEquals("Wrong button.", MouseButton.LEFT, event.getButton());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
		
		eventList.clear();
		eventInterceptor.robotMouseUpSafe(InputEvent.BUTTON1_DOWN_MASK);
		latch2.await(5, TimeUnit.SECONDS);
		assertEquals("Event was not registered.", 1, eventList.size());
		event = eventList.get(0);
		
		assertEquals("Wrong button.", MouseButton.LEFT, event.getButton());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
	}
	
	@Test
	public void testMiddleMouseDownAndUp()
	throws InterruptedException
	{
		final CountDownLatch latch = new CountDownLatch(1);
		final CountDownLatch latch2 = new CountDownLatch(1);
		
		List<MouseButtonEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseDown(ev -> {eventList.add(ev); latch.countDown(); });
		on.mouseUp(ev -> {eventList.add(ev); latch2.countDown(); });
		
		eventInterceptor.robotMouseDownSafe(InputEvent.BUTTON2_DOWN_MASK);
		latch.await(5, TimeUnit.SECONDS);
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseButtonEvent event = eventList.get(0);
		
		assertEquals("Wrong button.", MouseButton.MIDDLE, event.getButton());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
		
		eventList.clear();
		eventInterceptor.robotMouseUpSafe(InputEvent.BUTTON2_DOWN_MASK);
		latch2.await(5, TimeUnit.SECONDS);
		assertEquals("Event was not registered.", 1, eventList.size());
		event = eventList.get(0);
		
		assertEquals("Wrong button.", MouseButton.MIDDLE, event.getButton());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
	}
	
	@Test
	public void testRightMouseDownAndUp()
	throws InterruptedException
	{
		final CountDownLatch latch = new CountDownLatch(1);
		final CountDownLatch latch2 = new CountDownLatch(1);
		
		List<MouseButtonEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseDown(ev -> {eventList.add(ev); latch.countDown(); });
		on.mouseUp(ev -> {eventList.add(ev); latch2.countDown(); });
		
		eventInterceptor.robotMouseDownSafe(InputEvent.BUTTON3_DOWN_MASK);
		latch.await(5, TimeUnit.SECONDS);
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseButtonEvent event = eventList.get(0);
		
		assertEquals("Wrong button.", MouseButton.RIGHT, event.getButton());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
		
		eventList.clear();
		eventInterceptor.robotMouseUpSafe(InputEvent.BUTTON3_DOWN_MASK);
		latch2.await(5, TimeUnit.SECONDS);
		assertEquals("Event was not registered.", 1, eventList.size());
		event = eventList.get(0);
		
		assertEquals("Wrong button.", MouseButton.RIGHT, event.getButton());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
	}
	
	@Test
	public void testEventConsumptionLeftButton()
	throws InterruptedException
	{
		final CountDownLatch latch = new CountDownLatch(1);
		final CountDownLatch latch2 = new CountDownLatch(1);
		final CountDownLatch awtLatch = new CountDownLatch(1);
		
		final List<MouseEvent> awtEventList = Collections.synchronizedList(new ArrayList<>());
		eventInterceptor.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				awtEventList.add(e);
				awtLatch.countDown();
			}
		});
		
		List<MouseButtonEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseDown(ev -> {ev.consume(); eventList.add(ev); latch.countDown();});
		on.mouseUp(ev -> {ev.consume(); eventList.add(ev); latch2.countDown();});
		
		eventInterceptor.robotMouseDownSafe(InputEvent.BUTTON1_DOWN_MASK);
		latch.await(5, TimeUnit.SECONDS);
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseButtonEvent event = eventList.get(0);
		
		assertEquals("Wrong button.", MouseButton.LEFT, event.getButton());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
		
		eventList.clear();
		eventInterceptor.robotMouseUpSafe(InputEvent.BUTTON1_DOWN_MASK);
		latch2.await(5, TimeUnit.SECONDS);
		assertEquals("Event was not registered.", 1, eventList.size());
		event = eventList.get(0);
		
		assertEquals("Wrong button.", MouseButton.LEFT, event.getButton());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
		
		//Ensure event was consumed - AWT should not have seen event
		awtLatch.await(1, TimeUnit.SECONDS);
		assertEquals("AWT event not sent.", 0, awtEventList.size());
	}
	
	@Test
	public void testLeftButtonConstraint()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		
		List<MouseButtonEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseDown(new MouseEventConstraints(Collections.singleton(MouseButton.LEFT)), ev -> { eventList.add(ev); latch.countDown(); });
		
		eventInterceptor.robotClickSafe(InputEvent.BUTTON1_DOWN_MASK);
		eventInterceptor.robotClickSafe(InputEvent.BUTTON3_DOWN_MASK);
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseButtonEvent event = eventList.get(0);
		
		assertEquals("Wrong button.", MouseButton.LEFT, event.getButton());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
	}
	
	@Test
	public void testMiddleButtonConstraint()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		
		List<MouseButtonEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseDown(new MouseEventConstraints(Collections.singleton(MouseButton.MIDDLE)), ev -> { eventList.add(ev); latch.countDown(); });
		
		eventInterceptor.robotClickSafe(InputEvent.BUTTON1_DOWN_MASK);
		eventInterceptor.robotClickSafe(InputEvent.BUTTON2_DOWN_MASK);
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseButtonEvent event = eventList.get(0);
		
		assertEquals("Wrong button.", MouseButton.MIDDLE, event.getButton());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
	}
	
	@Test
	public void testRightButtonConstraint()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		
		List<MouseButtonEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseDown(new MouseEventConstraints(Collections.singleton(MouseButton.RIGHT)), ev -> { eventList.add(ev); latch.countDown(); });
		
		eventInterceptor.robotClickSafe(InputEvent.BUTTON1_DOWN_MASK);
		eventInterceptor.robotClickSafe(InputEvent.BUTTON3_DOWN_MASK);
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseButtonEvent event = eventList.get(0);
		
		assertEquals("Wrong button.", MouseButton.RIGHT, event.getButton());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
	}
	
	@Test
	public void testMultiButtonConstraint()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(2);
		
		List<MouseButtonEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseDown(new MouseEventConstraints(new HashSet<>(Arrays.asList(MouseButton.LEFT, MouseButton.RIGHT))), ev -> {eventList.add(ev); latch.countDown();});
		
		eventInterceptor.robotClickSafe(InputEvent.BUTTON1_DOWN_MASK);
		eventInterceptor.robotClickSafe(InputEvent.BUTTON2_DOWN_MASK);
		eventInterceptor.robotClickSafe(InputEvent.BUTTON3_DOWN_MASK);
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 2, eventList.size());
		MouseButtonEvent event1 = eventList.get(0);
		MouseButtonEvent event2 = eventList.get(1);
		
		assertEquals("Wrong button.", MouseButton.LEFT, event1.getButton());
		assertEquals("Wrong x.", safeX, event1.getX());
		assertEquals("Wrong y.", safeY, event1.getY());
		assertEquals("Wrong button.", MouseButton.RIGHT, event2.getButton());
		assertEquals("Wrong x.", safeX, event2.getX());
		assertEquals("Wrong y.", safeY, event2.getY());
	}
	
	@Test
	public void test4thButtonConstraint()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		
		List<MouseButtonEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseDown(new MouseEventConstraints(Collections.singleton(new MouseButton(4))), ev -> {eventList.add(ev); latch.countDown();});
		
		eventInterceptor.robotClickSafe(InputEvent.BUTTON1_DOWN_MASK);
		eventInterceptor.robotClickSafe(InputEvent.getMaskForButton(4));
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseButtonEvent event = eventList.get(0);
		
		assertEquals("Wrong button.", new MouseButton(4), event.getButton());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
	}
	
	@Test
	public void testScrollWheelUp()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		
		List<MouseWheelEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseWheelMove(ev -> {eventList.add(ev); latch.countDown();});
		
		eventInterceptor.robotMouseWheelSafe(-1);
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseWheelEvent event = eventList.get(0);
		
		assertEquals("Wrong direction.", MouseWheelDirection.UP, event.getDirection());
		assertEquals("Wrong amount.", -1, event.getSteps());
		assertEquals("Wrong amount.", 1, event.getRelativeSteps());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
	}
	
	@Test
	public void testScrollWheelDown()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		
		List<MouseWheelEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseWheelMove(ev -> {eventList.add(ev); latch.countDown();});
		
		eventInterceptor.robotMouseWheelSafe(1);
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseWheelEvent event = eventList.get(0);
		
		assertEquals("Wrong direction.", MouseWheelDirection.DOWN, event.getDirection());
		assertEquals("Wrong amount.", 1, event.getSteps());
		assertEquals("Wrong amount.", 1, event.getRelativeSteps());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
	}
	
	@Test
	public void testScrollWheelUpFilter()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		
		List<MouseWheelEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseWheelMove(new MouseWheelEventConstraints(MouseWheelDirection.UP), ev -> {eventList.add(ev); latch.countDown();});
		
		eventInterceptor.robotMouseWheelSafe(-1);
		eventInterceptor.robotMouseWheelSafe(1);
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseWheelEvent event = eventList.get(0);
		
		assertEquals("Wrong direction.", MouseWheelDirection.UP, event.getDirection());
		assertEquals("Wrong amount.", -1, event.getSteps());
		assertEquals("Wrong amount.", 1, event.getRelativeSteps());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
	}
	
	@Test
	public void testScrollWheelDownFilter()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		
		List<MouseWheelEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseWheelMove(new MouseWheelEventConstraints(MouseWheelDirection.DOWN), ev -> {eventList.add(ev); latch.countDown();});
		
		eventInterceptor.robotMouseWheelSafe(-1);
		eventInterceptor.robotMouseWheelSafe(1);
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseWheelEvent event = eventList.get(0);
		
		assertEquals("Wrong direction.", MouseWheelDirection.DOWN, event.getDirection());
		assertEquals("Wrong amount.", 1, event.getSteps());
		assertEquals("Wrong amount.", 1, event.getRelativeSteps());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
	}
	
	@Test
	public void testScrollWheelConsume()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		CountDownLatch awtLatch = new CountDownLatch(1);
		
		final List<java.awt.event.MouseWheelEvent> awtEventList = Collections.synchronizedList(new ArrayList<>());
		eventInterceptor.addMouseWheelListener(new MouseAdapter()
		{
			@Override
			public void mouseWheelMoved(java.awt.event.MouseWheelEvent e)
			{
				awtEventList.add(e);
				awtLatch.countDown();
			}
		});
		
		List<MouseWheelEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseWheelMove(ev -> {ev.consume(); eventList.add(ev); latch.countDown();});
		
		eventInterceptor.robotMouseWheelSafe(-1);
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseWheelEvent event = eventList.get(0);
		
		assertEquals("Wrong direction.", MouseWheelDirection.UP, event.getDirection());
		assertEquals("Wrong amount.", -1, event.getSteps());
		assertEquals("Wrong amount.", 1, event.getRelativeSteps());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
		
		//Event should be consumed
		awtLatch.await(1, TimeUnit.SECONDS);
		assertEquals("Event not consumed.", 0, awtEventList.size());
	}
}
