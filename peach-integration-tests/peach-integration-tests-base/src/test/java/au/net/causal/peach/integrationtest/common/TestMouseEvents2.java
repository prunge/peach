package au.net.causal.peach.integrationtest.common;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import au.net.causal.peach.DesktopEvents;
import au.net.causal.peach.event.MouseButton;
import au.net.causal.peach.event.MouseButtonEvent;
import au.net.causal.peach.event.MouseWheelDirection;
import au.net.causal.peach.event.MouseWheelEvent;
import au.net.causal.peach.integrationtest.AbstractDesktopTestCase;
import au.net.causal.peach.integrationtest.EventInterceptor;

import com.google.common.base.StandardSystemProperty;

import static org.junit.Assert.*;

/**
 * Test that listening for mouse events works.
 * <p>
 * 
 * Uses a {@link Robot} to generate events.  It is assumed that Peach's events will hear the robot's.  On Windows this is definitely the case. 
 */
public class TestMouseEvents2 extends AbstractDesktopTestCase
{
	private DesktopEvents on;
	private EventInterceptor eventInterceptor;
	
	private int safeX;
	private int safeY;
	
	@Before
	public void setUp()
	throws AWTException, InterruptedException
	{
		on = getDesktop().getOn();
		eventInterceptor = new EventInterceptor();
		safeX = eventInterceptor.safeX;
		safeY = eventInterceptor.safeY;
		eventInterceptor.showEventInterceptionWindow();
		
		//Robot doesn't give back events to event taps, so need to do this on Mac platform
		if (StandardSystemProperty.OS_NAME.value().equalsIgnoreCase("Mac OS X"))
			eventInterceptor.setMouseControl(getDesktop().getMouse().getControl());
		
		//Perform a dummy click in case the mouse down state is messed up
		eventInterceptor.robotClickSafe(InputEvent.BUTTON2_DOWN_MASK);
		Thread.sleep(200L);
		eventInterceptor.robotClickSafe(InputEvent.BUTTON3_DOWN_MASK);
		Thread.sleep(200L);
		eventInterceptor.robotClickSafe(InputEvent.getMaskForButton(4));
		Thread.sleep(200L);
		eventInterceptor.robotClickSafe(InputEvent.BUTTON1_DOWN_MASK);
		Thread.sleep(200L);
	}
	
	@After
	public void tearDown()
	throws InterruptedException
	{
		eventInterceptor.close();
	}
	
	@Test
	public void testAnEventPassedThroughWhenNotConsumedLeftButton()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(1);
		CountDownLatch latch2 = new CountDownLatch(1);
		CountDownLatch awtLatch = new CountDownLatch(1);
		
		final List<MouseEvent> awtEventList = Collections.synchronizedList(new ArrayList<>());
		eventInterceptor.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				awtEventList.add(e);
				awtLatch.countDown();
			}
		});
		
		List<MouseButtonEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseDown(ev -> {eventList.add(ev); latch.countDown();});
		on.mouseUp(ev -> {eventList.add(ev); latch2.countDown();});
		
		eventInterceptor.robotMouseDownSafe(InputEvent.BUTTON1_DOWN_MASK);
		latch.await(5, TimeUnit.SECONDS);
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseButtonEvent event = eventList.get(0);
		
		assertEquals("Wrong button.", MouseButton.LEFT, event.getButton());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
		
		eventList.clear();
		eventInterceptor.robotMouseUpSafe(InputEvent.BUTTON1_DOWN_MASK);
		latch2.await(5, TimeUnit.SECONDS);
		assertEquals("Event was not registered.", 1, eventList.size());
		event = eventList.get(0);
		
		assertEquals("Wrong button.", MouseButton.LEFT, event.getButton());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
		
		//Now make sure that AWT window actually got the event
		awtLatch.await(5, TimeUnit.SECONDS);
		assertEquals("AWT event not sent.", 1, awtEventList.size());
		assertEquals("Wrong button.", MouseEvent.BUTTON1, awtEventList.get(0).getButton());
	}
	
	@Test
	public void testAScrollWheelPassthroughWhenNotConsumed()
	throws InterruptedException
	{	
		CountDownLatch latch = new CountDownLatch(1);
		CountDownLatch awtLatch = new CountDownLatch(1);
		
		final List<java.awt.event.MouseWheelEvent> awtEventList = Collections.synchronizedList(new ArrayList<>());
		eventInterceptor.addMouseWheelListener(new MouseAdapter()
		{
			@Override
			public void mouseWheelMoved(java.awt.event.MouseWheelEvent e)
			{
				awtEventList.add(e);
				awtLatch.countDown();
			}
		});
		
		List<MouseWheelEvent> eventList = Collections.synchronizedList(new ArrayList<>());
		on.mouseWheelMove(ev -> {eventList.add(ev); latch.countDown();});
		
		eventInterceptor.robotMouseWheelSafe(-1);
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Event was not registered.", 1, eventList.size());
		MouseWheelEvent event = eventList.get(0);
		
		assertEquals("Wrong direction.", MouseWheelDirection.UP, event.getDirection());
		assertEquals("Wrong amount.", -1, event.getSteps());
		assertEquals("Wrong amount.", 1, event.getRelativeSteps());
		assertEquals("Wrong x.", safeX, event.getX());
		assertEquals("Wrong y.", safeY, event.getY());
		
		//Event should be consumed
		awtLatch.await(5, TimeUnit.SECONDS);
		assertEquals("Event should not be consumed.", 1, awtEventList.size());
		assertEquals("Wrong amount.", -1, awtEventList.get(0).getWheelRotation());
	}
}
