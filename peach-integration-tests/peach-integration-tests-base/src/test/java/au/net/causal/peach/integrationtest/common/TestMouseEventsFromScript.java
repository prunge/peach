package au.net.causal.peach.integrationtest.common;

import java.awt.AWTException;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import au.net.causal.peach.integrationtest.AbstractJavascriptTestCase;
import au.net.causal.peach.integrationtest.EventInterceptor;

import static org.junit.Assert.*;

public class TestMouseEventsFromScript extends AbstractJavascriptTestCase
{
	private EventInterceptor eventInterceptor;
	
	@Before
	public void setUp()
	throws AWTException
	{
		eventInterceptor = new EventInterceptor();
		eventInterceptor.setMouseControl(getDesktop().getMouse().getControl());
		eventInterceptor.showEventInterceptionWindow();
	}
	
	@After
	public void tearDown()
	{
		eventInterceptor.close();
	}
	
	@Test
	public void testMouseDown()
	throws InterruptedException
	{
		CountDownLatch latch = new CountDownLatch(2);
		
		AtomicInteger counter = new AtomicInteger();
		runScriptFunction("../mouseevent/mouseevent1.js", "test", Void.class, counter, latch);
		
		eventInterceptor.robotClickSafe(InputEvent.BUTTON1_DOWN_MASK);
		eventInterceptor.robotClickSafe(InputEvent.BUTTON3_DOWN_MASK);
		eventInterceptor.robotClickSafe(InputEvent.BUTTON1_DOWN_MASK);
		eventInterceptor.robotClickSafe(InputEvent.BUTTON2_DOWN_MASK);
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Wrong event count.", 2, counter.intValue());
	}
	
	@Test
	public void testIntercept()
	throws InterruptedException
	{
		runScriptFunction("../mouseevent/mouseevent2.js", "test");
		
		final List<Integer> buttonDownList = new ArrayList<>();
		
		CountDownLatch latch = new CountDownLatch(2);
		
		eventInterceptor.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mousePressed(MouseEvent e)
			{
				buttonDownList.add(e.getButton());
				latch.countDown();
			}
		});
		
		eventInterceptor.robotClickSafe(InputEvent.BUTTON2_DOWN_MASK);
		eventInterceptor.robotClickSafe(InputEvent.BUTTON1_DOWN_MASK);
		eventInterceptor.robotClickSafe(InputEvent.BUTTON3_DOWN_MASK);
		
		latch.await(5, TimeUnit.SECONDS);
		
		assertEquals("Middle clicks not intercepted.", Arrays.asList(1, 3), buttonDownList);
	}

}
;