package au.net.causal.peach.integrationtest.common;

import java.awt.AWTException;
import java.awt.event.InputEvent;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import au.net.causal.peach.Mouse;
import au.net.causal.peach.MouseState;
import au.net.causal.peach.event.MouseButton;
import au.net.causal.peach.integrationtest.AbstractDesktopTestCase;
import au.net.causal.peach.integrationtest.EventInterceptor;

import com.google.common.collect.ImmutableSet;

import static org.junit.Assert.*;

public class TestMouseState extends AbstractDesktopTestCase
{
	private EventInterceptor eventInterceptor;
	private Mouse mouse;
	
	@Before
	public void setUp()
	throws AWTException, InterruptedException
	{
		eventInterceptor = new EventInterceptor();
		eventInterceptor.showEventInterceptionWindow();
		mouse = getDesktop().getMouse();
		
		//Perform a dummy click in case the mouse down state is messed up
		eventInterceptor.robotClickSafe(InputEvent.BUTTON2_DOWN_MASK);
		Thread.sleep(200L);
		eventInterceptor.robotClickSafe(InputEvent.BUTTON3_DOWN_MASK);
		Thread.sleep(200L);
		eventInterceptor.robotClickSafe(InputEvent.getMaskForButton(4));
		Thread.sleep(200L);
		eventInterceptor.robotClickSafe(InputEvent.BUTTON1_DOWN_MASK);
		Thread.sleep(200L);
	}
	
	@After
	public void tearDown()
	{
		eventInterceptor.close();
	}
	
	@Test
	public void testLeftButtonDown()
	{
		eventInterceptor.robotMouseDownSafe(InputEvent.BUTTON1_DOWN_MASK);
		MouseState state = mouse.getCurrentState();
		
		assertTrue("Button not pressed.", state.isDown(MouseButton.LEFT));
		assertFalse("Button should not be pressed.", state.isDown(MouseButton.RIGHT));
		assertEquals("Wrong buttonst.", ImmutableSet.of(MouseButton.LEFT), state.getButtonsDown());
	}
	
	@Test
	public void restRightButtonDown()
	{
		eventInterceptor.robotMouseDownSafe(InputEvent.BUTTON3_DOWN_MASK);
		MouseState state = mouse.getCurrentState();
		
		assertTrue("Button not pressed.", state.isDown(MouseButton.RIGHT));
		assertFalse("Button should not be pressed.", state.isDown(MouseButton.LEFT));
		assertEquals("Wrong buttons.", ImmutableSet.of(MouseButton.RIGHT), state.getButtonsDown());
	}
	
	@Test
	public void testMultiPress()
	{
		eventInterceptor.robotMouseDownSafe(InputEvent.BUTTON1_DOWN_MASK);
		eventInterceptor.robotMouseDownSafe(InputEvent.BUTTON3_DOWN_MASK);
		MouseState state = mouse.getCurrentState();
		
		assertTrue("Button not pressed.", state.isDown(MouseButton.LEFT));
		assertTrue("Button not pressed.", state.isDown(MouseButton.RIGHT));
		assertFalse("Button should not be pressed.", state.isDown(MouseButton.MIDDLE));
		assertEquals("Wrong buttons.", ImmutableSet.of(MouseButton.LEFT, MouseButton.RIGHT), state.getButtonsDown());
	}
	
	@Test
	public void testNoPress()
	{
		MouseState state = mouse.getCurrentState();
		
		assertFalse("Button should not be pressed.", state.isDown(MouseButton.LEFT));
		assertFalse("Button should not be pressed.", state.isDown(MouseButton.RIGHT));
		assertFalse("Button should not be pressed.", state.isDown(MouseButton.MIDDLE));
		assertEquals("Wrong buttons.", ImmutableSet.of(), state.getButtonsDown());
	}
}
