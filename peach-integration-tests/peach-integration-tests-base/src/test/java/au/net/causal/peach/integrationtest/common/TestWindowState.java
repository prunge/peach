package au.net.causal.peach.integrationtest.common;

import java.awt.Frame;

import javax.swing.JFrame;

import org.junit.Test;

import au.net.causal.peach.NativeWindow;
import au.net.causal.peach.NativeWindowState;
import au.net.causal.peach.integrationtest.AbstractDesktopTestCase;

import static org.junit.Assert.*;

/**
 * Test retrieving and setting window state.
 */
public class TestWindowState extends AbstractDesktopTestCase
{
	@Test
	public void testGetStateNormal()
	{
		JFrame frame = createWindowWithRandomTitle();
		
		NativeWindow window = getDesktop().findWindowWithTitle(frame.getTitle());
		assertEquals("Wrong window state.", NativeWindowState.NORMAL, window.getState());
	}
	
	@Test
	public void testGetStateMinimized()
	{
		JFrame frame = createWindowWithRandomTitle();
		frame.setExtendedState(Frame.ICONIFIED);
		
		NativeWindow window = getDesktop().findWindowWithTitle(frame.getTitle());
		assertEquals("Wrong window state.", NativeWindowState.MINIMIZED, window.getState());
	}
	
	@Test
	public void testGetStateMaximized()
	{
		JFrame frame = createWindowWithRandomTitle();
		frame.setExtendedState(Frame.MAXIMIZED_BOTH);
		
		NativeWindow window = getDesktop().findWindowWithTitle(frame.getTitle());
		assertEquals("Wrong window state.", NativeWindowState.MAXIMIZED, window.getState());
	}
	
	@Test
	public void testSetStateMinimized()
	{
		JFrame frame = createWindowWithRandomTitle();
		
		NativeWindow window = getDesktop().findWindowWithTitle(frame.getTitle());
		window.setState(NativeWindowState.MINIMIZED);
		
		assertEquals("Wrong frame state.", Frame.ICONIFIED, frame.getExtendedState());
	}
	
	@Test
	public void testSetStateMaximized()
	{
		JFrame frame = createWindowWithRandomTitle();
		
		NativeWindow window = getDesktop().findWindowWithTitle(frame.getTitle());
		window.setState(NativeWindowState.MAXIMIZED);
		
		assertEquals("Wrong frame state.", Frame.MAXIMIZED_BOTH, frame.getExtendedState());
	}
	
	@Test
	public void testSetStateRestoreFromMaximized()
	{
		JFrame frame = createWindowWithRandomTitle();
		frame.setExtendedState(Frame.MAXIMIZED_BOTH);
		
		NativeWindow window = getDesktop().findWindowWithTitle(frame.getTitle());
		window.setState(NativeWindowState.NORMAL);
		
		assertEquals("Wrong frame state.", Frame.NORMAL, frame.getExtendedState());
	}
	
	@Test
	public void testSetStateRestoreFromMinimized()
	{
		JFrame frame = createWindowWithRandomTitle();
		frame.setExtendedState(Frame.ICONIFIED);
		
		NativeWindow window = getDesktop().findWindowWithTitle(frame.getTitle());
		window.setState(NativeWindowState.NORMAL);
		
		assertEquals("Wrong frame state.", Frame.NORMAL, frame.getExtendedState());
	}
}
