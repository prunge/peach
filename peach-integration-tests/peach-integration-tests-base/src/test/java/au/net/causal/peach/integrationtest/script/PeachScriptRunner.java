package au.net.causal.peach.integrationtest.script;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.script.Invocable;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import jdk.nashorn.api.scripting.URLReader;

import org.junit.ComparisonFailure;
import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.ParentRunner;
import org.junit.runners.model.InitializationError;

import au.net.causal.peach.Peach;

import com.google.common.base.Throwables;

@SuppressWarnings("restriction")
public class PeachScriptRunner extends ParentRunner<PeachScriptRunner.TestHolder>
{
	private final Class<?> testClass;
	
	private final Peach peach;
	private final ScriptEngine scriptEngine;
	private final Invocable invoker;
	
	private final URL testRunnerUrl = PeachScriptRunner.class.getResource("testrunner.js");
	
	private final List<TestHolder> tests = new ArrayList<>();
	
	private final List<URL> testScripts;
	
	public PeachScriptRunner(Class<?> testClass)
	throws InitializationError
	{
		super(testClass);
		this.testClass = testClass;
		
		peach = new Peach();
		scriptEngine = peach.createJavascriptEngine();
		invoker = (Invocable)scriptEngine;

		try
		{
			scriptEngine.eval(new URLReader(testRunnerUrl));
		}
		catch (ScriptException e)
		{
			throw new RuntimeException(e);
		}

		for (URL scriptUrl : readTestScriptUrls())
		{
			try
			{
				scriptEngine.eval(new URLReader(scriptUrl));
			}
			catch (ScriptException e)
			{
				throw new RuntimeException(e);
			}
		}
		
		TestCallback callBack = new TestCallback();
		scriptEngine.getBindings(ScriptContext.GLOBAL_SCOPE).put("scriptRunnerCallBack", callBack);
		try
		{
			scriptEngine.eval("unit.makeTests(scriptRunnerCallBack)");
		}
		catch (ScriptException e)
		{
			throw new RuntimeException(e);
		}
		
		for (Map<String, ?> test : callBack.getTests())
		{
			Description testDescription = Description.createTestDescription(testClass, test.get("name").toString());
			tests.add(new TestHolder(test, testDescription));
		}
		
		this.testScripts = readTestScriptUrls();
	}
	
	protected List<URL> readTestScriptUrls()
	{
		Stream<String> names;
		
		TestScript testScriptAnnotation = testClass.getAnnotation(TestScript.class);
		if (testScriptAnnotation != null)
			names = Stream.of(testScriptAnnotation.value());
		else
			names = Stream.of(testClass.getSimpleName() + ".js");
		
		return(names.map(testClass::getResource).filter(Objects::nonNull).collect(Collectors.toList()));
	}
	
	@Override
	protected String getName()
	{
		if (testScripts.isEmpty())
			return(super.getName());
		else if (testScripts.size() == 1)
			return(simplePath(testScripts.get(0)));
		else
			return(testScripts.stream().map(this::simplePath).collect(Collectors.toList()).toString());
	}
	
	private String simplePath(URL url)
	{
		return(url.getPath().substring(url.getPath().lastIndexOf('/') + 1));
	}
	
	@Override
	protected Description describeChild(TestHolder child)
	{
		return(child.getDescription());
	}
	
	@Override
	protected List<TestHolder> getChildren()
	{
		return(tests);
	}
	
	@Override
	protected void runChild(TestHolder test, RunNotifier notifier)
	{
		notifier.fireTestStarted(test.getDescription());
		try
		{
			invoker.invokeFunction("runSingleTest", test.getScriptTest());
		}
		catch (Throwable e)
		{
			Throwable original = e;
			
			//Unwrap assertion errors wrapped in script exceptions
			Throwable rootCause = Throwables.getRootCause(e);
			if (rootCause instanceof ComparisonFailure)
			{
				ComparisonFailure rootCF = (ComparisonFailure)rootCause;
				e = new ComparisonFailure(rootCF.getMessage(), rootCF.getExpected(), rootCF.getActual());
				e.initCause(original);
			}
			else if (rootCause instanceof AssertionError)
				e = new AssertionError(rootCause.getMessage(), original);
			
			notifier.fireTestFailure(new Failure(test.getDescription(), e));
		}
		finally
		{
			notifier.fireTestFinished(test.getDescription());
		}
	}
	
	public class TestCallback
	{
		private final List<Map<String, ?>> tests = new ArrayList<>();
		
		public void makeTest(Map<String, Object> td)
		{
			tests.add(td);
		}
		
		public List<? extends Map<String, ?>> getTests()
		{
			return(Collections.unmodifiableList(tests));
		}
	}
	
	public static class TestHolder
	{
		private final Map<String, ?> scriptTest;
		private final Description description;
		
		public TestHolder(Map<String, ?> scriptTest, Description description)
		{
			this.scriptTest = scriptTest;
			this.description = description;
		}
		
		public Map<String, ?> getScriptTest()
		{
			return(scriptTest);
		}
		
		public Description getDescription()
		{
			return(description);
		}
	}
	
	public static void throwRawError(Error t)
	{
		throw t;
	}
	
	public static void throwRawError(Exception t)
	{
		throw new RuntimeException(t);
	}
	
	public static void throwRawError(RuntimeException t)
	{
		throw t;
	}
}
