function test(atomicInteger, latch)
{
	desktop.on.mouseDown({button: 'left'}, 
			function(event)
			{
				atomicInteger.incrementAndGet();
				latch.countDown();
			});
}