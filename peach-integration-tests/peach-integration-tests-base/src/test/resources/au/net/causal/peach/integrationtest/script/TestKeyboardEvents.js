var eventInterceptor;

unit.beforeTest("setUpEventInterceptor", function()
{
	var EventInterceptor = Java.type("au.net.causal.peach.integrationtest.EventInterceptor");
	eventInterceptor = new EventInterceptor();
	eventInterceptor.showEventInterceptionWindow();
});

unit.afterTest("cleanUpEventInterceptor", function()
{
	eventInterceptor.close();
	desktop.on.clear();
});

unit.test("keyDownEvent", function(assert)
{
	var latch = new Latch(1);
	var keyPressCount = 0;
	
	desktop.on.keyDown(function(ev)
	{
		keyPressCount++;
		latch.countDown();
		
		assert.equal("Wrong key.", "a", ev.key.name);
	});
	
	//Potential for missing events since there could be delay between starting up event pump and getting first message
	sleep(100);
	
	desktop.keyboard.control.type(desktop.keyboard.getAny("a"));
	
	latch.await();
	
	assert.equal("Button press count wrong.", 1, keyPressCount);
});

unit.test("keyDownEventString", function(assert)
{
	var latch = new Latch(1);
	var keyPressCount = 0;
	
	desktop.on.keyDown(function(ev)
	{
		keyPressCount++;
		latch.countDown();
		
		assert.equal("Wrong key.", "a", ev.key.name);
	});
	
	//Potential for missing events since there could be delay between starting up event pump and getting first message
	sleep(100);
	
	desktop.keyboard.control.type("a");
	
	latch.await();
	
	assert.equal("Button press count wrong.", 1, keyPressCount);
});

unit.test("keySequence", function(assert)
{
	var latch = new Latch(2);
	var keyPressCount = 0;
	var keysPressed = [];
	
	desktop.on.keyDown(function(ev)
	{
		keyPressCount++;
		latch.countDown();
		keysPressed.push(ev.key.name);
		
		//If b is down, ensure a is not down
		if (ev.key.name == "b")
		{
			assert.false("Other key should not be down.", ev.keyboardState.areAnyDown(desktop.keyboard.getAll("a")));
		}
	});
	
	//Potential for missing events since there could be delay between starting up event pump and getting first message
	sleep(100);
	
	desktop.keyboard.control.typeSequence(desktop.keyboard.getAny("a"), desktop.keyboard.getAny("b"));
	
	latch.await();
	
	assert.equal("Button press count wrong.", 2, keyPressCount);
	assert.equal("Wrong keys pressed.", ["a", "b"], keysPressed);
});

unit.test("keySequenceString", function(assert)
{
	var latch = new Latch(2);
	var keyPressCount = 0;
	var keysPressed = [];
	
	desktop.on.keyDown(function(ev)
	{
		keyPressCount++;
		latch.countDown();
		keysPressed.push(ev.key.name);
		
		//If b is down, ensure a is not down
		if (ev.key.name == "b")
		{
			assert.false("Other key should not be down.", ev.keyboardState.areAnyDown("a"));
		}
	});
	
	//Potential for missing events since there could be delay between starting up event pump and getting first message
	sleep(100);
	
	desktop.keyboard.control.typeSequence("a", "b");
	
	latch.await();
	
	assert.equal("Button press count wrong.", 2, keyPressCount);
	assert.equal("Wrong keys pressed.", ["a", "b"], keysPressed);
});

unit.test("keyCombo", function(assert)
{
	var latch = new Latch(2);
	var keyPressCount = 0;
	var keysPressed = [];
	
	desktop.on.keyDown(function(ev)
	{
		keyPressCount++;
		latch.countDown();
		keysPressed.push(ev.key.name);
		
		//If b is down, ensure shift is also down
		if (ev.key.name == "b")
		{
			assert.true("Other key should be down.", ev.keyboardState.areAnyDown(desktop.keyboard.getAll("shift")));
		}
	});
	
	//Potential for missing events since there could be delay between starting up event pump and getting first message
	sleep(100);
	
	desktop.keyboard.control.typeCombo(desktop.keyboard.getAny("shift"), desktop.keyboard.getAny("b"));
	
	latch.await();
	
	assert.equal("Button press count wrong.", 2, keyPressCount);
	assert.equal("Wrong keys pressed.", ["shift", "b"], keysPressed);
});

unit.test("keyComboString", function(assert)
{
	var latch = new Latch(2);
	var keyPressCount = 0;
	var keysPressed = [];
	
	desktop.on.keyDown(function(ev)
	{
		keyPressCount++;
		latch.countDown();
		keysPressed.push(ev.key.name);
		
		//If b is down, ensure shift is also down
		if (ev.key.name == "b")
		{
			assert.true("Other key should be down.", ev.keyboardState.areAnyDown("shift"));
		}
	});
	
	//Potential for missing events since there could be delay between starting up event pump and getting first message
	sleep(100);
	
	desktop.keyboard.control.typeCombo("shift", "b");
	
	latch.await();
	
	assert.equal("Button press count wrong.", 2, keyPressCount);
	assert.equal("Wrong keys pressed.", ["shift", "b"], keysPressed);
});

unit.test("keyEventMouseState", function(assert)
{
	var latch = new Latch(1);
	var keyPressCount = 0;
	
	desktop.on.keyDown(function(ev)
	{
		keyPressCount++;
		latch.countDown();
		
		assert.equal("Wrong key.", "a", ev.key.name);
		
		assert.false("Button should not be down.", ev.mouseState.isDown("left"));
		assert.true("Button should be down.", ev.mouseState.isDown("middle"));
	});
	
	//Potential for missing events since there could be delay between starting up event pump and getting first message
	sleep(100);
	
	desktop.mouse.control.buttonDown("middle");
	desktop.keyboard.control.type(desktop.keyboard.getAny("a"));
	desktop.mouse.control.buttonUp("middle");
	
	latch.await();
	
	assert.equal("Button press count wrong.", 1, keyPressCount);
});