var eventInterceptor;

unit.beforeTest("setUpEventInterceptor", function()
{
	var EventInterceptor = Java.type("au.net.causal.peach.integrationtest.EventInterceptor");
	eventInterceptor = new EventInterceptor();
	eventInterceptor.showEventInterceptionWindow();
	
	//Perform a dummy click in case the mouse down state is messed up
	eventInterceptor.robotClickSafe(java.awt.event.InputEvent.BUTTON2_DOWN_MASK);
	sleep(200);
	eventInterceptor.robotClickSafe(java.awt.event.InputEvent.BUTTON3_DOWN_MASK);
	sleep(200);
	eventInterceptor.robotClickSafe(java.awt.event.InputEvent.BUTTON1_DOWN_MASK);
	sleep(200);
});

unit.afterTest("cleanUpEventInterceptor", function()
{
	eventInterceptor.close();
	desktop.on.clear();
});

unit.test("leftClick", function(assert)
{
	var latch = new Latch(1);
	var buttonPressCount = 0;
	
	desktop.on.mouseDown(function(ev)
	{
		buttonPressCount++;
		latch.countDown();
		assert.equal("Wrong button.", "left", ev.button.name);
		assert.equal("Wrong x position.", eventInterceptor.safeX, ev.x);
		assert.equal("Wrong y position.", eventInterceptor.safeY, ev.y);
	});
	
	//Potential for missing events since there could be delay between starting up event pump and getting first message
	sleep(100);
	
	desktop.mouse.control.move(eventInterceptor.safeX, eventInterceptor.safeY);
	desktop.mouse.control.click("left");
	
	latch.await();
	
	assert.equal("Button press count wrong.", 1, buttonPressCount);
});

unit.test("middleClick", function(assert)
{
	var latch = new Latch(1);
	var buttonPressCount = 0;
	
	desktop.on.mouseDown(function(ev)
	{
		buttonPressCount++;
		latch.countDown();
		assert.equal("Wrong button.", "middle", ev.button.name);
		assert.equal("Wrong x position.", eventInterceptor.safeX, ev.x);
		assert.equal("Wrong y position.", eventInterceptor.safeY, ev.y);
	});
	
	//Potential for missing events since there could be delay between starting up event pump and getting first message
	sleep(100);
	
	desktop.mouse.control.move(eventInterceptor.safeX, eventInterceptor.safeY);
	desktop.mouse.control.click("middle");
	
	latch.await();
	
	assert.equal("Button press count wrong.", 1, buttonPressCount);
});

unit.test("rightClick", function(assert)
{
	var latch = new Latch(1);
	var buttonPressCount = 0;
	
	desktop.on.mouseDown(function(ev)
	{
		buttonPressCount++;
		latch.countDown();
		assert.equal("Wrong button.", "right", ev.button.name);
		assert.equal("Wrong x position.", eventInterceptor.safeX, ev.x);
		assert.equal("Wrong y position.", eventInterceptor.safeY, ev.y);
	});
	
	//Potential for missing events since there could be delay between starting up event pump and getting first message
	sleep(100);
	
	desktop.mouse.control.move(eventInterceptor.safeX, eventInterceptor.safeY);
	desktop.mouse.control.click("right");
	
	latch.await();
	
	assert.equal("Button press count wrong.", 1, buttonPressCount);
});

unit.test("wheelDown", function(assert)
{
	var latch = new Latch(1);
	var wheelMovement = 0;
	
	desktop.on.mouseWheelMove(function(ev)
	{
		wheelMovement += ev.steps;
		latch.countDown();
		assert.equal("Wrong step count.", 1, ev.relativeSteps);
		assert.equal("Wrong direction.", MouseWheelDirection.DOWN, ev.direction);
		assert.equal("Wrong x position.", eventInterceptor.safeX, ev.x);
		assert.equal("Wrong y position.", eventInterceptor.safeY, ev.y);
	});
	
	//Potential for missing events since there could be delay between starting up event pump and getting first message
	sleep(100);
	
	desktop.mouse.control.move(eventInterceptor.safeX, eventInterceptor.safeY);
	desktop.mouse.control.scrollWheel("down", 1);
	
	latch.await();
	
	assert.equal("Wrong wheel movement.", 1, wheelMovement);
});

unit.test("wheelUp", function(assert)
{
	var latch = new Latch(1);
	var wheelMovement = 0;
	
	desktop.on.mouseWheelMove(function(ev)
	{
		wheelMovement += ev.steps;
		latch.countDown();
		assert.equal("Wrong step count.", 1, ev.relativeSteps);
		assert.equal("Wrong direction.", MouseWheelDirection.UP, ev.direction);
		assert.equal("Wrong x position.", eventInterceptor.safeX, ev.x);
		assert.equal("Wrong y position.", eventInterceptor.safeY, ev.y);
	});
	
	//Potential for missing events since there could be delay between starting up event pump and getting first message
	sleep(100);
	
	desktop.mouse.control.move(eventInterceptor.safeX, eventInterceptor.safeY);
	desktop.mouse.control.scrollWheel("up", 1);
	
	latch.await();
	
	assert.equal("Wrong wheel movement.", -1, wheelMovement);
});

unit.test("leftClickFilter", function(assert)
{
	var latch = new Latch(1);
	var buttonPressCount = 0;
	
	desktop.on.mouseDown({button: "left"}, function(ev)
	{
		buttonPressCount++;
		latch.countDown();
		assert.equal("Wrong button.", "left", ev.button.name);
		assert.equal("Wrong x position.", eventInterceptor.safeX, ev.x);
		assert.equal("Wrong y position.", eventInterceptor.safeY, ev.y);
	});
	
	//Potential for missing events since there could be delay between starting up event pump and getting first message
	sleep(100);
	
	desktop.mouse.control.move(eventInterceptor.safeX, eventInterceptor.safeY);
	desktop.mouse.control.click("right");
	desktop.mouse.control.click("left");
	
	latch.await();
	
	assert.equal("Button press count wrong.", 1, buttonPressCount);
});

unit.test("rightClickFilter", function(assert)
{
	var latch = new Latch(1);
	var buttonPressCount = 0;
	
	desktop.on.mouseDown({button: "right"}, function(ev)
	{
		buttonPressCount++;
		latch.countDown();
		assert.equal("Wrong button.", "right", ev.button.name);
		assert.equal("Wrong x position.", eventInterceptor.safeX, ev.x);
		assert.equal("Wrong y position.", eventInterceptor.safeY, ev.y);
	});
	
	//Potential for missing events since there could be delay between starting up event pump and getting first message
	sleep(100);
	
	desktop.mouse.control.move(eventInterceptor.safeX, eventInterceptor.safeY);
	desktop.mouse.control.click("middle");
	desktop.mouse.control.click("right");
	
	latch.await();
	
	assert.equal("Button press count wrong.", 1, buttonPressCount);
});

unit.test("multiFilter", function(assert)
{
	var latch = new Latch(2);
	var buttonPressCount = 0;
	
	desktop.on.mouseDown({buttons: ["left", "right"]}, function(ev)
	{
		buttonPressCount++;
		latch.countDown();
		assert.notEqual("Wrong button.", "middle", ev.button.name);
		assert.equal("Wrong x position.", eventInterceptor.safeX, ev.x);
		assert.equal("Wrong y position.", eventInterceptor.safeY, ev.y);
	});
	
	//Potential for missing events since there could be delay between starting up event pump and getting first message
	sleep(100);
	
	desktop.mouse.control.move(eventInterceptor.safeX, eventInterceptor.safeY);
	desktop.mouse.control.click("left");
	desktop.mouse.control.click("middle");
	desktop.mouse.control.click("right");
	
	latch.await();
	
	assert.equal("Button press count wrong.", 2, buttonPressCount);
});

unit.test("wheelFilter", function(assert)
{
	var latch = new Latch(1);
	var wheelMovement = 0;
	
	desktop.on.mouseWheelMove({direction: "up"}, function(ev)
	{
		wheelMovement += ev.steps;
		latch.countDown();
		assert.equal("Wrong step count.", 1, ev.relativeSteps);
		assert.equal("Wrong direction.", MouseWheelDirection.UP, ev.direction);
		assert.equal("Wrong x position.", eventInterceptor.safeX, ev.x);
		assert.equal("Wrong y position.", eventInterceptor.safeY, ev.y);
	});
	
	//Potential for missing events since there could be delay between starting up event pump and getting first message
	sleep(100);
	
	desktop.mouse.control.move(eventInterceptor.safeX, eventInterceptor.safeY);
	desktop.mouse.control.scrollWheel("down", 1);
	desktop.mouse.control.scrollWheel("up", 1);
	
	latch.await();
	
	assert.equal("Wrong wheel movement.", -1, wheelMovement);
});

unit.test("eventMouseState", function(assert)
{
	var latch = new Latch(2);
	var buttonPressCount = 0;
	
	desktop.on.mouseDown(function(ev)
	{
		buttonPressCount++;
		latch.countDown();
		assert.equal("Wrong x position.", eventInterceptor.safeX, ev.x);
		assert.equal("Wrong y position.", eventInterceptor.safeY, ev.y);
		
		if (ev.button.name == "right")
		{
			assert.true("Button should be down.", ev.mouseState.isDown("middle"));
			assert.false("Button should not be down.", ev.mouseState.isDown("left"));
		}
		else
		{
			assert.false("Button should not be down.", ev.mouseState.isDown("middle"));
			assert.false("Button should not be down.", ev.mouseState.isDown("left"));
		}
	});
	
	//Potential for missing events since there could be delay between starting up event pump and getting first message
	sleep(100);
	
	desktop.mouse.control.move(eventInterceptor.safeX, eventInterceptor.safeY);
	desktop.mouse.control.buttonDown("middle");
	desktop.mouse.control.buttonDown("right");
	desktop.mouse.control.buttonUp("right");
	desktop.mouse.control.buttonUp("middle");
	
	latch.await();
	
	assert.equal("Button press count wrong.", 2, buttonPressCount);
});

unit.test("eventKeyState", function(assert)
{
	var latch = new Latch(1);
	var buttonPressCount = 0;
	
	desktop.on.mouseDown(function(ev)
	{
		buttonPressCount++;
		latch.countDown();
		assert.equal("Wrong x position.", eventInterceptor.safeX, ev.x);
		assert.equal("Wrong y position.", eventInterceptor.safeY, ev.y);
		
		assert.true("Key should be down.", ev.keyboardState.areAnyDown("shift"));
		assert.false("Key should not be down.", ev.keyboardState.areAnyDown("control"));
	});
	
	//Potential for missing events since there could be delay between starting up event pump and getting first message
	sleep(100);
	
	desktop.keyboard.control.press("shift");
	desktop.mouse.control.move(eventInterceptor.safeX, eventInterceptor.safeY);
	desktop.mouse.control.click("middle");
	desktop.keyboard.control.release("shift");
	
	latch.await();
	
	assert.equal("Button press count wrong.", 1, buttonPressCount);
});

