var unit = 
{
	tests: [],
	beforeTests: [],
	afterTests: [],
		
	test: function(name, testFunction)
	{
		this.tests.push({name: name, testFunction: testFunction});
	},

	beforeTest: function(name, beforeTestFunction)
	{
		this.beforeTests.push({name: name, beforeTestFunction: beforeTestFunction});
	},
	
	afterTest: function(name, afterTestFunction)
	{
		this.afterTests.push({name: name, afterTestFunction: afterTestFunction});
	},
	
	runSingleTest: function(test)
	{
		this.assert.failures = [];
		
		for each (var beforeTest in this.beforeTests)
		{
			beforeTest.beforeTestFunction(this.assert);
		}
		
		try
		{	
			test.testFunction(this.assert);
			
			//If we get an async assertion error throw it here
			for each (var asyncFailure in this.assert.failures)
			{
				//Yeah we only get the first one but as long as the user is alerted to the problems this is fine
				//We have to do it this way because otherwise AssertionError gets wrapped in ScriptException and then it doesn't show up
				//correctly in the JUnit UI in the IDE
				Java.type("au.net.causal.peach.integrationtest.script.PeachScriptRunner").throwRawError(asyncFailure);
			}
		}
		finally
		{
			for each (var afterTest in this.afterTests)
			{
				try
				{
					afterTest.afterTestFunction(this.assert);
				}
				catch (err)
				{
					print("Error running after function '" + afterTest.name + "': " + err);
				}
			}
		}
	},
	
	runTests: function()
	{
		for each (var test in this.tests)
		{
			this.runSingleTest(test);
		}
	},
	
	makeTests: function(callback)
	{
		for each (var test in this.tests)
		{
			callback.makeTest(test);
		}
	},
	
	assert: 
	{
		failures: [],
		
		equal: function()
		{
			try
			{
				var expected, actual, message;
				if (arguments.length == 3)
				{
					message = arguments[0];
					expected = arguments[1];
					actual = arguments[2];
				}
				else
				{
					message = null;
					expected = arguments[0];
					actual = arguments[1];
				}
				if (Array.isArray(expected))
				{
					var expectedJavaArray = Java.to(expected, "java.lang.Object[]");
					var actualJavaArray = Java.to(actual, "java.lang.Object[]");
					org.junit.Assert.assertArrayEquals(message, expectedJavaArray, actualJavaArray);
				}
				if ("number" === typeof(expected))
				{	
					org.junit.Assert.assertEquals(message, expected, actual, 0.0);
				}
				else
				{
					org.junit.Assert.assertEquals(message, expected, actual);
				}
			}
			catch (err)
			{
				this.failures.push(err);
				throw err;
			}
		},
	
		notEqual: function()
		{
			try
			{
				var expected, actual, message;
				if (arguments.length == 3)
				{
					message = arguments[0];
					expected = arguments[1];
					actual = arguments[2];
				}
				else
				{
					message = null;
					expected = arguments[0];
					actual = arguments[1];
				}
				org.junit.Assert.assertNotEquals(message, expected, actual);
			}
			catch (err)
			{
				this.failures.push(err);
				throw err;
			}
		},
		
		true: function()
		{
			try
			{
				var message, condition;
				if (arguments.length == 2)
				{
					message = arguments[0];
					condition = arguments[1];
				}
				else
				{
					message = null;
					condition = arguments[0];
				}
				org.junit.Assert.assertTrue(message, condition);
			}
			catch (err)
			{
				this.failures.push(err);
				throw err;
			}
		},
		
		false: function()
		{
			try
			{
				var message, condition;
				if (arguments.length == 2)
				{
					message = arguments[0];
					condition = arguments[1];
				}
				else
				{
					message = null;
					condition = arguments[0];
				}
				org.junit.Assert.assertFalse(message, condition);
			}
			catch (err)
			{
				this.failures.push(err);
				throw err;
			}
		},
		
		fail: function(message)
		{
			try
			{
				org.junit.Assert.fail(message);
			}
			catch (err)
			{
				this.failures.push(err);
				throw err;
			}
		}
	}
};

function sleep(milliseconds)
{
	java.lang.Thread.sleep(milliseconds);
}

function runSingleTest(test)
{
	unit.runSingleTest(test);
}

var Latch = function(n) 
{
	this.latch = new java.util.concurrent.CountDownLatch(n);
	
	this.countDown = function()
	{
		this.latch.countDown();
	};
	
	this.await = function()
	{
		this.latch.await(5, java.util.concurrent.TimeUnit.SECONDS);
	};
};

