package au.net.causal.peach.integrationtest.mac;

import au.net.causal.peach.Desktop;
import au.net.causal.peach.integrationtest.AbstractDesktopTestCase;
import au.net.causal.peach.mac.MacDesktop;

import static org.junit.Assert.*;

public class AbstractMacDesktopTestCase extends AbstractDesktopTestCase
{
	@Override
	public MacDesktop getDesktop()
	{
		Desktop desktop = super.getDesktop();
		assertTrue("Not running under Mac OS.", desktop instanceof MacDesktop);
		return((MacDesktop)desktop);
	}
}
