package au.net.causal.peach.integrationtest.mac;

import java.nio.file.Path;

import javax.swing.JFrame;

import org.junit.Test;

import au.net.causal.peach.NativeProcess;
import au.net.causal.peach.NativeWindow;

import static org.junit.Assert.*;

public class TestMacNativeProcess extends AbstractMacDesktopTestCase
{
	private NativeProcess getOwnNativeProcess()
	{
		JFrame frame = createWindowWithRandomTitle();
		NativeWindow window = getDesktop().findWindowWithTitle(frame.getTitle());
		NativeProcess process = window.getProcess();
		assertNotNull("Null process.", process);
		return(process);
	}
	
	@Test
	public void testGetProcessId()
	{
		NativeProcess process = getOwnNativeProcess();
		Long processId = process.getId();
		assertTrue("Bad process ID.", processId > 0L);
	}
	
	@Test
	public void testGetExecutable()
	{
		NativeProcess process = getOwnNativeProcess();
		Path exe = process.getExecutable();
		String exeFileName = exe.getFileName().toString();
		assertTrue("Unrecognized process exe name: " + exeFileName, exeFileName.equalsIgnoreCase("java") || exeFileName.equalsIgnoreCase("javaw"));
	}

}
