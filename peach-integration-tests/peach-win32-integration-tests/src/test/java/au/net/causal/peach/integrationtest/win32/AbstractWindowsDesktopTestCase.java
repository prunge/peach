package au.net.causal.peach.integrationtest.win32;

import static org.junit.Assert.*;

import au.net.causal.peach.Desktop;
import au.net.causal.peach.integrationtest.AbstractDesktopTestCase;
import au.net.causal.peach.windows.WindowsDesktop;

public class AbstractWindowsDesktopTestCase extends AbstractDesktopTestCase
{
	@Override
	public WindowsDesktop getDesktop()
	{
		Desktop desktop = super.getDesktop();
		assertTrue("Not running under Windows.", desktop instanceof WindowsDesktop);
		return((WindowsDesktop)desktop);
	}
}
