package au.net.causal.peach;

import java.util.Objects;

import au.net.causal.peach.support.ArrayPreconditions;

public abstract class AbstractKeyboardControl implements KeyboardControl
{
	private static final int DEFAULT_WAIT_TIME = 10;
	
	private final Keyboard keyboard;
	
	protected AbstractKeyboardControl(Keyboard keyboard)
	{
		Objects.requireNonNull(keyboard, "keyboard == null");
		this.keyboard = keyboard;
	}

	@Override
	public abstract void press(Key key);

	@Override
	public abstract void release(Key key);

	@Override
	public void type(Key key)
	{
		Objects.requireNonNull(key, "key == null");
		press(key);
		defaultWait();
		release(key);
	}

	@Override
	public void typeSequence(Key... keys)
	{
		ArrayPreconditions.requireElementsNonNull(keys, "keys");
		for (Key key : keys)
		{
			type(key);
			defaultWait();
		}
	}

	@Override
	public void typeCombo(Key... keys)
	{
		ArrayPreconditions.requireElementsNonNull(keys, "keys");
		
		for (Key key : keys)
		{
			press(key);
			defaultWait();
		}
		
		for (int i = keys.length - 1; i >= 0; i--)
		{
			Key key = keys[i];
			release(key);
			defaultWait();
		}
	}

	private void defaultWait()
	{
		try
		{
			Thread.sleep(DEFAULT_WAIT_TIME);
		}
		catch (InterruptedException e)
		{
			//Interrupted, not much we can do but proceed
		}
	}

	@Override
	public void press(String keyString)
	{
		Objects.requireNonNull(keyString, "keyString == null");
		
		Key key = keyboard.getAny(keyString);
		if (key != null)
			press(key);
	}

	@Override
	public void release(String keyString)
	{
		Objects.requireNonNull(keyString, "keyString == null");
		
		Key key = keyboard.getAny(keyString);
		if (key != null)
			release(key);
	}

	@Override
	public void type(String keyString)
	{
		Objects.requireNonNull(keyString, "keyString == null");
		
		Key key = keyboard.getAny(keyString);
		if (key != null)
			type(key);
	}
	
	private Key[] keyStringsToKeys(String... keyStrings)
	{
		Key[] keys = new Key[keyStrings.length];
		for (int i = 0; i < keyStrings.length; i++)
		{
			String keyString = keyStrings[i];
			Key key = keyboard.getAny(keyString);
			if (key == null)
				return(null);
			
			keys[i] = key;
		}
		return(keys);
	}

	@Override
	public void typeSequence(String... keyStrings)
	{
		ArrayPreconditions.requireElementsNonNull(keyStrings, "keyStrings");
		Key[] keys = keyStringsToKeys(keyStrings);
		if (keys == null)
			return;
		typeSequence(keys);
	}

	@Override
	public void typeCombo(String... keyStrings)
	{
		ArrayPreconditions.requireElementsNonNull(keyStrings, "keyStrings");
		Key[] keys = keyStringsToKeys(keyStrings);
		if (keys == null)
			return;
		typeCombo(keys);
	}

}
