package au.net.causal.peach;

import java.util.Objects;

import au.net.causal.peach.event.MouseButton;

public abstract class AbstractMouseControl implements MouseControl
{
	private final Mouse mouse;
	
	protected AbstractMouseControl(Mouse mouse)
	{
		Objects.requireNonNull(mouse, "mouse == null");
		this.mouse = mouse;
	}
	
	@Override
	public void buttonDown(String buttonName)
	{
		MouseButton button = mouse.button(buttonName);
		if (button != null)
			buttonDown(button);
	}

	@Override
	public void buttonUp(String buttonName)
	{
		MouseButton button = mouse.button(buttonName);
		if (button != null)
			buttonUp(button);
	}

	@Override
	public void click(String buttonName)
	{
		MouseButton button = mouse.button(buttonName);
		if (button != null)
			click(button);
	}

	@Override
	public void doubleClick(String buttonName)
	{
		MouseButton button = mouse.button(buttonName);
		if (button != null)
			doubleClick(button);
	}
}
