package au.net.causal.peach;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Provides access to native windows.
 */
@ParametersAreNonnullByDefault
public interface Desktop
{
	/**
	 * @return a list of all application windows.
	 */
	@Nonnull
	public List<? extends NativeWindow> getWindows();
	
	/**
	 * Finds windows matching a filter.
	 * 
	 * @param predicate the filter predicate.
	 * 
	 * @return a list of matching windows.
	 */
	@Nonnull
	public default List<? extends NativeWindow> findWindows(Predicate<? super NativeWindow> predicate)
	{
		Objects.requireNonNull(predicate, "predicate == null");
		Stream<? extends NativeWindow> stream = getWindows().stream();
		List<? extends NativeWindow> result = stream.filter(predicate).collect(Collectors.<NativeWindow>toList());
		return(result);
	}
	
	/**
	 * Finds all windows with the specified title.  Title match is exact and case sensitive.
	 * 
	 * @param title the title to match.
	 * 
	 * @return a list of matching windows.
	 */
	@Nonnull
	public default List<? extends NativeWindow> findWindowsWithTitle(String title)
	{
		Objects.requireNonNull(title, "title == null");
		return(findWindows(w -> title.equals(w.getTitle())));
	}
	
	/**
	 * Finds the first window with the specified title.  Title match is exact and case sensitive.
	 * If no matching windows are found, <code>null</code> is returned.
	 * 
	 * @param title the title to match.
	 * 
	 * @return the first matching window, or <code>null</code>.
	 */
	@Nullable
	public default NativeWindow findWindowWithTitle(String title)
	{
		Objects.requireNonNull(title, "title == null");
		return(findWindowsWithTitle(title).stream().findFirst().orElse(null));
	}
	
	/**
	 * Finds all windows whose title matches a regular expression pattern.
	 * 
	 * @param pattern the regular expression pattern.
	 * 
	 * @return a list of matching windows.
	 */
	@Nonnull
	public default List<? extends NativeWindow> findWindowsWithTitleMatching(String pattern)
	{
		Objects.requireNonNull(pattern, "pattern == null");
		return(findWindowsWithTitleMatching(Pattern.compile(pattern)));
	}
	
	/**
	 * Finds the first window whose title matches a regular expression pattern.
	 * If no matching windows are found, <code>null</code> is returned.
	 * 
	 * @param pattern the regular expression pattern.
	 * 
	 * @return the first matching window, or <code>null</code>.
	 */
	@Nullable
	public default NativeWindow findWindowWithTitleMatching(String pattern)
	{
		Objects.requireNonNull(pattern, "pattern == null");
		return(findWindowsWithTitleMatching(pattern).stream().findFirst().orElse(null));
	}
	
	/**
	 * Finds all windows whose title matches a regular expression pattern.
	 * 
	 * @param pattern the regular expression pattern.
	 * 
	 * @return a list of matching windows.
	 */
	@Nonnull
	public default List<? extends NativeWindow> findWindowsWithTitleMatching(Pattern pattern)
	{
		Objects.requireNonNull(pattern, "pattern == null");
		return(findWindows(w -> pattern.matcher(w.getTitle()).matches()));
	}
	
	/**
	 * Finds the first window whose title matches a regular expression pattern.
	 * If no matching windows are found, <code>null</code> is returned.
	 * 
	 * @param pattern the regular expression pattern.
	 * 
	 * @return the first matching window, or <code>null</code>.
	 */
	@Nullable
	public default NativeWindow findWindowWithTitleMatching(Pattern pattern)
	{
		Objects.requireNonNull(pattern, "pattern == null");
		return(findWindowsWithTitleMatching(pattern).stream().findFirst().orElse(null));
	}
	
	/**
	 * Finds all windows whose title contains a string.  Matches are case sensitive.
	 * 
	 * @param partialTitle the partial title to search for.
	 * 
	 * @return a list of matching windows.
	 */
	@Nonnull
	public default List<? extends NativeWindow> findWindowsWithTitleContaining(String partialTitle)
	{
		Objects.requireNonNull(partialTitle, "partialTitle == null");
		return(findWindows(w -> 
					{ 
						String t = w.getTitle();
						if (t == null)
							return(false);
						
						return(w.getTitle().contains(partialTitle));
					}));
	}
	
	/**
	 * Finds the first window whose title contains a string.  Matches are case sensitive.
	 * If no matching windows are found, <code>null</code> is returned.
	 * 
	 * @param partialTitle the partial title to search for.
	 * 
	 * @return the first matching window, or <code>null</code>.
	 */
	@Nullable
	public default NativeWindow findWindowWithTitleContaining(String partialTitle)
	{
		Objects.requireNonNull(partialTitle, "partialTitle == null");
		return(findWindowsWithTitleContaining(partialTitle).stream().findFirst().orElse(null));
	}
	
	/**
	 * Finds all windows whose title contains a string.  Matches are case insensitive.
	 * 
	 * @param partialTitle the partial title to search for.
	 * 
	 * @return a list of matching windows.
	 */
	@Nonnull
	public default List<? extends NativeWindow> findWindowsWithTitleContainingIgnoreCase(String partialTitle)
	{
		Objects.requireNonNull(partialTitle, "partialTitle == null");
		return(findWindows(w -> w.getTitle().toLowerCase(Locale.ENGLISH).contains(partialTitle)));
	}
	
	/**
	 * Finds the first window whose title contains a string.  Matches are case insensitive.
	 * If no matching windows are found, <code>null</code> is returned.
	 * 
	 * @param partialTitle the partial title to search for.
	 * 
	 * @return the first matching window, or <code>null</code>.
	 */
	@Nullable
	public default NativeWindow findWindowWithTitleContainingIgnoreCase(String partialTitle)
	{
		Objects.requireNonNull(partialTitle, "partialTitle == null");
		return(findWindowsWithTitleContainingIgnoreCase(partialTitle).stream().findFirst().orElse(null));
	}
	
	@Nonnull
	public DesktopEvents getOn();
	
	@Nonnull
	public Keyboard getKeyboard();
	
	@Nonnull
	public Mouse getMouse();
}
