package au.net.causal.peach;

import java.util.Collections;
import java.util.Map;

import au.net.causal.peach.event.GenericEventListener;
import au.net.causal.peach.event.KeyDownListener;
import au.net.causal.peach.event.KeyEventConstraints;
import au.net.causal.peach.event.KeyUpListener;
import au.net.causal.peach.event.MouseDownListener;
import au.net.causal.peach.event.MouseEventConstraints;
import au.net.causal.peach.event.MouseUpListener;
import au.net.causal.peach.event.MouseWheelEventConstraints;
import au.net.causal.peach.event.MouseWheelListener;
import au.net.causal.peach.event.StringifiedKeyEventConstraints;
import au.net.causal.peach.support.RealObjectMaker;

public interface DesktopEvents
{
	public void clear();
	
	public void mouseDown(MouseEventConstraints constraints, MouseDownListener listener);
	public void mouseUp(MouseEventConstraints constraints, MouseUpListener listener);
	public void mouseWheelMove(MouseWheelEventConstraints constraints, MouseWheelListener listener);
	
	public void keyDown(KeyEventConstraints constraints, KeyDownListener listener);
	public void keyUp(KeyEventConstraints constraints, KeyUpListener listener);
	
	public default void mouseDown(MouseDownListener listener)
	{
		mouseDown(MouseEventConstraints.NONE, listener);
	}
	
	public default void mouseDown(Map<String, ?> constraints, MouseDownListener listener)
	{
		mouseDown(RealObjectMaker.make(constraints, MouseEventConstraints.class), listener);
	}
	
	public default void mouseUp(MouseUpListener listener)
	{
		mouseUp(MouseEventConstraints.NONE, listener);
	}
	
	public default void mouseUp(Map<String, ?> constraints, MouseUpListener listener)
	{
		mouseUp(RealObjectMaker.make(constraints, MouseEventConstraints.class), listener);
	}
	
	public default void mouseWheelMove(MouseWheelListener listener)
	{
		mouseWheelMove(MouseWheelEventConstraints.NONE, listener);
	}
	
	public default void mouseWheelMove(Map<String, ?> constraints, MouseWheelListener listener)
	{
		mouseWheelMove(RealObjectMaker.make(constraints, MouseWheelEventConstraints.class), listener);
	}
	
	public default void keyDown(KeyDownListener listener)
	{
		keyDown(KeyEventConstraints.NONE, listener);
	}
	
	public default void keyDown(Map<String, ?> constraints, KeyDownListener listener)
	{
		keyDown(RealObjectMaker.make(constraints, StringifiedKeyEventConstraints.class), listener);
	}
	
	public default void keyDown(String constraints, KeyDownListener listener)
	{
		keyDown(StringifiedKeyEventConstraints.parse(constraints), listener);
	}
	
	public void keyDown(StringifiedKeyEventConstraints constraints, KeyDownListener listener);
	
	public default void keyUp(KeyUpListener listener)
	{
		keyUp(KeyEventConstraints.NONE, listener);
	}
	
	public default void keyUp(Map<String, ?> constraints, KeyUpListener listener)
	{
		keyUp(RealObjectMaker.make(constraints, StringifiedKeyEventConstraints.class), listener);
	}
	
	public default void keyUp(String constraints, KeyUpListener listener)
	{
		keyUp(StringifiedKeyEventConstraints.parse(constraints), listener);
	}
	
	public void keyUp(StringifiedKeyEventConstraints constraints, KeyUpListener listener);	
	
	public default void event(String name, GenericEventListener listener)
	{
		event(name, Collections.emptyMap(), listener);
	}
	
	public default void event(String name, Map<String, ?> constraints, GenericEventListener listener)
	{
		//TODO could we scan this via reflection?
		//TODO use a map and magic method handle stuff for efficiency? (but how from an interface?)
		switch (name)
		{
			case "mouseDown":
				mouseDown(constraints, event -> listener.onEvent(event));
				break;
			case "mouseUp":
				mouseUp(constraints, event -> listener.onEvent(event));
				break;
			case "mouseWheelMove":
				mouseWheelMove(constraints, event -> listener.onEvent(event));
				break;
			case "keyDown":
				keyDown(constraints, event -> listener.onEvent(event));
				break;
			case "keyUp":
				keyUp(constraints, event -> listener.onEvent(event));
				break;
		}
	}
}
