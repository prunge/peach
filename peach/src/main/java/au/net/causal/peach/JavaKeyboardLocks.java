package au.net.causal.peach;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.Objects;

import javax.annotation.Nonnull;

public class JavaKeyboardLocks implements KeyboardLocks
{
	private final @Nonnull Toolkit toolkit;

	public JavaKeyboardLocks()
	{
		this(Toolkit.getDefaultToolkit());
	}
	
	public JavaKeyboardLocks(@Nonnull Toolkit toolkit)
	{
		Objects.requireNonNull(toolkit, "toolkit == null");
		this.toolkit = toolkit;
	}

	@Override
	public boolean isCapsLock()
	{
		return(toolkit.getLockingKeyState(KeyEvent.VK_CAPS_LOCK));
	}

	@Override
	public boolean isNumLock()
	{
		return(toolkit.getLockingKeyState(KeyEvent.VK_NUM_LOCK));
	}

	@Override
	public boolean isScrollLock()
	{
		return(toolkit.getLockingKeyState(KeyEvent.VK_SCROLL_LOCK));
	}

	@Override
	public boolean isKanaLock()
	{
		return(toolkit.getLockingKeyState(KeyEvent.VK_KANA_LOCK));
	}

	@Override
	public void setCapsLock(boolean capsLock)
	{
		toolkit.setLockingKeyState(KeyEvent.VK_CAPS_LOCK, capsLock);
	}

	@Override
	public void setNumLock(boolean numLock)
	{
		toolkit.setLockingKeyState(KeyEvent.VK_NUM_LOCK, numLock);
	}

	@Override
	public void setScrollLock(boolean scrollLock)
	{
		toolkit.setLockingKeyState(KeyEvent.VK_SCROLL_LOCK, scrollLock);
	}

	@Override
	public void setKanaLock(boolean kanaLock)
	{
		toolkit.setLockingKeyState(KeyEvent.VK_KANA_LOCK, kanaLock);
	}
	
	@Override
	public void toggleCapsLock()
	{
		setCapsLock(!isCapsLock());
	}
	
	@Override
	public void toggleKanaLock()
	{
		setKanaLock(!isKanaLock());
	}
	
	@Override
	public void toggleNumLock()
	{
		setNumLock(!isNumLock());
	}
	
	@Override
	public void toggleScrollLock()
	{
		setScrollLock(!isScrollLock());
	}
}
