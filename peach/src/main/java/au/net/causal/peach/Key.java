package au.net.causal.peach;

import java.util.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public class Key
{
	private final KeyPosition position;
	private final String name;
	private final int code;
	private final Character character;

	public Key(KeyPosition position, String name, int code)
	{
		this(position, name, code, null);
	}
	
	public Key(KeyPosition position, String name, int code, @Nullable Character character)
	{
		Objects.requireNonNull(position, "position == null");
		Objects.requireNonNull(name, "name == null");
		
		this.position = position;
		this.name = name;
		this.code = code;
		this.character = character;
	}
	
	@Nonnull
	public KeyPosition getPosition()
	{
		return(position);
	}
	
	@Nonnull
	public String getName()
	{
		return(name);
	}

	public int getCode()
	{
		return(code);
	}
	
	@Nullable
	public Character getCharacter()
	{
		return(character);
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + code;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Key other = (Key)obj;
		if (code != other.code)
			return false;
		return true;
	}

	@Nonnull
	@Override
	public String toString()
	{
		return "Key [position=" + position + ", name=" + name + ", code=" + Integer.toHexString(code) + ", character=" + character + "]";
	}
}
