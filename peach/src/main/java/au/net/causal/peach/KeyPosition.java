package au.net.causal.peach;

import java.util.Objects;

public class KeyPosition
{
	public static final KeyPosition MAIN = new KeyPosition("main");
	public static final KeyPosition LEFT = new KeyPosition("left");
	public static final KeyPosition RIGHT = new KeyPosition("right");
	public static final KeyPosition KEYPAD = new KeyPosition("keypad");
	
	private final String name;
	
	public KeyPosition(String name)
	{
		Objects.requireNonNull(name, "name == null");
		this.name = name;
	}
	
	public String getName()
	{
		return(name);
	}
	
	@Override
	public String toString()
	{
		return(name);
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KeyPosition other = (KeyPosition)obj;
		if (name == null)
		{
			if (other.name != null)
				return false;
		}
		else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
}
