package au.net.causal.peach;

import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public interface Keyboard
{
	@Nonnull
	public default Set<? extends Key> getAll(String name)
	{
		return(getAll(null, name));
	}
	
	@Nullable
	public default Key getAny(String name)
	{
		return(getAny(null, name));
	}
	
	@Nullable
	public default Key getAny(@Nullable KeyPosition position, String name)
	{
		Set<? extends Key> keys = getAll(position, name);
		if (keys.isEmpty())
			return(null);
		else
			return(keys.iterator().next());
	}
	
	@Nonnull
	public Set<? extends Key> getAll(@Nullable KeyPosition position, String name);
	
	@Nonnull
	public Key forCode(int nativeKeyCode);
	
	@Nonnull
	public KeyboardState getCurrentState();
	
	@Nonnull
	public KeyboardLocks getLocks();
	
	@Nonnull
	public KeyboardControl getControl();
}
