package au.net.causal.peach;

public interface KeyboardControl
{
	public void press(Key key);
	public void release(Key key);
	
	/**
	 * Presses, then releases a single key.
	 * 
	 * @param key the key to type.
	 */
	public void type(Key key);
	
	/**
	 * Types multiple keys, one after the other.  Suitable for typing multiple keystrokes into an application.  
	 * <p>
	 * 
	 * For example, if keys are 'c', 'a' and 't':
	 * <ul>
	 * 	<li>'c' is pressed</li>
	 * 	<li>'c' is released</li>
	 * 	<li>'a' is pressed</li>
	 * 	<li>'a' is released</li>
	 * 	<li>'t' is pressed</li>
	 * 	<li>'t' is released</li>
	 * </ul> 
	 * 
	 * @param keys the keys to type.
	 */
	public void typeSequence(Key... keys);
	
	/**
	 * Presses multiple keys in order, then releases those keys.  Suitable for typing combinations of modifier keys.
	 * <p>
	 * 
	 * For example, if keys are control, shift and F2:
	 * <ul>
	 * 	<li>control is pressed</li>
	 * 	<li>shift is pressed</li>
	 * 	<li>F2 is pressed</li>
	 * 	<li>F2 is released</li>
	 * 	</li>shift is released</li>
	 * 	<li>control is released</li>
	 * </ul>
	 * 
	 * @param keys the keys to type.
	 */
	public void typeCombo(Key... keys);
	
	public void press(String key);
	public void release(String key);
	public void type(String key);
	public void typeSequence(String... keys);
	public void typeCombo(String... keys);
}
