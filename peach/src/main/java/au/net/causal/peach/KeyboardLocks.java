package au.net.causal.peach;

public interface KeyboardLocks
{
	public boolean isCapsLock();
	public boolean isNumLock();
	public boolean isScrollLock();
	public boolean isKanaLock();
	
	public void setCapsLock(boolean capsLock);
	public void setNumLock(boolean numLock);
	public void setScrollLock(boolean scrollLock);
	public void setKanaLock(boolean kanaLock);
	
	public void toggleCapsLock();
	public void toggleNumLock();
	public void toggleScrollLock();
	public void toggleKanaLock();
}
