package au.net.causal.peach;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public interface KeyboardState
{
	public default boolean isDown(Key key)
	{
		Objects.requireNonNull(key, "key == null");
		return(areAnyDown(Collections.singleton(key)));
	}
	
	public default boolean isDown(String key)
	{
		Objects.requireNonNull(key, "key == null");
		return(areAnyDown(key));
	}
	
	public default boolean areAllDown(Set<? extends Key> keys)
	{
		Objects.requireNonNull(keys, "keys == null");
		return(getKeysDown().containsAll(keys));
	}
	
	public default boolean areAnyDown(Set<? extends Key> keys)
	{
		Objects.requireNonNull(keys, "keys == null");
		return(getKeysDown().stream().anyMatch(keys::contains));
	}
	
	public boolean areAnyDown(String... keys);
	public boolean areAllDown(String... keys);
	
	public Set<? extends Key> getKeysDown();
}
