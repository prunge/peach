package au.net.causal.peach;

import java.util.Objects;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

public class KeyboardStateSnapshot implements KeyboardState
{
	private final Keyboard keyboard;
	private final Set<? extends Key> keysDown;

	public KeyboardStateSnapshot(Keyboard keyboard, Set<? extends Key> keysDown)
	{
		Objects.requireNonNull(keyboard, "keyboard == null");
		Objects.requireNonNull(keysDown, "keysDown == null");
		this.keyboard = keyboard;
		this.keysDown = ImmutableSet.copyOf(keysDown);
	}

	@Override
	public Set<? extends Key> getKeysDown()
	{
		return(keysDown);
	}

	@Override
	public boolean areAnyDown(String... keys)
	{
		for (String key : keys)
		{
			if (areAnyDown(keyboard.getAll(key)))
				return(true);
		}
		
		return(false);
	}

	@Override
	public boolean areAllDown(String... keys)
	{
		for (String key : keys)
		{
			Set<? extends Key> keySet = keyboard.getAll(key);
			if (keySet.isEmpty())
				return(false);
			
			//The keyset contains one or more keys that are the keys with the specified name
			//Only one for each key in the input array need to be down
			if (!areAnyDown(keySet))
				return(false);
		}
		
		return(true);
	}
	
	@Override
	public String toString()
	{
		return "KeyboardStateSnapshot [keysDown=" + keysDown + "]";
	}

	@Override
	public int hashCode()
	{
		return(keysDown.hashCode());
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KeyboardStateSnapshot other = (KeyboardStateSnapshot)obj;
		if (!keysDown.equals(other.keysDown))
			return false;
		return true;
	}
}
