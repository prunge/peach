package au.net.causal.peach;

import java.util.Set;

import au.net.causal.peach.event.MouseButton;

public interface Mouse
{
	public MouseButton button(String name);
	public Set<? extends MouseButton> getButtons();
	
	public MouseState getCurrentState();
	public MouseControl getControl();
}
