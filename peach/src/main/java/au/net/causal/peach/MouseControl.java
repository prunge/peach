package au.net.causal.peach;

import java.util.Locale;
import java.util.Objects;

import au.net.causal.peach.event.MouseButton;
import au.net.causal.peach.event.MouseWheelDirection;

public interface MouseControl
{
	public void move(int x, int y);
	
	public void buttonDown(MouseButton button);
	public void buttonUp(MouseButton button);
	public void click(MouseButton button);
	public void doubleClick(MouseButton button);

	public void buttonDown(String buttonName);
	public void buttonUp(String buttonName);
	public void click(String buttonName);
	public void doubleClick(String buttonName);
	
	public default void scrollWheel(String direction, int amount)
	{
		Objects.requireNonNull(direction, "direction == null");
		scrollWheel(MouseWheelDirection.valueOf(direction.toUpperCase(Locale.ENGLISH)), amount);
	}
	
	public default void scrollWheel(MouseWheelDirection direction, int amount)
	{
		Objects.requireNonNull(direction, "direction == null");
		switch (direction)
		{
			case UP:
				scrollWheel(-amount);
				break;
			case DOWN:
				scrollWheel(amount);
				break;
			default:
				throw new Error("Unknown direction: " + direction);
		}
	}
	
	public void scrollWheel(int relativeAmount);
}
