package au.net.causal.peach;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

import au.net.causal.peach.event.MouseButton;

public interface MouseState
{
	public default boolean isDown(MouseButton button)
	{
		Objects.requireNonNull(button, "button == null");
		return(areAnyDown(Collections.singleton(button)));
	}
	
	public default boolean isDown(String button)
	{
		Objects.requireNonNull(button, "button == null");
		return(areAnyDown(button));
	}
	
	public default boolean areAllDown(Set<? extends MouseButton> buttons)
	{
		Objects.requireNonNull(buttons, "buttons == null");
		return(getButtonsDown().containsAll(buttons));
	}
	
	public default boolean areAnyDown(Set<? extends MouseButton> buttons)
	{
		Objects.requireNonNull(buttons, "buttons == null");
		return(getButtonsDown().stream().anyMatch(buttons::contains));
	}
	
	public boolean areAnyDown(String... buttons);
	public boolean areAllDown(String... buttons);
	
	
	public Set<? extends MouseButton> getButtonsDown();
	public int getX();
	public int getY();
}
