package au.net.causal.peach;

import java.awt.Point;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.ImmutableSet;

import au.net.causal.peach.event.MouseButton;

public class MouseStateSnapshot implements MouseState
{
	private final Mouse mouse;
	
	private final Set<? extends MouseButton> buttonsDown;
	private final Point position;
	
	public MouseStateSnapshot(Mouse mouse, Set<? extends MouseButton> buttonsDown, Point position)
	{
		Objects.requireNonNull(mouse, "mouse == null");
		Objects.requireNonNull(buttonsDown, "buttonsDown == null");
		Objects.requireNonNull(position, "position == null");
		this.mouse = mouse;
		this.buttonsDown = ImmutableSet.copyOf(buttonsDown);
		this.position = position;
	}

	@Override
	public Set<? extends MouseButton> getButtonsDown()
	{
		return(buttonsDown);
	}

	@Override
	public int getX()
	{
		return(position.x);
	}

	@Override
	public int getY()
	{
		return(position.y);
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + buttonsDown.hashCode();
		result = prime * result +position.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MouseStateSnapshot other = (MouseStateSnapshot)obj;
		if (!buttonsDown.equals(other.buttonsDown))
			return false;
		if (!position.equals(other.position))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "MouseStateSnapshot [buttonsDown=" + buttonsDown + ", position=" + position + "]";
	}

	@Override
	public boolean areAnyDown(String... buttons)
	{
		return(areAnyDown(parseButtonNames(buttons)));
	}

	@Override
	public boolean areAllDown(String... buttons)
	{
		return(areAllDown(parseButtonNames(buttons)));
	}

	private Set<MouseButton> parseButtonNames(String... buttons)
	{
		return(Arrays.stream(buttons).map(buttonName -> mouse.button(buttonName)).filter(Objects::nonNull).collect(Collectors.toSet()));
	}
	
}
