package au.net.causal.peach;

import java.nio.file.Path;

public interface NativeProcess
{
	public Long getId();
	public Path getExecutable();
}
