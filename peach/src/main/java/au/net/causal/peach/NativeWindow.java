package au.net.causal.peach;

import java.awt.Rectangle;

/**
 * A native window on the operating system belonging to an application.
 */
public interface NativeWindow
{
	/**
	 * Returns the title of the window, or <code>null</code> if the title does not have a window.
	 * May also return an empty string for untitled windows depending on the operating system.
	 * 
	 * @return the window title.
	 */
	public String getTitle();
	
	/**
	 * Returns the bounds of the window if it is displayed.  Returns <code>null</code> if the window is not visible
	 * on the screen.
	 * 
	 * @return the window bounds.
	 */
	public Rectangle getBounds();	
	
	/**
	 * @return the native process that created the window.
	 */
	public NativeProcess getProcess();
	
	/**
	 * @return the state of the window, whether it is maximized, minimized or in a normal state.  Returns null
	 * 			if the window is not visible.
	 */
	public NativeWindowState getState();
	
	/**
	 * Maximizes, minimizes or restores the window.
	 * 
	 * @param state the state to set.
	 * 
	 * @throws IllegalStateException if the window is not visible.
	 * @throws NullPointerException if <code>state</code> is null.
	 */
	public void setState(NativeWindowState state);
	
	/**
	 * Maximize the window.
	 * 
	 * @throws IllegalStateException if the window is not visible.
	 */
	public default void maximize()
	{
		setState(NativeWindowState.MAXIMIZED);
	}
	
	/**
	 * Minimize the window.
	 * 
	 * @throws IllegalStateException if the window is not visible.
	 */
	public default void minimize()
	{
		setState(NativeWindowState.MINIMIZED);
	}
	
	/**
	 * If the window is maximized, unmaximize the window, if the window is minimized, unminimize it.
	 * 
	 * @throws IllegalStateException if the window is not visible.
	 */
	public default void restore()
	{
		setState(NativeWindowState.NORMAL);
	}
	
	public default boolean isMaximized()
	{
		return(getState() == NativeWindowState.MAXIMIZED);
	}
	
	public default boolean isMinimized()
	{
		return(getState() == NativeWindowState.MINIMIZED);
	}
}
