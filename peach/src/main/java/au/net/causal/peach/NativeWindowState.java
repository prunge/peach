package au.net.causal.peach;

public enum NativeWindowState
{
	NORMAL,
	MINIMIZED,
	MAXIMIZED;
}
