package au.net.causal.peach;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import jdk.internal.dynalink.beans.StaticClass;
import jdk.nashorn.api.scripting.NashornScriptEngine;
import au.net.causal.peach.event.KeyEventConstraints;
import au.net.causal.peach.event.MouseButton;
import au.net.causal.peach.event.MouseEventConstraints;
import au.net.causal.peach.event.MouseWheelDirection;
import au.net.causal.peach.event.MouseWheelEventConstraints;
import au.net.causal.peach.mac.MacDesktopImpl;
import au.net.causal.peach.nashorn.NashornDesktop;
import au.net.causal.peach.windows.WindowsDesktopImpl;

@SuppressWarnings("restriction")
public class Peach
{
	/**
	 * Creates an appropriate desktop for the current platform.
	 * 
	 * @return the created desktop.
	 */
	public Desktop createDesktopForPlatform()
	{
		Desktop desktop;
		
		String os = System.getProperty("os.name");
        if (os.startsWith("Windows")) 
        	desktop = new WindowsDesktopImpl();
        else if (os.startsWith("Mac"))
            desktop = new MacDesktopImpl();
        else
        	throw new RuntimeException("Unsupported platform: " + os);
        
        return(desktop);
	}
	
	public Desktop createDesktopForScriptEngine(ScriptEngine engine)
	{
		Desktop desktop = createDesktopForPlatform();
		
		if (engine instanceof NashornScriptEngine)
			desktop = NashornDesktop.wrap(desktop);
		
		return(desktop);
	}
	
	public ScriptEngine createJavascriptEngine()
	{
		ScriptEngine jsEngine = new ScriptEngineManager().getEngineByExtension("js");
		Bindings bindings = jsEngine.getBindings(ScriptContext.ENGINE_SCOPE);
		Desktop desktop = createDesktopForScriptEngine(jsEngine);
		bindings.put("desktop", desktop);
		bindings.put("MouseEventConstraints", StaticClass.forClass(MouseEventConstraints.class));
		bindings.put("MouseWheelEventConstraints", StaticClass.forClass(MouseWheelEventConstraints.class));
		bindings.put("KeyEventConstraints", StaticClass.forClass(KeyEventConstraints.class));
		bindings.put("MouseButton", StaticClass.forClass(MouseButton.class));
		bindings.put("MouseWheelDirection", StaticClass.forClass(MouseWheelDirection.class));
		bindings.put("Key", StaticClass.forClass(Key.class));
		return(jsEngine);
	}
}
