package au.net.causal.peach;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.util.Objects;

import au.net.causal.peach.event.MouseButton;

import com.google.common.base.Preconditions;

public class RobotMouseControl extends AbstractMouseControl
{
	private static final int DEFAULT_CLICK_DELAY = 20;
	
	private final Robot robot;
	private final int clickDelay;

	public RobotMouseControl(Mouse mouse)
	throws AWTException
	{
		this(mouse, new Robot());
	}
	
	public RobotMouseControl(Mouse mouse, Robot robot)
	{
		this(mouse, robot, DEFAULT_CLICK_DELAY);
	}
	
	public RobotMouseControl(Mouse mouse, Robot robot, int clickDelay)
	{
		super(mouse);
		Objects.requireNonNull(robot, "robot == null");
		Preconditions.checkArgument(clickDelay >= 0, "clickDelay (" + clickDelay + ") < 0");
		this.robot = robot;
		this.clickDelay = clickDelay;
	}

	@Override
	public void move(int x, int y)
	{
		robot.mouseMove(x, y);
		robot.waitForIdle();
	}

	@Override
	public void buttonDown(MouseButton button)
	{
		Objects.requireNonNull(button, "button == null");
		robot.mousePress(InputEvent.getMaskForButton(button.getId()));
		robot.waitForIdle();
	}

	@Override
	public void buttonUp(MouseButton button)
	{
		Objects.requireNonNull(button, "button == null");
		robot.mouseRelease(InputEvent.getMaskForButton(button.getId()));
		robot.waitForIdle();
	}

	@Override
	public void click(MouseButton button)
	{
		Objects.requireNonNull(button, "button == null");
		buttonDown(button);
		robotDelay();
		buttonUp(button);
	}

	@Override
	public void doubleClick(MouseButton button)
	{
		Objects.requireNonNull(button, "button == null");
		click(button);
		robotDelay();
		click(button);
	}

	@Override
	public void scrollWheel(int relativeAmount)
	{
		robot.mouseWheel(relativeAmount);
		robot.waitForIdle();
	}
	
	private void robotDelay()
	{
		robot.delay(clickDelay);
	}
}
