package au.net.causal.peach.event;

import java.util.Set;

import au.net.causal.peach.Key;
import au.net.causal.peach.KeyboardState;

public abstract class ConstrainedKeyAdapter
{
	private final KeyEventConstraints constraints;
	
	protected ConstrainedKeyAdapter(KeyEventConstraints constraints)
	{
		if (constraints == null)
			this.constraints = KeyEventConstraints.NONE;
		else
			this.constraints = constraints;
	}
	
	protected boolean accept(KeyEvent event)
	{
		//Key check
		if (constraints.getKeys() != null)
		{
			if (!constraints.getKeys().contains(event.getKey()))
				return(false);
		}
		
		//Modifier check
		if (constraints.getModifiers() != null)
		{
			KeyboardState state = event.getKeyboardState();
			
			for (Set<? extends Key> keySet : constraints.getModifiers())
			{
				if (!state.areAnyDown(keySet))
					return(false);
			}
		}
		
		return(true);
	}

}
