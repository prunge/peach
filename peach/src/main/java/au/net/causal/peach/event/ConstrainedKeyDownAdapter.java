package au.net.causal.peach.event;

public class ConstrainedKeyDownAdapter
extends ConstrainedKeyAdapter
implements KeyDownListener
{
	private final KeyDownListener listener;
	
	public ConstrainedKeyDownAdapter(KeyDownListener listener, KeyEventConstraints constraints)
	{
		super(constraints);
		this.listener = listener;
	}
	
	@Override
	public void keyDown(KeyEvent event)
	{
		if (!accept(event))
			return;
		
		listener.keyDown(event);
	}
}
