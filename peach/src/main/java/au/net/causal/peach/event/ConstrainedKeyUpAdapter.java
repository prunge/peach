package au.net.causal.peach.event;

public class ConstrainedKeyUpAdapter
extends ConstrainedKeyAdapter
implements KeyUpListener
{
	private final KeyUpListener listener;
	
	public ConstrainedKeyUpAdapter(KeyUpListener listener, KeyEventConstraints constraints)
	{
		super(constraints);
		this.listener = listener;
	}
	
	@Override
	public void keyUp(KeyEvent event)
	{
		if (!accept(event))
			return;
		
		listener.keyUp(event);
	}
}