package au.net.causal.peach.event;

public class ConstrainedMouseDownAdapter implements MouseDownListener
{
	private final MouseDownListener listener;
	private final MouseEventConstraints constraints;
	
	public ConstrainedMouseDownAdapter(MouseDownListener listener, MouseEventConstraints constraints)
	{
		this.listener = listener;
		
		if (constraints == null)
			this.constraints = MouseEventConstraints.NONE;
		else
			this.constraints = constraints;
	}

	@Override
	public void mouseDown(MouseButtonEvent event)
	{
		if (constraints.getButtons() != null)
		{
			if (!constraints.getButtons().contains(event.getButton()))
				return;
		}
		
		listener.mouseDown(event);
	}

}
