package au.net.causal.peach.event;

public class ConstrainedMouseUpAdapter implements MouseUpListener
{
	private final MouseUpListener listener;
	private final MouseEventConstraints constraints;
	
	public ConstrainedMouseUpAdapter(MouseUpListener listener, MouseEventConstraints constraints)
	{
		this.listener = listener;
		
		if (constraints == null)
			this.constraints = MouseEventConstraints.NONE;
		else
			this.constraints = constraints;
	}

	@Override
	public void mouseUp(MouseButtonEvent event)
	{
		if (constraints.getButtons() != null)
		{
			if (!constraints.getButtons().contains(event.getButton()))
				return;
		}
		
		listener.mouseUp(event);
	}

}
