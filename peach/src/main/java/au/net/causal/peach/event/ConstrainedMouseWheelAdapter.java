package au.net.causal.peach.event;

public class ConstrainedMouseWheelAdapter implements MouseWheelListener
{
	private final MouseWheelListener listener;
	private final MouseWheelEventConstraints constraints;
	
	public ConstrainedMouseWheelAdapter(MouseWheelListener listener, MouseWheelEventConstraints constraints)
	{
		this.listener = listener;
		
		if (constraints == null)
			this.constraints = MouseWheelEventConstraints.NONE;
		else
			this.constraints = constraints;
	}
	
	@Override
	public void mouseWheelMoved(MouseWheelEvent event)
	{
		if (constraints.getDirection() != null && event.getDirection() != constraints.getDirection())
			return;
		
		listener.mouseWheelMoved(event);
	}
}
