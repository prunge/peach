package au.net.causal.peach.event;

import java.util.EventObject;

public abstract class ConsumableEvent extends EventObject
{
	private boolean consumed;
	
	protected ConsumableEvent(Object source)
	{
		super(source);
	}
	
	public void consume()
	{
		this.consumed = true;
	}
	
	public boolean isConsumed()
	{
		return(consumed);
	}
}
