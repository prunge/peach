package au.net.causal.peach.event;

import java.util.EventListener;
import java.util.EventObject;

@FunctionalInterface
public interface GenericEventListener extends EventListener
{
	public void onEvent(EventObject event);
}
