package au.net.causal.peach.event;

import java.util.EventListener;

@FunctionalInterface
public interface KeyDownListener extends EventListener
{
	public void keyDown(KeyEvent event);
}
