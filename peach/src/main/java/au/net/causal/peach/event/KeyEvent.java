package au.net.causal.peach.event;

import au.net.causal.peach.Key;
import au.net.causal.peach.KeyboardState;
import au.net.causal.peach.MouseState;

public class KeyEvent extends ConsumableEvent
{
	private final Key key;
	private final long timestamp;
	private final KeyboardState keyboardState;
	private final MouseState mouseState;
	
	public KeyEvent(Object source, Key key, long timestamp, KeyboardState keyboardState, MouseState mouseState)
	{
		super(source);
		this.key = key;
		this.timestamp = timestamp;
		this.keyboardState = keyboardState;
		this.mouseState = mouseState;
	}

	public Key getKey()
	{
		return(key);
	}

	public long getTimestamp()
	{
		return(timestamp);
	}
	
	public KeyboardState getKeyboardState()
	{
		return(keyboardState);
	}
	
	public MouseState getMouseState()
	{
		return(mouseState);
	}

	@Override
	public String toString()
	{
		return "KeyEvent [key=" + key + ", timestamp=" + timestamp + "]";
	}
}
