package au.net.causal.peach.event;

import java.beans.ConstructorProperties;
import java.util.Collections;
import java.util.Set;

import au.net.causal.peach.Key;
import au.net.causal.peach.support.Alias;

import com.google.common.collect.ImmutableSet;

public class KeyEventConstraints
{
	public static final KeyEventConstraints NONE = new KeyEventConstraints(null, null);
	
	private final Set<Key> keys;
	private final Set<Set<? extends Key>> modifiers;
	
	@ConstructorProperties({"keys", "modifiers"})
	public KeyEventConstraints(@Alias("key") Set<? extends Key> keys, @Alias("modifier") Set<Set<? extends Key>> modifiers)
	{
		if (keys == null)
			this.keys = null;
		else
			this.keys = ImmutableSet.copyOf(keys);
		
		if (modifiers == null)
			this.modifiers = null;
		else
			this.modifiers = ImmutableSet.copyOf(modifiers);
	}
	
	public KeyEventConstraints(Set<Key> keys)
	{
		this(keys, Collections.emptySet());
	}
	
	public Set<Key> getKeys()
	{
		return(keys);
	}
	
	public Set<Set<? extends Key>> getModifiers()
	{
		return(modifiers);
	}

	@Override
	public String toString()
	{
		return "KeyEventConstraints [keys=" + keys + ", modifiers=" + modifiers + "]";
	}
}
