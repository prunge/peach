package au.net.causal.peach.event;

import java.util.EventListener;

@FunctionalInterface
public interface KeyUpListener extends EventListener
{
	public void keyUp(KeyEvent event);
}
