package au.net.causal.peach.event;

public class MouseButton
{
	public static final MouseButton LEFT = new MouseButton(1, "left");
	public static final MouseButton MIDDLE = new MouseButton(2, "middle");
	public static final MouseButton RIGHT = new MouseButton(3, "right");
	
	private final int id;
	private final String name;
	
	public MouseButton(int id)
	{
		this(id, null);
	}
	
	public MouseButton(int id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	public int getId()
	{
		return(id);
	}
	
	public String getName()
	{
		return(name);
	}
	
	@Override
	public String toString()
	{
		return("MouseButton[" + (getName() == null ? getId() : getName()) + "]");
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MouseButton other = (MouseButton)obj;
		if (id != other.id)
			return false;
		return true;
	}
}
