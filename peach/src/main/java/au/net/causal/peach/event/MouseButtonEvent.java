package au.net.causal.peach.event;

import au.net.causal.peach.KeyboardState;
import au.net.causal.peach.MouseState;

public class MouseButtonEvent extends ConsumableEvent
{
	private final int x;
	private final int y;
	private final MouseButton button;
	private final long timestamp;
	private final KeyboardState keyboardState;
	private final MouseState mouseState;
	
	public MouseButtonEvent(Object source, int x, int y, MouseButton button, long timestamp, KeyboardState keyboardState, MouseState mouseState)
	{
		super(source);
		this.x = x;
		this.y = y;
		this.button = button;
		this.timestamp = timestamp;
		this.keyboardState = keyboardState;
		this.mouseState = mouseState;
	}

	public int getX()
	{
		return(x);
	}

	public int getY()
	{
		return(y);
	}

	public MouseButton getButton()
	{
		return(button);
	}
	
	public KeyboardState getKeyboardState()
	{
		return(keyboardState);
	}
	
	public MouseState getMouseState()
	{
		return(mouseState);
	}

	public long getTimestamp()
	{
		return(timestamp);
	}

	@Override
	public String toString()
	{
		return "MouseClickEvent [source=" + getSource() + ",x=" + x + ", y=" + y + ", button=" + button + ", timestamp=" + timestamp + "]";
	}
	
	
}
