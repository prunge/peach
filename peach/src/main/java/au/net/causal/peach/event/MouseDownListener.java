package au.net.causal.peach.event;

import java.util.EventListener;

@FunctionalInterface
public interface MouseDownListener extends EventListener
{
	public void mouseDown(MouseButtonEvent event);
}
