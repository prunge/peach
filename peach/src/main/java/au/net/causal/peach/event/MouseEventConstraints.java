package au.net.causal.peach.event;

import java.beans.ConstructorProperties;
import java.util.Set;

import au.net.causal.peach.support.Alias;

public class MouseEventConstraints
{
	public static final MouseEventConstraints NONE = new MouseEventConstraints(null);
	
	private final Set<MouseButton> buttons;
	
	@ConstructorProperties("buttons")
	public MouseEventConstraints(@Alias("button") Set<MouseButton> buttons)
	{
		this.buttons = buttons;
	}
	
	public Set<MouseButton> getButtons()
	{
		return(buttons);
	}

	@Override
	public String toString()
	{
		return "MouseEventConstraints [buttons=" + buttons + "]";
	}
}
