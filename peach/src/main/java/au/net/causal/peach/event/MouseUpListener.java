package au.net.causal.peach.event;

import java.util.EventListener;

@FunctionalInterface
public interface MouseUpListener extends EventListener
{
	public void mouseUp(MouseButtonEvent event);
}
