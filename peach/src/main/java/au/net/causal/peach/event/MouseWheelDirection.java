package au.net.causal.peach.event;

public enum MouseWheelDirection
{
	UP,
	DOWN;
}
