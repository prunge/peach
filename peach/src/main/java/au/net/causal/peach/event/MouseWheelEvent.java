package au.net.causal.peach.event;

import au.net.causal.peach.KeyboardState;
import au.net.causal.peach.MouseState;

public class MouseWheelEvent extends ConsumableEvent
{
	private final int x;
	private final int y;
	private final int steps;
	private final double pixels;
	private final long timestamp;
	private final KeyboardState keyboardState;
	private final MouseState mouseState;
	
	public MouseWheelEvent(Object source, int x, int y, int steps, double pixels, long timestamp, KeyboardState keyboardState, MouseState mouseState)
	{
		super(source);
		this.x = x;
		this.y = y;
		this.steps = steps;
		this.pixels = pixels;
		this.timestamp = timestamp;
		this.keyboardState = keyboardState;
		this.mouseState = mouseState;
	}

	public int getX()
	{
		return(x);
	}

	public int getY()
	{
		return(y);
	}
	
	public int getRelativeSteps()
	{
		return(Math.abs(steps));
	}
	
	public double getRelativePixels()
	{
		return(Math.abs(pixels));
	}
	
	public MouseWheelDirection getDirection()
	{
		if (steps < 0 || pixels < 0.0)
			return(MouseWheelDirection.UP);
		else
			return(MouseWheelDirection.DOWN);
	}

	public int getSteps()
	{
		return(steps);
	}
	
	public double getPixels()
	{
		return(pixels);
	}
	
	public long getTimestamp()
	{
		return(timestamp);
	}
	
	public KeyboardState getKeyboardState()
	{
		return(keyboardState);
	}
	
	public MouseState getMouseState()
	{
		return(mouseState);
	}

	@Override
	public String toString()
	{
		return "MouseWheelEvent [source=" + getSource() + ",x=" + x + ", y=" + y + ", steps=" + steps + ", pixels=" + pixels + ", timestamp=" + timestamp + "]";
	}
	
	
}
