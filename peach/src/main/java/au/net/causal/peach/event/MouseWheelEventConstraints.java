package au.net.causal.peach.event;

import java.beans.ConstructorProperties;

public class MouseWheelEventConstraints
{
	public static final MouseWheelEventConstraints NONE = new MouseWheelEventConstraints(null);
	
	private final MouseWheelDirection direction;
	
	@ConstructorProperties("direction")
	public MouseWheelEventConstraints(MouseWheelDirection direction)
	{
		this.direction = direction;
	}
	
	public MouseWheelDirection getDirection()
	{
		return(direction);
	}
	
	@Override
	public String toString()
	{
		return "MouseWheelEventConstraints [direction=" + direction + "]";
	}
}
