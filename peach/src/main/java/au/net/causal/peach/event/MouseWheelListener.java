package au.net.causal.peach.event;

import java.util.EventListener;

@FunctionalInterface
public interface MouseWheelListener extends EventListener
{
	public void mouseWheelMoved(MouseWheelEvent event);
}
