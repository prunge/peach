package au.net.causal.peach.event;

import java.beans.ConstructorProperties;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;

import au.net.causal.peach.Key;
import au.net.causal.peach.Keyboard;
import au.net.causal.peach.support.Alias;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableSet;

public class StringifiedKeyEventConstraints
{
	private final Set<String> keys;
	private final Set<String> modifiers;
	
	/**
	 * Expects the form: <code>[modifier]+ key</code>.
	 * 
	 * @param s the string to parse.
	 * 
	 * @return the parsed constraints.
	 */
	public static StringifiedKeyEventConstraints parse(CharSequence s)
	{
		if (s == null || s.length() == 0)
			return(null);
		
		ImmutableSet.Builder<String> modifierBuilder = ImmutableSet.builder();
		String lastKey = null;
		
		for (Iterator<String> i = Splitter.on(CharMatcher.WHITESPACE).omitEmptyStrings().split(s).iterator(); i.hasNext();)
		{
			String token = i.next();
			
			//Not the last one
			if (i.hasNext())
				modifierBuilder.add(token);
			else
				lastKey = token;
		}
		
		if (lastKey == null)
			return(null);
		else
			return(new StringifiedKeyEventConstraints(ImmutableSet.of(lastKey), modifierBuilder.build()));
	}
	
	@ConstructorProperties({"keys", "modifiers"})
	public StringifiedKeyEventConstraints(@Alias("key") Set<String> keys, @Alias("modifier") Set<String> modifiers)
	{
		Objects.requireNonNull(keys, "keys == null");
		
		this.keys = ImmutableSet.copyOf(keys);
		
		if (modifiers == null)
			this.modifiers = null;
		else
			this.modifiers = ImmutableSet.copyOf(modifiers);
	}

	public Set<String> getKeys()
	{
		return(keys);
	}

	public Set<String> getModifiers()
	{
		return(modifiers);
	}
	
	public KeyEventConstraints toKeyEventConstraints(Keyboard keyboard)
	{
		Objects.requireNonNull(keyboard, "keyboard == null");
		
		ImmutableSet.Builder<Set<? extends Key>> modifierBuilder = ImmutableSet.builder();
		ImmutableSet.Builder<Key> keyBuilder = ImmutableSet.builder();
		
		for (String modifier : getModifiers())
		{
			Set<? extends Key> modifierKeys = keyboard.getAll(modifier);
			if (modifierKeys.isEmpty())
				return(null); //User specified modifier that we couldn't find on the keyboard
			
			modifierBuilder.add(modifierKeys);
		}
		
		for (String key : getKeys())
		{
			Set<? extends Key> keyboardKeys = keyboard.getAll(key);
			if (keyboardKeys.isEmpty())
				return(null); //User specified key that we couldn't find on the keyboard
			
			keyBuilder.addAll(keyboardKeys);
		}
		
		Set<Key> keys = keyBuilder.build();
		
		return(new KeyEventConstraints(keys, modifierBuilder.build()));
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((keys == null) ? 0 : keys.hashCode());
		result = prime * result + ((modifiers == null) ? 0 : modifiers.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StringifiedKeyEventConstraints other = (StringifiedKeyEventConstraints)obj;
		if (keys == null)
		{
			if (other.keys != null)
				return false;
		}
		else if (!keys.equals(other.keys))
			return false;
		if (modifiers == null)
		{
			if (other.modifiers != null)
				return false;
		}
		else if (!modifiers.equals(other.modifiers))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "StringifiedKeyEventConstraints [keys=" + keys + ", modifiers=" + modifiers + "]";
	}
}
