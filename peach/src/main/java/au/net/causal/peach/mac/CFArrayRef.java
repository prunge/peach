package au.net.causal.peach.mac;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

public class CFArrayRef extends PointerByReference
{
    public CFArrayRef()
    {
    }

    public CFArrayRef(Pointer pointer)
    {
        super(pointer);
    }

    public int getCount()
    {
        return(CoreFoundation.INSTANCE.CFArrayGetCount(this));
    }

    public Pointer get(int i)
    {
        return(CoreFoundation.INSTANCE.CFArrayGetValueAtIndex(this, i));
    }
}
