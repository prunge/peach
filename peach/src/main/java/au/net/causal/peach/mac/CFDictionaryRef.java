package au.net.causal.peach.mac;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

public class CFDictionaryRef extends PointerByReference
{
    public CFDictionaryRef()
    {
    }

    public CFDictionaryRef(Pointer pointer)
    {
        super(pointer);
    }

    public Pointer get(Pointer key)
    {
        return(CoreFoundation.INSTANCE.CFDictionaryGetValue(this.getValue(), key));
    }
}
