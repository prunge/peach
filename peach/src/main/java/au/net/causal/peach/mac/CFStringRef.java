package au.net.causal.peach.mac;

import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

public class CFStringRef extends PointerByReference
{
    public CFStringRef()
    {
    }

    public CFStringRef(Pointer pointer)
    {
        super(pointer);
    }

    public String getStringValue()
    {
        return(getStringFromCFStringRef(this.getValue()));
    }

    private static String getStringFromCFStringRef(Pointer cfStringPointer)
    {
        if (cfStringPointer != null)
        {
            long length = CoreFoundation.INSTANCE.CFStringGetLength(cfStringPointer);
            if (length == 0)
                return("");

            long maxSize = CoreFoundation.INSTANCE.CFStringGetMaximumSizeForEncoding(length, 0x08000100); //UTF-8
            Pointer buf = new Memory(maxSize);
            if (CoreFoundation.INSTANCE.CFStringGetCString(cfStringPointer, buf, maxSize, 0x08000100))
            {
                return(buf.getString(0L));
            }
        }

        return(null);
    }
}
