package au.net.causal.peach.mac;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

public class CGEventRef extends PointerByReference
{
    public CGEventRef()
    {
    }

    public CGEventRef(Pointer pointer)
    {
        super(pointer);
    }
}
