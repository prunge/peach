package au.net.causal.peach.mac;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

public class CGEventSourceRef extends PointerByReference
{
    public CGEventSourceRef()
    {
    }

    public CGEventSourceRef(Pointer pointer)
    {
        super(pointer);
    }
}
