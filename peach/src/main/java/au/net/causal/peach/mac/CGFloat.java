package au.net.causal.peach.mac;

import com.sun.jna.FromNativeContext;
import com.sun.jna.Native;
import com.sun.jna.NativeMapped;

public class CGFloat extends Number implements NativeMapped
{
	private final double value;
	
	public CGFloat()
	{
		this(0.0);
	}
	
	public CGFloat(double value)
	{
		this.value = value;
	}

	@Override
	public Object fromNative(Object nativeValue, FromNativeContext context)
	{
		switch (Native.LONG_SIZE)
		{
			case Long.BYTES: //64-bit
				return(new CGFloat((Double)nativeValue));
			case Integer.BYTES: //32-bit
				return(new CGFloat((Float)nativeValue));
			default:
				throw new Error("Unknown long size: " + Native.LONG_SIZE);
		}
	}

	@Override
	public Object toNative()
	{
		switch (Native.LONG_SIZE)
		{
			case Long.BYTES: //64-bit
				return(Double.valueOf(value));
			case Integer.BYTES: //32-bit
				return(Float.valueOf((float)value));
			default:
				throw new Error("Unknown long size: " + Native.LONG_SIZE);
		}
	}

	@Override
	public Class nativeType()
	{
		switch (Native.LONG_SIZE)
		{
			case Long.BYTES: //64-bit
				return(Double.class);
			case Integer.BYTES: //32-bit
				return(Float.class);
			default:
				throw new Error("Unknown long size: " + Native.LONG_SIZE);
		}
	}

	@Override
	public int intValue()
	{
		return((int)value);
	}

	@Override
	public long longValue()
	{
		return((long)value);
	}

	@Override
	public float floatValue()
	{
		return((float)value);
	}

	@Override
	public double doubleValue()
	{
		return(value);
	}

	@Override
	public String toString()
	{
		return(String.valueOf(value));
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(value);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CGFloat other = (CGFloat) obj;
		if (Double.doubleToLongBits(value) != Double.doubleToLongBits(other.value))
			return false;
		return true;
	}
}
