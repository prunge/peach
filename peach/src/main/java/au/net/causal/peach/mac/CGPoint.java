package au.net.causal.peach.mac;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

public class CGPoint extends Structure
{
	public CGFloat x;
	public CGFloat y;
	
	public CGPoint()
	{
	}
	
	public CGPoint(Pointer pointer)
	{
		super(pointer);
		super.read();
	}
	
	@Override
	protected List<String> getFieldOrder()
	{
		return(Arrays.asList("x", "y"));
	}

	@Override
	public String toString()
	{
		return "CGPoint [x=" + x + ", y=" + y + "]";
	}
	
	public static class ByValue extends CGPoint implements Structure.ByValue
	{
		public ByValue()
		{
		}
	}
	
	public static class ByReference extends CGPoint implements Structure.ByReference
	{
		public ByReference()
		{
		}
		
		public ByReference(Pointer pointer)
		{
			super(pointer);
		}
	}
}
