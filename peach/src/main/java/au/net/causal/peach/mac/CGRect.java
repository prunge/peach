package au.net.causal.peach.mac;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

public class CGRect extends Structure
{
	public CGPoint origin;
	public CGSize size;
	
	public CGRect()
	{
	}
	
	public CGRect(Pointer pointer)
	{
		super(pointer);
		super.read();
	}
	
	@Override
	protected List<String> getFieldOrder()
	{
		return(Arrays.asList("origin", "size"));
	}
	
	@Override
	public String toString()
	{
		return "CGRect [origin=" + origin + ", size=" + size + "]";
	}



	public static class ByValue extends CGRect implements Structure.ByValue
	{
		
	}
	
	public static class ByReference extends CGRect implements Structure.ByReference
	{
		public ByReference()
		{
		}
		
		public ByReference(Pointer pointer)
		{
			super(pointer);
		}
	}
}
