package au.net.causal.peach.mac;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

public class CGSize extends Structure
{
	public CGFloat width;
	public CGFloat height;
	
	public CGSize()
	{
	}
	
	public CGSize(Pointer pointer)
	{
		super(pointer);
		super.read();
	}
	
	@Override
	protected List<String> getFieldOrder()
	{
		return(Arrays.asList("width", "height"));
	}

	@Override
	public String toString()
	{
		return "CGSize [height=" + height + ", width=" + width + "]";
	}
	
	public static class ByValue extends CGSize implements Structure.ByValue
	{
		public ByValue()
		{
		}
	}
	
	public static class ByReference extends CGSize implements Structure.ByReference
	{
		public ByReference()
		{
		}
		
		public ByReference(Pointer pointer)
		{
			super(pointer);
		}
	}
}
