package au.net.causal.peach.mac;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;
import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.LongByReference;
import com.sun.jna.ptr.PointerByReference;

public interface CoreFoundation extends Library
{
	public static final CoreFoundation INSTANCE = (CoreFoundation)Native.loadLibrary("CoreFoundation", CoreFoundation.class);

	public CFArrayRef CFArrayCreate (Pointer allocator, int[] values, NativeLong numValues, Pointer callBacks);
    public int CFArrayGetCount(CFArrayRef theArray);
    public Pointer CFArrayGetValueAtIndex(CFArrayRef theArray, int idx);

    public Pointer CFDictionaryGetValue(Pointer dict, Pointer key);
    public int CFDictionaryGetCount(Pointer dict);
    public void CFDictionaryGetKeysAndValues(Pointer dict, Pointer[] keys, Pointer[] values);

    public CFDictionaryRef CFDictionaryCreate(Pointer allocator, Pointer[] keys, Pointer[] values, int numValues, Pointer keyCallBacks, Pointer valueCallBacks);

    public long CFStringGetLength(Pointer cfStringRef);
    public long CFStringGetMaximumSizeForEncoding(long length, int encoding);

    public boolean CFStringGetCString(Pointer cfStringRef, Pointer buffer, long maxSize, int encoding);
    public CFStringRef CFStringCreateWithCharacters(Pointer alloc, char[] source, long length);

    public boolean CFNumberGetValue(Pointer number, int type, LongByReference valuePtr);

    public Pointer CFMachPortCreateRunLoopSource(Pointer allocator, Pointer port, int order);
    public void CFRunLoopAddSource(Pointer rl, Pointer source, Pointer mode);
    public Pointer CFRunLoopGetCurrent();
    public void CFRunLoopRun();

    public CFStringRef CFURLGetString(Pointer anURL);

    public void CFRelease(PointerByReference s);

    public static final class Constants
    {
        private static final NativeLibrary CG_NATIVE_LIBRARY = NativeLibrary.getInstance("CoreFoundation");

        public static final Pointer kCFRunLoopCommonModes = getKeyCFStringRef("kCFRunLoopCommonModes");
        public static final Pointer kCFAllocatorDefault = getKeyCFStringRef("kCFAllocatorDefault");

        public static final Pointer kCFBooleanTrue = getKeyCFStringRef("kCFBooleanTrue");
        public static final Pointer kCFBooleanFalse = getKeyCFStringRef("kCFBooleanFalse");

        private static Pointer getKeyCFStringRef(String key)
        {
            return(CG_NATIVE_LIBRARY.getGlobalVariableAddress(key).getPointer(0L));
        }
    }
}
