package au.net.causal.peach.mac;

import com.sun.jna.Callback;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;
import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.NativeLongByReference;

public interface CoreGraphics extends Library
{
	public static final CoreGraphics INSTANCE = (CoreGraphics)Native.loadLibrary("CoreGraphics", CoreGraphics.class);

    public CFArrayRef CGWindowListCreate(int option, int relativeToWindow);
    public CFArrayRef CGWindowListCopyWindowInfo(int option, int relativeToWindow);
    public CFArrayRef CGWindowListCreateDescriptionFromArray(CFArrayRef windowArray);
    
    public Pointer /* CFMachPortRef */ CGEventTapCreate(int tap, int place, int options, long eventsOfInterest, CGEventTapCallBack callback, Pointer refcon);
    public void CGEventTapEnable(Pointer myTap, boolean enable);
    public boolean CGEventTapIsEnabled(Pointer myTap);

    public long CGEventGetTimestamp(Pointer event);
    public long CGEventGetFlags(Pointer event);
    public void CGEventSetFlags(CGEventRef event, long flags);
    public CGPoint.ByValue CGEventGetLocation(Pointer event);
    public void CGEventSetLocation(CGEventRef event, CGPoint location);
    public void CGEventKeyboardGetUnicodeString(Pointer event, NativeLong maxStringLength, NativeLongByReference actualStringLength, char[] unicodeString);

    public long CGEventGetIntegerValueField(Pointer event, int field);
    public void CGEventSetIntegerValueField(CGEventRef event, int field, long value);
    
    public double CGEventGetDoubleValueField (Pointer event, int field);
    public void CGEventSetDoubleValueField(CGEventRef event, int field, double value);

    /* Actually a boolean instead of a btye according to the spec but this is not interpreted by JNA properly */
    public byte CGEventSourceKeyState(int sourceState, short key);
    /* Actually a boolean instead of a byte according to the spec but this is not interpreted by JNA properly */
    public byte CGEventSourceButtonState(int sourceState, int button);
    public long CGEventSourceFlagsState(int sourceState);

    /**
     * For some reason JNA does not interpret boolean return values correctly so define them as bytes and convert them
     * ourselves.
     */
    public static boolean byteAsBoolean(byte value)
    {
        return (value & 0x01) != 0;
    }
    
    public boolean CGRectMakeWithDictionaryRepresentation(Pointer dict, CGRect.ByReference rect);
    
    public void CGEventPost(int tap, CGEventRef event);
    public CGEventRef CGEventCreate(CGEventSourceRef source);
    public CGEventSourceRef CGEventSourceCreate(int sourceState);
    public CGEventRef CGEventCreateKeyboardEvent(CGEventSourceRef source, short virtualKey, boolean keyDown);
    
    public void CGEventSetType(CGEventRef event, int type);
    
    public static final class CGEventTapLocation
    {
    	private CGEventTapLocation()
    	{
    	}
    	
    	public static final int kCGHIDEventTap = 0;
    	public static final int kCGSessionEventTap = 1;
    	public static final int kCGAnnotatedSessionEventTap = 2;
    }
    
    public static final class CGEventTapPlacement
    {
    	private CGEventTapPlacement()
    	{
    	}
    	
    	public static final int kCGHeadInsertEventTap = 0;
    	public static final int kCGTailAppendEventTap = 1;
    }
    
    public static final class CGEventTapOptions
    {
    	private CGEventTapOptions()
    	{
    	}
    	
    	public static final int kCGEventTapOptionDefault = 0;
    	public static final int kCGEventTapOptionListenOnly = 1;
    }
    
    public static final class CGEventField
    {
    	private CGEventField()
    	{
    	}
    	
        public static final int kCGMouseEventNumber = 0;
        public static final int kCGMouseEventClickState = 1;
        public static final int kCGMouseEventPressure = 2;
        public static final int kCGMouseEventButtonNumber = 3;
        public static final int kCGMouseEventDeltaX = 4;
        public static final int kCGMouseEventDeltaY = 5;
        public static final int kCGMouseEventInstantMouser = 6;
        public static final int kCGMouseEventSubtype = 7;
        public static final int kCGKeyboardEventAutorepeat = 8;
        public static final int kCGKeyboardEventKeycode = 9;
        public static final int kCGKeyboardEventKeyboardType = 10;
        public static final int kCGScrollWheelEventDeltaAxis1 = 11;
        public static final int kCGScrollWheelEventDeltaAxis2 = 12;
        public static final int kCGScrollWheelEventDeltaAxis3 = 13;
        public static final int kCGScrollWheelEventFixedPtDeltaAxis1 = 93;
        public static final int kCGScrollWheelEventFixedPtDeltaAxis2 = 94;
        public static final int kCGScrollWheelEventFixedPtDeltaAxis3 = 95;
        public static final int kCGScrollWheelEventPointDeltaAxis1 = 96;
        public static final int kCGScrollWheelEventPointDeltaAxis2 = 97;
        public static final int kCGScrollWheelEventPointDeltaAxis3 = 98;
        public static final int kCGScrollWheelEventInstantMouser = 14;
        public static final int kCGTabletEventPointX = 15;
        public static final int kCGTabletEventPointY = 16;
        public static final int kCGTabletEventPointZ = 17;
        public static final int kCGTabletEventPointButtons = 18;
        public static final int kCGTabletEventPointPressure = 19;
        public static final int kCGTabletEventTiltX = 20;
        public static final int kCGTabletEventTiltY = 21;
        public static final int kCGTabletEventRotation = 22;
        public static final int kCGTabletEventTangentialPressure = 23;
        public static final int kCGTabletEventDeviceID = 24;
        public static final int kCGTabletEventVendor1 = 25;
        public static final int kCGTabletEventVendor2 = 26;
        public static final int kCGTabletEventVendor3 = 27;
        public static final int kCGTabletProximityEventVendorID = 28;
        public static final int kCGTabletProximityEventTabletID = 29;
        public static final int kCGTabletProximityEventPointerID = 30;
        public static final int kCGTabletProximityEventDeviceID = 31;
        public static final int kCGTabletProximityEventSystemTabletID = 32;
        public static final int kCGTabletProximityEventVendorPointerType = 33;
        public static final int kCGTabletProximityEventVendorPointerSerialNumber = 34;
        public static final int kCGTabletProximityEventVendorUniqueID = 35;
        public static final int kCGTabletProximityEventCapabilityMask = 36;
        public static final int kCGTabletProximityEventPointerType = 37;
        public static final int kCGTabletProximityEventEnterProximity = 38;
        public static final int kCGEventTargetProcessSerialNumber = 39;
        public static final int kCGEventTargetUnixProcessID = 40;
        public static final int kCGEventSourceUnixProcessID = 41;
        public static final int kCGEventSourceUserData = 42;
        public static final int kCGEventSourceUserID = 43;
        public static final int kCGEventSourceGroupID = 44;
        public static final int kCGEventSourceStateID = 45;
        public static final int kCGScrollWheelEventIsContinuous = 88;
    }
    
    public static final class CGEventSourceStateID
    {
    	private CGEventSourceStateID()
    	{
    	}
    	
    	public static final int kCGEventSourceStatePrivate = -1;
    	public static final int kCGEventSourceStateCombinedSessionState  = 0;
    	public static final int kCGEventSourceStateHIDSystemState = 1;
    }
    
    public static final class CGEventType
    {
    	private CGEventType()
    	{
    	}
    	
    	public static final int kCGEventNull = 0;
    	public static final int kCGEventLeftMouseDown = 1;
    	public static final int kCGEventLeftMouseUp = 2;
    	public static final int kCGEventRightMouseDown = 3;
    	public static final int kCGEventRightMouseUp = 4;
    	public static final int kCGEventMouseMoved = 5;
    	public static final int kCGEventLeftMouseDragged = 6;
    	public static final int kCGEventRightMouseDragged = 7;
    	public static final int kCGEventKeyDown = 10;
    	public static final int kCGEventKeyUp = 11;
    	public static final int kCGEventFlagsChanged = 12;
    	public static final int kCGEventScrollWheel = 22;
    	public static final int kCGEventTabletPointer = 23;
    	public static final int kCGEventTabletProximity = 24;
    	public static final int kCGEventOtherMouseDown = 25;
    	public static final int kCGEventOtherMouseUp = 26;
    	public static final int kCGEventOtherMouseDragged = 27;
    	public static final int kCGEventTapDisabledByTimeout = 0xFFFFFFFE;
    	public static final int kCGEventTapDisabledByUserInput = 0xFFFFFFFF;
    	
    	public static int mask(int... eventTypes)
    	{
    		int m = 0;
    		for (int eventType : eventTypes)
    		{
    			m |= (1 << eventType);
    		}
    		return(m);
    	}
    }
    
    public static final class CGMouseButton
    {
    	private CGMouseButton()
    	{
    	}
    	
    	public static final int kCGMouseButtonLeft = 0;
    	public static final int kCGMouseButtonRight = 1;
    	public static final int kCGMouseButtonCenter = 2;
    }
    
    public static final class CGEventFlags
    {
    	private CGEventFlags()
    	{
    	}
    	
    	public static final long kCGEventFlagMaskAlphaShift = 0x00010000;
    	public static final long kCGEventFlagMaskShift = 0x00020000;
    	public static final long kCGEventFlagMaskControl = 0x00040000;
    	public static final long kCGEventFlagMaskAlternate = 0x00080000;
    	public static final long kCGEventFlagMaskCommand = 0x00100000;
    	public static final long kCGEventFlagMaskHelp = 0x00400000;
    	public static final long kCGEventFlagMaskSecondaryFn = 0x00800000;
    	public static final long kCGEventFlagMaskNumericPad = 0x00200000;
    	public static final long kCGEventFlagMaskNonCoalesced = 0x00000100;
    	
    }

    public static interface CGEventTapCallBack extends Callback
    {
        public Pointer call(Pointer proxy, int type, Pointer event, Pointer refcon);
    }

    public static final class Constants
    {
        private static final NativeLibrary CG_NATIVE_LIBRARY = NativeLibrary.getInstance("CoreGraphics");

        public static final Pointer kCGWindowName = getKeyCFStringRef("kCGWindowName");
        public static final Pointer kCGWindowOwnerName = getKeyCFStringRef("kCGWindowOwnerName");
        public static final Pointer kCGWindowOwnerPID = getKeyCFStringRef("kCGWindowOwnerPID");
        public static final Pointer kCGWindowBounds = getKeyCFStringRef("kCGWindowBounds");

        private static Pointer getKeyCFStringRef(String key)
        {
            return(CG_NATIVE_LIBRARY.getGlobalVariableAddress(key).getPointer(0L));
        }

    }
}
