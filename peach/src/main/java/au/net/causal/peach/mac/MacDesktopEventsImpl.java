package au.net.causal.peach.mac;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;

import au.net.causal.peach.Key;
import au.net.causal.peach.Keyboard;
import au.net.causal.peach.event.ConstrainedKeyDownAdapter;
import au.net.causal.peach.event.ConstrainedKeyUpAdapter;
import au.net.causal.peach.event.ConstrainedMouseDownAdapter;
import au.net.causal.peach.event.ConstrainedMouseUpAdapter;
import au.net.causal.peach.event.ConstrainedMouseWheelAdapter;
import au.net.causal.peach.event.KeyDownListener;
import au.net.causal.peach.event.KeyEvent;
import au.net.causal.peach.event.KeyEventConstraints;
import au.net.causal.peach.event.KeyUpListener;
import au.net.causal.peach.event.MouseButton;
import au.net.causal.peach.event.MouseButtonEvent;
import au.net.causal.peach.event.MouseDownListener;
import au.net.causal.peach.event.MouseEventConstraints;
import au.net.causal.peach.event.MouseUpListener;
import au.net.causal.peach.event.MouseWheelEvent;
import au.net.causal.peach.event.MouseWheelEventConstraints;
import au.net.causal.peach.event.MouseWheelListener;
import au.net.causal.peach.event.StringifiedKeyEventConstraints;

import com.google.common.collect.ImmutableSet;
import com.sun.jna.Pointer;

public class MacDesktopEventsImpl implements MacDesktopEvents
{
	private static final Logger log = Logger.getLogger(MacDesktopEventsImpl.class.getCanonicalName());
	
	private static final CoreFoundation cf = CoreFoundation.INSTANCE;
    private static final CoreGraphics cg = CoreGraphics.INSTANCE;
    
    private Thread hookThread;
    private CountDownLatch hookThreadLatch = new CountDownLatch(1);

    private final List<MouseDownListener> mouseDownListenerList = new CopyOnWriteArrayList<>();
	private final List<MouseUpListener> mouseUpListenerList = new CopyOnWriteArrayList<>();
	private final List<MouseWheelListener> mouseWheelListenerList = new CopyOnWriteArrayList<>();
	
	private final List<KeyDownListener> keyDownListenerList = new CopyOnWriteArrayList<>();
	private final List<KeyUpListener> keyUpListenerList = new CopyOnWriteArrayList<>();
	
	private final Keyboard keyboard;
	private final MacMouse mouse;
	
	public MacDesktopEventsImpl(Keyboard keyboard, MacMouse mouse)
	{
		Objects.requireNonNull(keyboard, "keyboard == null");
		Objects.requireNonNull(mouse, "mouse == null");
		
		this.keyboard = keyboard;
		this.mouse = mouse;
	}
	
	protected boolean fireKeyPressed(int nativeKeyCode, long timestamp)
    {
    	Key key = keyboard.forCode(nativeKeyCode);
    	
    	KeyEvent event = new KeyEvent(this, key, timestamp, keyboard.getCurrentState(), mouse.getCurrentState());
    	
    	for (KeyDownListener listener : keyDownListenerList)
    	{
    		listener.keyDown(event);
    	}
    	
    	return(!event.isConsumed());
    }
    
    protected boolean fireKeyReleased(int nativeKeyCode, long timestamp)
    {
    	Key key = keyboard.forCode(nativeKeyCode);
    	
    	KeyEvent event = new KeyEvent(this, key, timestamp, keyboard.getCurrentState(), mouse.getCurrentState());
    	
    	for (KeyUpListener listener : keyUpListenerList)
    	{
    		listener.keyUp(event);
    	}
    	
    	return(!event.isConsumed());
    }
	
	protected boolean fireMouseWheelMoved(int x, int y, long timestamp, int steps, double pixels)
    {
    	MouseWheelEvent event = new MouseWheelEvent(this, x, y, steps, pixels, timestamp, keyboard.getCurrentState(), mouse.getCurrentState());
    	
    	for (MouseWheelListener listener : mouseWheelListenerList)
    	{
   			listener.mouseWheelMoved(event);
    	}
    	
    	return(!event.isConsumed());
    }
	
	protected boolean fireMouseDown(int x, int y, long timestamp, MouseButton button)
    {
		MouseButtonEvent event = new MouseButtonEvent(this, x, y, button, timestamp, keyboard.getCurrentState(), mouse.getCurrentState());
    	
    	for (MouseDownListener listener : mouseDownListenerList)
    	{
    		listener.mouseDown(event);
    	}
    	
    	return(!event.isConsumed());
    }
    
    protected boolean fireMouseUp(int x, int y, long timestamp, MouseButton button)
    {
    	MouseButtonEvent event = new MouseButtonEvent(this, x, y, button, timestamp, keyboard.getCurrentState(), mouse.getCurrentState());
    	
    	for (MouseUpListener listener : mouseUpListenerList)
    	{
    		listener.mouseUp(event);
    	}
    	
    	return(!event.isConsumed());
    }
	
	@Override
	public void clear()
	{
		mouseDownListenerList.clear();
		mouseUpListenerList.clear();
		mouseWheelListenerList.clear();
		keyDownListenerList.clear();
		keyUpListenerList.clear();
	}

	@Override
	public void mouseDown(MouseEventConstraints constraints, MouseDownListener listener)
	{
		Objects.requireNonNull(listener, "listener == null");
		
		if (!isStarted())
    		start();
		
		mouseDownListenerList.add(new ConstrainedMouseDownAdapter(listener, constraints));
	}
	
	@Override
	public void mouseUp(MouseEventConstraints constraints, MouseUpListener listener)
	{
		Objects.requireNonNull(listener, "listener == null");
		
		if (!isStarted())
    		start();
		
		mouseUpListenerList.add(new ConstrainedMouseUpAdapter(listener, constraints));
	}
	
	@Override
	public void mouseWheelMove(MouseWheelEventConstraints constraints, MouseWheelListener listener)
	{
		Objects.requireNonNull(listener, "listener == null");
		
		if (!isStarted())
    		start();
		
		mouseWheelListenerList.add(new ConstrainedMouseWheelAdapter(listener, constraints));
	}
	
	@Override
	public void keyDown(KeyEventConstraints constraints, KeyDownListener listener)
	{
		Objects.requireNonNull(listener, "listener == null");
		
		if (!isStarted())
    		start();
		
		keyDownListenerList.add(new ConstrainedKeyDownAdapter(listener, constraints));
	}
	
	@Override
	public void keyUp(KeyEventConstraints constraints, KeyUpListener listener)
	{
		Objects.requireNonNull(listener, "listener == null");
		
		if (!isStarted())
    		start();
		
		keyUpListenerList.add(new ConstrainedKeyUpAdapter(listener, constraints));
	}
	
	@Override
	public void keyDown(StringifiedKeyEventConstraints constraints, KeyDownListener listener)
	{
		KeyEventConstraints keyConstraints;
		if (constraints == null)
			keyConstraints = new KeyEventConstraints(ImmutableSet.of());
		else
			keyConstraints = constraints.toKeyEventConstraints(keyboard);
		
		if (keyConstraints != null)
			keyDown(keyConstraints, listener);
	}

	@Override
	public void keyUp(StringifiedKeyEventConstraints constraints, KeyUpListener listener)
	{
		KeyEventConstraints keyConstraints;
		if (constraints == null)
			keyConstraints = new KeyEventConstraints(ImmutableSet.of());
		else
			keyConstraints = constraints.toKeyEventConstraints(keyboard);
		
		if (keyConstraints != null)
			keyUp(keyConstraints, listener);
	}
	
	private synchronized boolean isStarted()
    {
    	return(hookThread != null);
    }
	
	private synchronized void start()
	{
		log.info("Start hook.");
		
		if (hookThread == null)
    	{
	        hookThread = new Thread("NativeHook Thread")
	        {
	        	@Override
	        	public void run()
	        	{
	        		boolean trusted = AXUIElement.INSTANCE.AXIsProcessTrusted();
	        		//Do this in 10.9
	                //CFDictionaryRef dict = CoreFoundation.INSTANCE.CFDictionaryCreate(CoreFoundation.Constants.kCFAllocatorDefault, new Pointer[] {AXUIElement.Constants.kAXTrustedCheckOptionPrompt}, new Pointer[] {CoreFoundation.Constants.kCFBooleanTrue}, 1, null, null);
	                //boolean trusted = AXUIElement.INSTANCE.AXIsProcessTrustedWithOptions(dict);
	        		log.info("Trusted: " + trusted);
	        		mainLoop();
	        	}
	        };
	        hookThread.start();
	        
	        try
	        {
	        	hookThreadLatch.await();
	        }
	        catch (InterruptedException e)
	        {
	        	log.log(Level.WARNING, "Interrupted waiting for hook thread.", e);
	        }
    	}
	}

	private void mainLoop()
    {
		int mask = CoreGraphics.CGEventType.mask(
						CoreGraphics.CGEventType.kCGEventScrollWheel,
						CoreGraphics.CGEventType.kCGEventLeftMouseDown,
						CoreGraphics.CGEventType.kCGEventLeftMouseUp,
						CoreGraphics.CGEventType.kCGEventRightMouseDown,
						CoreGraphics.CGEventType.kCGEventRightMouseUp,
						CoreGraphics.CGEventType.kCGEventOtherMouseDown,
						CoreGraphics.CGEventType.kCGEventOtherMouseUp,
						CoreGraphics.CGEventType.kCGEventKeyDown,
						CoreGraphics.CGEventType.kCGEventKeyUp,
						CoreGraphics.CGEventType.kCGEventFlagsChanged
						);
		
		Pointer eventTap = cg.CGEventTapCreate(CoreGraphics.CGEventTapLocation.kCGHIDEventTap, 
								CoreGraphics.CGEventTapPlacement.kCGHeadInsertEventTap, 
								CoreGraphics.CGEventTapOptions.kCGEventTapOptionDefault, 
								mask, 
			new CoreGraphics.CGEventTapCallBack()
	        {
	            @Override
	            public Pointer call(Pointer proxy, int type, Pointer event, Pointer refcon)
	            {
	                log.fine("Event: " + type);
	                
	            	boolean proceed;
	            	
	            	switch (type)
	            	{
	            		case CoreGraphics.CGEventType.kCGEventScrollWheel:
	            		{
	            			CGPoint location = cg.CGEventGetLocation(event);
		                	long timestamp = cg.CGEventGetTimestamp(event);
		                	
		                    long deltaWheel = cg.CGEventGetIntegerValueField(event, CoreGraphics.CGEventField.kCGScrollWheelEventDeltaAxis1);
		                    int x = location.x.intValue();
		                    int y = location.y.intValue();
		                    long deltaPixels = cg.CGEventGetIntegerValueField(event, CoreGraphics.CGEventField.kCGScrollWheelEventPointDeltaAxis1);
		                    
		                    proceed = fireMouseWheelMoved(x, y, timestamp, (int)-deltaWheel, -deltaPixels);
		                    
	            			break;
	            		}
	            		case CoreGraphics.CGEventType.kCGEventLeftMouseDown:
	            		{
	            			CGPoint location = cg.CGEventGetLocation(event);
		                	long timestamp = cg.CGEventGetTimestamp(event);
		                	int x = location.x.intValue();
		                    int y = location.y.intValue();
		                	proceed = fireMouseDown(x, y, timestamp, MouseButton.LEFT);
	            			break;
	            		}
	            		case CoreGraphics.CGEventType.kCGEventLeftMouseUp:
	            		{
	            			CGPoint location = cg.CGEventGetLocation(event);
		                	long timestamp = cg.CGEventGetTimestamp(event);
		                	int x = location.x.intValue();
		                    int y = location.y.intValue();
		                	proceed = fireMouseUp(x, y, timestamp, MouseButton.LEFT);
	            			break;
	            		}
	            		case CoreGraphics.CGEventType.kCGEventRightMouseDown:
	            		{
	            			CGPoint location = cg.CGEventGetLocation(event);
		                	long timestamp = cg.CGEventGetTimestamp(event);
		                	int x = location.x.intValue();
		                    int y = location.y.intValue();
		                	proceed = fireMouseDown(x, y, timestamp, MouseButton.RIGHT);
	            			break;
	            		}
	            		case CoreGraphics.CGEventType.kCGEventRightMouseUp:
	            		{
	            			CGPoint location = cg.CGEventGetLocation(event);
		                	long timestamp = cg.CGEventGetTimestamp(event);
		                	int x = location.x.intValue();
		                    int y = location.y.intValue();
		                	proceed = fireMouseUp(x, y, timestamp, MouseButton.RIGHT);
	            			break;
	            		}
	            		case CoreGraphics.CGEventType.kCGEventOtherMouseDown:
	            		{
	            			CGPoint location = cg.CGEventGetLocation(event);
		                	long timestamp = cg.CGEventGetTimestamp(event);
		                	int x = location.x.intValue();
		                    int y = location.y.intValue();
		                    int buttonNumber = (int)cg.CGEventGetIntegerValueField(event, CoreGraphics.CGEventField.kCGMouseEventButtonNumber);
		                    MouseButton mouseButton = mouse.forMouseCode(buttonNumber);
		                	proceed = fireMouseDown(x, y, timestamp, mouseButton);
	            			break;
	            		}
	            		case CoreGraphics.CGEventType.kCGEventOtherMouseUp:
	            		{
	            			CGPoint location = cg.CGEventGetLocation(event);
		                	long timestamp = cg.CGEventGetTimestamp(event);
		                	int x = location.x.intValue();
		                    int y = location.y.intValue();
		                    int buttonNumber = (int)cg.CGEventGetIntegerValueField(event, CoreGraphics.CGEventField.kCGMouseEventButtonNumber);
		                    MouseButton mouseButton = mouse.forMouseCode(buttonNumber);
		                	proceed = fireMouseUp(x, y, timestamp, mouseButton);
	            			break;
	            		}
	            		case CoreGraphics.CGEventType.kCGEventKeyDown:
	            		{
	            			long repeat = cg.CGEventGetIntegerValueField(event, CoreGraphics.CGEventField.kCGKeyboardEventAutorepeat);

	            			if (repeat == 0L)
	            			{
		            			long keyCode = cg.CGEventGetIntegerValueField(event, CoreGraphics.CGEventField.kCGKeyboardEventKeycode);
		            			long timestamp = cg.CGEventGetTimestamp(event);
		            			
		            			proceed = fireKeyPressed((int)keyCode, timestamp);
	            			}
	            			else
	            				proceed = true;
	            			
	            			break;
	            		}
	            		case CoreGraphics.CGEventType.kCGEventKeyUp:
	            		{
	            			long keyCode = cg.CGEventGetIntegerValueField(event, CoreGraphics.CGEventField.kCGKeyboardEventKeycode);
	            			long timestamp = cg.CGEventGetTimestamp(event);
	            			
	            			proceed = fireKeyReleased((int)keyCode, timestamp);
	            			
	            			break;
	            		}
	            		case CoreGraphics.CGEventType.kCGEventFlagsChanged:
	            		{
	            			long keyCode = cg.CGEventGetIntegerValueField(event, CoreGraphics.CGEventField.kCGKeyboardEventKeycode);
	            			long timestamp = cg.CGEventGetTimestamp(event);
	            			long flags = cg.CGEventGetFlags(event);

	            			//Can't use keyboard state snapshot - it's worse - it doesn't know difference between left and right keys (e.g. left shift vs right shift)
	            			//Wish there was a better way to detect whether key is down or up...
	            			//TODO could use HID API to make this better
	            			boolean down;
	            			switch ((int)keyCode)
	            			{
	            				case 0x38: //left shift
	            				case 0x3c: //right shift
	            					down = ((flags & CoreGraphics.CGEventFlags.kCGEventFlagMaskShift) != 0L);
	            					break;
	            				case 0x3b: //left control
	            				case 0x3e: //right control
	            					down = ((flags & CoreGraphics.CGEventFlags.kCGEventFlagMaskControl) != 0L);
	            					break;
	            				case 0x37: //command
	            					down = ((flags & CoreGraphics.CGEventFlags.kCGEventFlagMaskCommand) != 0L);
	            					break;
	            				case 0x3a: //left option
	            				case 0x3d: //right option
	            					down = ((flags & CoreGraphics.CGEventFlags.kCGEventFlagMaskAlternate) != 0L);
	            					break;
	            				case 0x39: //capslock
	            					down = ((flags & CoreGraphics.CGEventFlags.kCGEventFlagMaskAlphaShift) != 0L); //TODO is this state or key?  Maybe rethink?
	            					break;
	            				default:
	            					down = true; //Bad situation, can't tell if it is an up event so assume down
	            			}
	            			
	            			if (down)
	            				proceed = fireKeyPressed((int)keyCode, timestamp);
	            			else
	            				proceed = fireKeyReleased((int)keyCode, timestamp);
	            			
	            			break;
	            		}
	            		default:
	            		{
	            			log.warning("Got an unknown event: " + type);
	            			proceed = true;
	            		}
	            	}

	            	if (!proceed)
	            		return(null);
	            	else
	            		return(event);
	            }
	        }, null);

        if (eventTap == null)
            throw new RuntimeException("Failed to create event tap");

        Pointer src = cf.CFMachPortCreateRunLoopSource(CoreFoundation.Constants.kCFAllocatorDefault, eventTap, 0);
        cf.CFRunLoopAddSource(cf.CFRunLoopGetCurrent(), src, CoreFoundation.Constants.kCFRunLoopCommonModes);

        cg.CGEventTapEnable(eventTap, true);
        log.info("Tap enabled: " + cg.CGEventTapIsEnabled(eventTap));

        hookThreadLatch.countDown();
        
        cf.CFRunLoopRun();

        log.info("At the end of the loop");
    }
}
