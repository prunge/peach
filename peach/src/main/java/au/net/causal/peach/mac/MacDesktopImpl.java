package au.net.causal.peach.mac;

import java.util.ArrayList;
import java.util.List;

import au.net.causal.peach.DesktopEvents;
import au.net.causal.peach.Keyboard;
import au.net.causal.peach.Mouse;
import au.net.causal.peach.NativeWindow;

import com.sun.jna.Pointer;

public class MacDesktopImpl implements MacDesktop
{	
	private final Keyboard keyboard = new MacKeyboard();
	private final MacMouse mouse = new MacMouse();
	
	private final MacDesktopEvents events = new MacDesktopEventsImpl(keyboard, mouse);
	
	@Override
	public List<? extends NativeWindow> getWindows()
	{
		//TODO sensible configuration
		CFArrayRef windowList = CoreGraphics.INSTANCE.CGWindowListCreate(0, 0);
		int size = windowList.getCount();
		
		List<MacNativeWindow> resultList = new ArrayList<>(size);
		
		for (int i = 0; i < size; i++)
		{
			Pointer p = windowList.get(i);
			int windowId = (int)Pointer.nativeValue(p);
			MacNativeWindowImpl window = new MacNativeWindowImpl(windowId);
			resultList.add(window);
		}
		
		return(resultList);
	}
	
	@Override
	public DesktopEvents getOn()
	{
		return(events);
	}
	
	@Override
	public Keyboard getKeyboard()
	{
		return(keyboard);
	}
	
	@Override
	public Mouse getMouse()
	{
		return(mouse);
	}
}
