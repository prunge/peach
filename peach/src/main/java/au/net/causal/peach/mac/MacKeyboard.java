package au.net.causal.peach.mac;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import au.net.causal.peach.Key;
import au.net.causal.peach.KeyPosition;
import au.net.causal.peach.Keyboard;
import au.net.causal.peach.KeyboardControl;
import au.net.causal.peach.KeyboardLocks;
import au.net.causal.peach.KeyboardState;
import au.net.causal.peach.KeyboardStateSnapshot;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Table;

import static au.net.causal.peach.KeyPosition.*;

public class MacKeyboard implements Keyboard
{
	private static final CoreGraphics cg = CoreGraphics.INSTANCE;
	
	private final Table<String, String, Key> namePositionTable = HashBasedTable.create();
	private final SetMultimap<String, Key> nameMap = HashMultimap.create();
	private final Map<Integer, Key> codeMap = new HashMap<>();
	
	private final Map<String, KeyPosition> positionMap = new HashMap<>();
	
	private final MacModifierKeyState modifierKeyState = new MacModifierKeyState();
	private final KeyboardControl control = new MacKeyboardControl(this, modifierKeyState);
	private final KeyboardLocks locks;
	
	public MacKeyboard()
	{
		//Register these first so their names aren't overridden by keycodes
		register(new Key(MAIN, "1", 0x12, '1'));
		register(new Key(MAIN, "2", 0x13, '2'));
		register(new Key(MAIN, "3", 0x14, '3'));
		register(new Key(MAIN, "4", 0x15, '4'));
		register(new Key(MAIN, "6", 0x16, '6'));
		register(new Key(MAIN, "5", 0x17, '5'));
		register(new Key(MAIN, "=", 0x18, '='));
		register(new Key(MAIN, "9", 0x19, '9'));
		register(new Key(MAIN, "7", 0x1a, '7'));
		register(new Key(MAIN, "-", 0x1b, '-'));
		register(new Key(MAIN, "8", 0x1c, '8'));
		register(new Key(MAIN, "0", 0x1d, '0'));
		
		//a-z keys
		register(new Key(MAIN, "a", 0x00, 'a'));
		register(new Key(MAIN, "s", 0x01, 's'));
		register(new Key(MAIN, "d", 0x02, 'd'));
		register(new Key(MAIN, "f", 0x03, 'f'));
		register(new Key(MAIN, "h", 0x04, 'h'));
		register(new Key(MAIN, "g", 0x05, 'g'));
		register(new Key(MAIN, "z", 0x06, 'z'));
		register(new Key(MAIN, "x", 0x07, 'x'));
		register(new Key(MAIN, "c", 0x08, 'c'));
		register(new Key(MAIN, "v", 0x09, 'v'));
		register(new Key(MAIN, "b", 0x0b, 'b'));
		register(new Key(MAIN, "q", 0x0c, 'q'));
		register(new Key(MAIN, "w", 0x0d, 'w'));
		register(new Key(MAIN, "e", 0x0e, 'e'));
		register(new Key(MAIN, "r", 0x0f, 'r'));
		register(new Key(MAIN, "y", 0x10, 'y'));
		register(new Key(MAIN, "t", 0x11, 't'));
		
		register(new Key(MAIN, "]", 0x1e, ']'));
		register(new Key(MAIN, "o", 0x1f, 'o'));
		register(new Key(MAIN, "u", 0x20, 'u'));
		register(new Key(MAIN, "[", 0x21, '['));
		register(new Key(MAIN, "i", 0x22, 'i'));
		register(new Key(MAIN, "p", 0x23, 'p'));
		register(new Key(MAIN, "l", 0x25, 'l'));
		register(new Key(MAIN, "j", 0x26, 'j'));
		register(new Key(MAIN, "\'", 0x27, '\''));
		register(new Key(MAIN, "k", 0x28, 'k'));
		register(new Key(MAIN, ";", 0x29, ';'));
		register(new Key(MAIN, "\\", 0x2a, '\\'));
		register(new Key(MAIN, ",", 0x2b, ','));
		register(new Key(MAIN, "/", 0x2c, '/'));
		register(new Key(MAIN, "n", 0x2d, 'n'));
		register(new Key(MAIN, "m", 0x2e, 'm'));
		register(new Key(MAIN, ".", 0x2f, '.'));
		register(new Key(MAIN, "`", 0x32, '`'));
		register(new Key(KEYPAD, ".", 0x41, '.'));
		register(new Key(KEYPAD, "*", 0x43, '*'));
		register(new Key(KEYPAD, "+", 0x45, '+'));
		register(new Key(KEYPAD, "clear", 0x47));
		register(new Key(KEYPAD, "/", 0x4b, '/'));
		register(new Key(KEYPAD, "enter", 0x4c, '\n'), "return");
		register(new Key(KEYPAD, "-", 0x4e, '-'));
		register(new Key(KEYPAD, "=", 0x51, '='));
		register(new Key(KEYPAD, "0", 0x52, '0'));
		register(new Key(KEYPAD, "1", 0x53, '1'));
		register(new Key(KEYPAD, "2", 0x54, '2'));
		register(new Key(KEYPAD, "3", 0x55, '3'));
		register(new Key(KEYPAD, "4", 0x56, '4'));
		register(new Key(KEYPAD, "5", 0x57, '5'));
		register(new Key(KEYPAD, "6", 0x58, '6'));
		register(new Key(KEYPAD, "7", 0x59, '7'));
		register(new Key(KEYPAD, "8", 0x5b, '8'));
		register(new Key(KEYPAD, "9", 0x5c, '9'));
		
		register(new Key(MAIN, "enter", 0x24, '\n'), "return");
		register(new Key(MAIN, "tab", 0x30, '\t'));
		register(new Key(MAIN, "space", 0x31, ' '), "spacebar");
		register(new Key(MAIN, "backspace", 0x33));
		register(new Key(MAIN, "escape", 0x35), "esc");
		register(new Key(LEFT, "command", 0x37));
		register(new Key(LEFT, "shift", 0x38));
		register(new Key(MAIN, "capslock", 0x39));
		register(new Key(LEFT, "option", 0x3a), "alt");
		register(new Key(LEFT, "control", 0x3b), "ctrl");
		register(new Key(RIGHT, "shift", 0x3c));
		register(new Key(RIGHT, "option", 0x3d), "alt");
		register(new Key(RIGHT, "control", 0x3e), "ctrl");
		register(new Key(MAIN, "function", 0x3f), "fn");
		register(new Key(MAIN, "f17", 0x40));
		register(new Key(MAIN, "volumeup", 0x48));
		register(new Key(MAIN, "volumedown", 0x49));
		register(new Key(MAIN, "mute", 0x4a));
		register(new Key(MAIN, "f18", 0x4f));
		register(new Key(MAIN, "f19", 0x50));
		register(new Key(MAIN, "f20", 0x5a));
		register(new Key(MAIN, "f5", 0x60));
		register(new Key(MAIN, "f6", 0x61));
		register(new Key(MAIN, "f7", 0x62));
		register(new Key(MAIN, "f3", 0x63));
		register(new Key(MAIN, "f8", 0x64));
		register(new Key(MAIN, "f9", 0x65));
		register(new Key(MAIN, "f11", 0x67));
		register(new Key(MAIN, "f13", 0x69));
		register(new Key(MAIN, "f16", 0x6a));
		register(new Key(MAIN, "f14", 0x6b));
		register(new Key(MAIN, "f10", 0x6d));
		register(new Key(MAIN, "application", 0x6e), "apps", "app");
		register(new Key(MAIN, "f12", 0x6f));
		register(new Key(MAIN, "f15", 0x71));
		register(new Key(MAIN, "help", 0x72), "insert");
		register(new Key(MAIN, "home", 0x73));
		register(new Key(MAIN, "pageup", 0x74), "pgup");
		register(new Key(MAIN, "delete", 0x75), "forwarddelete");
		register(new Key(MAIN, "f4", 0x76));
		register(new Key(MAIN, "end", 0x77));
		register(new Key(MAIN, "f2", 0x78));
		register(new Key(MAIN, "pagedown", 0x79), "pgdn");
		register(new Key(MAIN, "f1", 0x7a));
		register(new Key(MAIN, "left", 0x7b));
		register(new Key(MAIN, "right", 0x7c));
		register(new Key(MAIN, "down", 0x7d));
		register(new Key(MAIN, "up", 0x7e));
		
		locks = new MacKeyboardLocks();
		
	}
	
	private void register(Key key, String... otherNames)
	{
		KeyPosition position = key.getPosition();
		String positionName = position.getName().toLowerCase(Locale.ENGLISH);
		positionMap.put(positionName, position);
		namePositionTable.put(key.getName().toLowerCase(Locale.ENGLISH), positionName, key);
		nameMap.put(key.getName().toLowerCase(Locale.ENGLISH), key);
		
		//Allow user to specify code as key, but only if key with this name doesn't exist already (e.g. '5' key should not be overridden with key with code 5) 
		if (!namePositionTable.containsRow(String.valueOf(key.getCode())))
			namePositionTable.put(String.valueOf(key.getCode()), positionName, key);
		if (!nameMap.containsKey(String.valueOf(key.getCode())))
			nameMap.put(String.valueOf(key.getCode()), key);
		
		codeMap.put(key.getCode(), key);
		
		for (String otherName : otherNames)
		{
			otherName = otherName.toLowerCase(Locale.ENGLISH);
			namePositionTable.put(otherName, position.getName(), key);
			nameMap.put(otherName, key);
		}
	}
	
	@Override
	public Set<? extends Key> getAll(KeyPosition position, String name)
	{		
		name = name.toLowerCase(Locale.ENGLISH);
		
		if (position == null)
		{
			List<String> tokens = Splitter.on(CharMatcher.WHITESPACE).omitEmptyStrings().splitToList(name);
			if (tokens.size() == 2)
			{
				position = getPosition(tokens.get(0));
				name = tokens.get(1).toLowerCase(Locale.ENGLISH);
			}
		}
		
		if (position == null)
			return(nameMap.get(name));
		else
		{
			Key key = namePositionTable.get(name, position.getName());
			if (key == null)
				return(Collections.emptySet());
			else
				return(Collections.singleton(key));
		}
	}
	
	public KeyPosition getPosition(String name)
	{
		return(positionMap.get(name.toLowerCase(Locale.ENGLISH)));
	}
	
	@Override
	public Key forCode(int code)
	{
		Key key = codeMap.get(code);
		if (key == null)
			key = new Key(KeyPosition.MAIN, "#" + code, code);
		
		return(key);
	}
		
	@Override
	public KeyboardState getCurrentState()
	{
		ImmutableSet.Builder<Key> keySetBuilder = ImmutableSet.builder();		

		boolean shiftConfigured = false;
		boolean controlConfigured = false;
		boolean altConfigured = false;
		boolean commandConfigured = false;
		for (short keyCode = 0x00; keyCode <= 0x7F; keyCode++)
		{
			boolean down = CoreGraphics.byteAsBoolean(cg.CGEventSourceKeyState(CoreGraphics.CGEventSourceStateID.kCGEventSourceStateCombinedSessionState, keyCode));
			if (down)
			{
				Key key = forCode(keyCode);
				keySetBuilder.add(key);
				
				if ("shift".equals(key.getName()))
					shiftConfigured = true;
				if ("control".equals(key.getName()))
					controlConfigured = true;
				if ("alt".equals(key.getName()))
					altConfigured = true;
				if ("command".equals(key.getName()))
					commandConfigured = true;
			}
		}
		
		//Also update from modifier key state
		long flags = cg.CGEventSourceFlagsState(CoreGraphics.CGEventSourceStateID.kCGEventSourceStateCombinedSessionState);
		boolean shift = (flags & CoreGraphics.CGEventFlags.kCGEventFlagMaskShift) != 0;
		boolean control = (flags & CoreGraphics.CGEventFlags.kCGEventFlagMaskControl) != 0;
		boolean command = (flags & CoreGraphics.CGEventFlags.kCGEventFlagMaskCommand) != 0;
		boolean alt = (flags & CoreGraphics.CGEventFlags.kCGEventFlagMaskAlternate) != 0;
		
		//In case we already have modifiers configured with key state above, don't also use the modifiers
		if (shift && !shiftConfigured)
			keySetBuilder.add(getAny("shift"));
		if (control && !controlConfigured)
			keySetBuilder.add(getAny("control"));
		if (alt && !altConfigured)
			keySetBuilder.add(getAny("alt"));
		if (command && !commandConfigured)
			keySetBuilder.add(getAny("command"));
		
		return(new KeyboardStateSnapshot(this, keySetBuilder.build()));
	}
	
	@Override
	public KeyboardControl getControl()
	{
		return(control);
	}
	
	@Override
	public KeyboardLocks getLocks()
	{
		return(locks);
	}
}
