package au.net.causal.peach.mac;

import java.util.Objects;

import au.net.causal.peach.AbstractKeyboardControl;
import au.net.causal.peach.Key;
import au.net.causal.peach.Keyboard;
import au.net.causal.peach.mac.accessibility.AccessibleObject;

public class MacKeyboardControl extends AbstractKeyboardControl
{
	private static final CoreGraphics cg = CoreGraphics.INSTANCE;
	private static final CoreFoundation cf = CoreFoundation.INSTANCE;
	
	//Keep track of modifier states in case of combos since we can't just type these keys
	//TODO this is crap, use HID API
	private final MacModifierKeyState modifierKeyState;
	
	public MacKeyboardControl(Keyboard keyboard, MacModifierKeyState modifierKeyState)
	{
		super(keyboard);
		
		Objects.requireNonNull(modifierKeyState, "modifierKeyState == null");
		
		this.modifierKeyState = modifierKeyState;
	}
	
	@Override
	public void press(Key key)
	{
		Objects.requireNonNull(key, "key == null");
		sendEvent(key, true);
	}

	@Override
	public void release(Key key)
	{
		Objects.requireNonNull(key, "key == null");
		sendEvent(key, false);
	}
	
	private void sendEvent(Key key, boolean down)
	{
		System.err.println("Send event " + key + " " + down);
		
		//Doesn't work reliably on 10.8 - always generates up event???
		/*
		CGEventSourceRef source = cg.CGEventSourceCreate(CoreGraphics.CGEventSourceStateID.kCGEventSourceStateHIDSystemState);
		CGEventRef event = cg.CGEventCreateKeyboardEvent(source, (short)key.getCode(), down);
		return(event);
		*/
		
		//TODO need to test whether modifier keys (shift, etc.) work with this
		//TODO ability to set caps lock state
			
		CGEventSourceRef source = cg.CGEventSourceCreate(CoreGraphics.CGEventSourceStateID.kCGEventSourceStateHIDSystemState);
		try
		{
			//If the key is a modifier key, also send a flags changed event
			int keyCode = key.getCode();
			boolean modifier;
			boolean capsToggle = false;
	
			switch (keyCode)
			{
				case 0x38: //left shift
				case 0x3c: //right shift
					modifier = true;
					modifierKeyState.setShiftDown(down);
					break;
				case 0x3b: //left control
				case 0x3e: //right control
					modifier = true;
					modifierKeyState.setControlDown(down);
					break;
				case 0x37: //command
					modifier = true;
					modifierKeyState.setCommandDown(down);
					break;
				case 0x3a: //left option
				case 0x3d: //right option
					modifier = true;
					modifierKeyState.setAltDown(down);
					break;
				case 0x39: //capslock
					modifier = false;
					capsToggle = true;
					break;
				default:
					modifier = false;
			}
	
			long flags = cg.CGEventSourceFlagsState(CoreGraphics.CGEventSourceStateID.kCGEventSourceStateCombinedSessionState);
			
			if (modifierKeyState.isShiftDown())
				flags |= CoreGraphics.CGEventFlags.kCGEventFlagMaskShift;
			else
				flags &= ~CoreGraphics.CGEventFlags.kCGEventFlagMaskShift;
			if (modifierKeyState.isControlDown())
				flags |= CoreGraphics.CGEventFlags.kCGEventFlagMaskControl;
			else
				flags &= ~CoreGraphics.CGEventFlags.kCGEventFlagMaskControl;
			if (modifierKeyState.isCommandDown())
				flags |= CoreGraphics.CGEventFlags.kCGEventFlagMaskCommand;
			else
				flags &= ~CoreGraphics.CGEventFlags.kCGEventFlagMaskCommand;
			if (modifierKeyState.isAltDown())
				flags |= CoreGraphics.CGEventFlags.kCGEventFlagMaskAlternate;
			else
				flags &= ~CoreGraphics.CGEventFlags.kCGEventFlagMaskAlternate;
			if (capsToggle && down)
				flags ^= CoreGraphics.CGEventFlags.kCGEventFlagMaskAlphaShift;
			
			/* Hmm apparently you can't post an event flags changed event
			if (modifier || (capsToggle && down))
			{
				CGEventRef event = cg.CGEventCreate(source);
				cg.CGEventSetType(event, CoreGraphics.CGEventType.kCGEventFlagsChanged);
				cg.CGEventSetIntegerValueField(event, CoreGraphics.CGEventField.kCGKeyboardEventKeycode, key.getCode());
				cg.CGEventSetFlags(event, flags);
				
				try
				{
					cg.CGEventPost(CoreGraphics.CGEventTapLocation.kCGHIDEventTap, event);
				}
				finally
				{
					cf.CFRelease(event);
				}
			}
			*/
			
			if (modifier)
			{
				//Use accessibility API to send keys as well
				AccessibleObject.global().postKeyboardEvent((short)0, (short)keyCode, down);
			}
			
			//else
			{
				//Now send the keystroke itself
				CGEventRef event = cg.CGEventCreate(source);
				
				int type;
				if (down)
					type = CoreGraphics.CGEventType.kCGEventKeyDown;
				else
					type = CoreGraphics.CGEventType.kCGEventKeyUp;
				
				if (event != null)
				{
					cg.CGEventSetType(event, type);
					cg.CGEventSetFlags(event, flags);
					cg.CGEventSetIntegerValueField(event, CoreGraphics.CGEventField.kCGKeyboardEventKeycode, key.getCode());
					
					try
					{
						System.err.println("Post event " + key.getCode());
						cg.CGEventPost(CoreGraphics.CGEventTapLocation.kCGHIDEventTap, event);
					}
					finally
					{
						cf.CFRelease(event);
					}
				}
			}
		}
		finally
		{
			cf.CFRelease(source);
		}
	}
}
