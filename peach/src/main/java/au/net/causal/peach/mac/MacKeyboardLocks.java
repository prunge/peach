package au.net.causal.peach.mac;

import java.awt.Toolkit;

import au.net.causal.peach.JavaKeyboardLocks;

public class MacKeyboardLocks extends JavaKeyboardLocks
{
	public MacKeyboardLocks()
	{
	}

	public MacKeyboardLocks(Toolkit toolkit)
	{
		super(toolkit);
	}

	@Override
	public void setCapsLock(boolean capsLock)
	{
		//No support
	}

	@Override
	public void setNumLock(boolean numLock)
	{
		//No support
	}

	@Override
	public void setScrollLock(boolean scrollLock)
	{
		//No support
	}

	@Override
	public void setKanaLock(boolean kanaLock)
	{
		//No support
	}

	@Override
	public void toggleCapsLock()
	{
		//No support
	}

	@Override
	public void toggleKanaLock()
	{
		//No support
	}

	@Override
	public void toggleNumLock()
	{
		//No support
	}

	@Override
	public void toggleScrollLock()
	{
		//No support
	}	
}
