package au.net.causal.peach.mac;

public class MacModifierKeyState
{
	private boolean shiftDown;
	private boolean controlDown;
	private boolean altDown;
	private boolean commandDown;
	
	public boolean isShiftDown()
	{
		return(shiftDown);
	}
	
	public void setShiftDown(boolean shiftDown)
	{
		this.shiftDown = shiftDown;
	}
	
	public boolean isControlDown()
	{
		return(controlDown);
	}
	
	public void setControlDown(boolean controlDown)
	{
		this.controlDown = controlDown;
	}
	
	public boolean isAltDown()
	{
		return(altDown);
	}
	
	public void setAltDown(boolean altDown)
	{
		this.altDown = altDown;
	}
	
	public boolean isCommandDown()
	{
		return(commandDown);
	}
	
	public void setCommandDown(boolean commandDown)
	{
		this.commandDown = commandDown;
	}

	@Override
	public String toString()
	{
		return "MacModifierKeyState [shiftDown=" + shiftDown + ", controlDown=" + controlDown + ", altDown=" + altDown + ", commandDown=" + commandDown + "]";
	}
}
