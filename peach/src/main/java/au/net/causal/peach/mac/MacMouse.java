package au.net.causal.peach.mac;

import java.awt.Point;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import au.net.causal.peach.Mouse;
import au.net.causal.peach.MouseControl;
import au.net.causal.peach.MouseState;
import au.net.causal.peach.MouseStateSnapshot;
import au.net.causal.peach.event.MouseButton;
import au.net.causal.peach.mac.CoreGraphics.CGMouseButton;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

public class MacMouse implements Mouse
{
	private static final CoreGraphics cg = CoreGraphics.INSTANCE;
	
	private final MouseControl control;
	
	private final Map<String, ? extends MouseButton> namedButtons;
	private final Set<? extends MouseButton> buttons;
	
	public MacMouse()
	{
		this.control = new MacMouseControl(this);
		
		ImmutableMap.Builder<String, MouseButton> namedButtonBuilder = ImmutableMap.builder();
		
		registerButton(namedButtonBuilder, MouseButton.LEFT);
		registerButton(namedButtonBuilder, MouseButton.MIDDLE);
		registerButton(namedButtonBuilder, MouseButton.RIGHT);
		registerButton(namedButtonBuilder, new MouseButton(4, "x1"));
		registerButton(namedButtonBuilder, new MouseButton(5, "x2"));
		
		namedButtons = namedButtonBuilder.build();
		buttons = ImmutableSet.copyOf(namedButtons.values());
	}
	
	private void registerButton(ImmutableMap.Builder<String, MouseButton> b, MouseButton button)
	{
		b.put(button.getName().toLowerCase(Locale.ENGLISH), button);
		b.put(String.valueOf(button.getId()), button);
	}
	
	@Override
	public MouseState getCurrentState()
	{
		CGEventRef tempEvent = cg.CGEventCreate(null);
		CGPoint point = cg.CGEventGetLocation(tempEvent.getPointer());
		Point position = new Point(point.x.intValue(), point.y.intValue());
		
		ImmutableSet.Builder<MouseButton> sb = ImmutableSet.builder();
		
		for (int i = 0; i < 32; i++)
		{
			if (CoreGraphics.byteAsBoolean(cg.CGEventSourceButtonState(CoreGraphics.CGEventSourceStateID.kCGEventSourceStateCombinedSessionState, i)))
				sb.add(forMouseCode(i));
		}
		
		return(new MouseStateSnapshot(this, sb.build(), position));
	}
	
	@Override
	public MouseControl getControl()
	{
		return(control);
	}
	
	@Override
	public Set<? extends MouseButton> getButtons()
	{
		return(buttons);
	}
	
	@Override
	public MouseButton button(String name)
	{
		Objects.requireNonNull(name, "name == null");
		return(namedButtons.get(name.toLowerCase(Locale.ENGLISH)));
	}
	
	public MouseButton forMouseCode(int code)
	{
		switch (code)
		{
			case CGMouseButton.kCGMouseButtonLeft:
				return(MouseButton.LEFT);
			case CGMouseButton.kCGMouseButtonCenter:
				return(MouseButton.MIDDLE);
			case CGMouseButton.kCGMouseButtonRight:
				return(MouseButton.RIGHT);
			case 3:
				return(button("x1"));
			case 4:
				return(button("x2"));
			default:
				return(new MouseButton(code + 1, "button " + (code + 1)));
		}
	}
	
	public int toCode(MouseButton button)
	{
		if (button.getId() == MouseButton.LEFT.getId())
			return(CGMouseButton.kCGMouseButtonLeft);
		if (button.getId() == MouseButton.MIDDLE.getId())
			return(CGMouseButton.kCGMouseButtonCenter);
		if (button.getId() == MouseButton.RIGHT.getId())
			return(CGMouseButton.kCGMouseButtonRight);
		if (button.getId() == button("x1").getId())
			return(3);
		if (button.getId() == button("x2").getId())
			return(4);
		
		return(button.getId() - 1);
	}
	
}
