package au.net.causal.peach.mac;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

import au.net.causal.peach.AbstractMouseControl;
import au.net.causal.peach.MouseState;
import au.net.causal.peach.event.MouseButton;

public class MacMouseControl extends AbstractMouseControl
{
	private static final CoreGraphics cg = CoreGraphics.INSTANCE;
	
	private final MacMouse mouse;
	private final Robot robot;
	
	private static final AtomicLong eventNumber = new AtomicLong((int)(Math.random() * 32000)); 
	
	public MacMouseControl(MacMouse mouse)
	{
		super(mouse);
		this.mouse = mouse;
		
		try
		{
			this.robot = new Robot();
		}
		catch (AWTException e)
		{
			//TODO
			throw new RuntimeException(e);
		}
	}

	@Override
	public void move(int x, int y)
	{
		CGPoint pos = new CGPoint();
        pos.x = new CGFloat(x);
        pos.y = new CGFloat(y);
        pos.write();
        
        System.err.println("Mouse move to " + pos);
        
        /*
        CGEventRef event = cg.CGEventCreate(null);
        cg.CGEventSetType(event, CoreGraphics.CGEventType.kCGEventMouseMoved);
        cg.CGEventSetLocation(event, pos);
        cg.CGEventPost(CoreGraphics.CGEventTapLocation.kCGHIDEventTap, event);
        */
        
        robot.mouseMove(x, y);
        robot.waitForIdle();
        
        //Validate position now
        System.err.println("Position: " + mouse.getCurrentState().getX() + ", " + mouse.getCurrentState().getY());
	}

	@Override
	public void buttonDown(MouseButton button)
	{
		Objects.requireNonNull(button, "button == null");
		
		int buttonId = button.getId();
		
		MouseState state = mouse.getCurrentState();
		CGPoint pos = new CGPoint();
		pos.x = new CGFloat(state.getX());
		pos.y = new CGFloat(state.getY());
		pos.write();
		
		CGEventRef event = cg.CGEventCreate(null);
		
		if (buttonId == MouseButton.LEFT.getId())
			cg.CGEventSetType(event, CoreGraphics.CGEventType.kCGEventLeftMouseDown);
		else if (buttonId == MouseButton.RIGHT.getId())
			cg.CGEventSetType(event, CoreGraphics.CGEventType.kCGEventRightMouseDown);
		else
			cg.CGEventSetType(event, CoreGraphics.CGEventType.kCGEventOtherMouseDown);
		
		cg.CGEventSetIntegerValueField(event, CoreGraphics.CGEventField.kCGMouseEventButtonNumber, mouse.toCode(button));
		
		//Weird stuff happens when event number not changed for each button down event
		cg.CGEventSetIntegerValueField(event, CoreGraphics.CGEventField.kCGMouseEventNumber, eventNumber.incrementAndGet());
		
        cg.CGEventPost(CoreGraphics.CGEventTapLocation.kCGHIDEventTap, event);
	}

	@Override
	public void buttonUp(MouseButton button)
	{
		Objects.requireNonNull(button, "button == null");
		
		int buttonId = button.getId();
		
		MouseState state = mouse.getCurrentState();
		CGPoint pos = new CGPoint();
		pos.x = new CGFloat(state.getX());
		pos.y = new CGFloat(state.getY());
		pos.write();
		
		CGEventRef event = cg.CGEventCreate(null);
		
		if (buttonId == MouseButton.LEFT.getId())
			cg.CGEventSetType(event, CoreGraphics.CGEventType.kCGEventLeftMouseUp);
		else if (buttonId == MouseButton.RIGHT.getId())
			cg.CGEventSetType(event, CoreGraphics.CGEventType.kCGEventRightMouseUp);
		else
			cg.CGEventSetType(event, CoreGraphics.CGEventType.kCGEventOtherMouseUp);
		
		cg.CGEventSetIntegerValueField(event, CoreGraphics.CGEventField.kCGMouseEventButtonNumber, mouse.toCode(button));
		
        cg.CGEventPost(CoreGraphics.CGEventTapLocation.kCGHIDEventTap, event);
	}

	@Override
	public void click(MouseButton button)
	{
		Objects.requireNonNull(button, "button == null");
		buttonDown(button);
		buttonUp(button);
	}

	@Override
	public void doubleClick(MouseButton button)
	{
		Objects.requireNonNull(button, "button == null");
		click(button);
		click(button);
	}

	@Override
	public void scrollWheel(int relativeAmount)
	{
		MouseState state = mouse.getCurrentState();
		CGPoint pos = new CGPoint();
		pos.x = new CGFloat(state.getX());
		pos.y = new CGFloat(state.getY());
		pos.write();
		
		CGEventRef event = cg.CGEventCreate(null);
		
		cg.CGEventSetType(event, CoreGraphics.CGEventType.kCGEventScrollWheel);
		cg.CGEventSetIntegerValueField(event, CoreGraphics.CGEventField.kCGScrollWheelEventDeltaAxis1, -relativeAmount);
		
        cg.CGEventPost(CoreGraphics.CGEventTapLocation.kCGHIDEventTap, event);
	}

}
