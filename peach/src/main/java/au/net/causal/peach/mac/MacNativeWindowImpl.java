package au.net.causal.peach.mac;

import java.awt.Rectangle;

import au.net.causal.peach.NativeWindowState;

import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;

public class MacNativeWindowImpl implements MacNativeWindow
{
	private final int id; 
	
	public MacNativeWindowImpl(int id)
	{
		this.id = id;
	}
	
	private Pointer readWindowAttribute(Pointer attributeConstant)
	{
		CFArrayRef array = CoreFoundation.INSTANCE.CFArrayCreate(null, new int[] {id}, new NativeLong(1L), null);
		CFArrayRef descriptionArray = CoreGraphics.INSTANCE.CGWindowListCreateDescriptionFromArray(array);
		Pointer descriptionPointer = descriptionArray.get(0);
		if (descriptionPointer == null)
			return(null);
		
		CFDictionaryRef dict = new CFDictionaryRef(descriptionPointer);
		Pointer windowAttributePointer = dict.get(attributeConstant);
		
		return(windowAttributePointer);
	}
	
	@Override
	public String getTitle()
	{
		Pointer windowNamePointer = readWindowAttribute(CoreGraphics.Constants.kCGWindowName);
		if (windowNamePointer == null)
			return(null);
		
		CFStringRef windowName = new CFStringRef(windowNamePointer);
		return(windowName.getStringValue());
	}
	
	@Override
	public Rectangle getBounds()
	{
		Pointer windowBoundsPointer = readWindowAttribute(CoreGraphics.Constants.kCGWindowBounds);
		if (windowBoundsPointer == null)
			return(null);
			
		CGRect.ByReference windowBounds = new CGRect.ByReference();
		boolean ok = CoreGraphics.INSTANCE.CGRectMakeWithDictionaryRepresentation(windowBoundsPointer, windowBounds);
		if (!ok)
			throw new RuntimeException("Failed to make rect from dictionary representation");
		
		return(new Rectangle(windowBounds.origin.x.intValue(), windowBounds.origin.y.intValue(), 
							windowBounds.size.width.intValue(), windowBounds.size.height.intValue()));
	}
	
	@Override
	public MacNativeProcess getProcess()
	{
		//TODO implement
		throw new UnsupportedOperationException("not yet implemented");
	}
	
	@Override
	public NativeWindowState getState()
	{
		//TODO
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void setState(NativeWindowState state)
	{
		//TODO
		throw new UnsupportedOperationException();
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MacNativeWindowImpl other = (MacNativeWindowImpl) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "MacNativeWindowImpl [id=" + id + "]";
	}
}
