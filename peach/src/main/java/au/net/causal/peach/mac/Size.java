package au.net.causal.peach.mac;

public class Size
{
    public final double width;
    public final double height;

    public Size(double width, double height)
    {
        this.width = width;
        this.height = height;
    }

    public double getWidth()
    {
        return width;
    }

    public double getHeight()
    {
        return height;
    }

    @Override
    public String toString()
    {
        return("[" + width + ", " + height + "]");
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (!(o instanceof Size))
        {
            return false;
        }

        Size size = (Size) o;

        if (Double.compare(size.width, width) != 0)
        {
            return false;
        }
        if (Double.compare(size.height, height) != 0)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result;
        long temp;
        temp = Double.doubleToLongBits(width);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(height);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
