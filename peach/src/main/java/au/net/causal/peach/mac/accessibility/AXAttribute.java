package au.net.causal.peach.mac.accessibility;

import java.util.Objects;

public abstract class AXAttribute<T>
{
    private final String key;

    protected AXAttribute(String key)
    {
        Objects.requireNonNull(key, "key == null");
        this.key = key;
    }

    public String getKey()
    {
        return(key);
    }

    public abstract T read(AccessibleObject from);

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (!(o instanceof AXAttribute))
        {
            return false;
        }

        AXAttribute<?> that = (AXAttribute<?>) o;

        return(key.equals(that.key));
    }

    @Override
    public int hashCode()
    {
        return(key.hashCode());
    }
}
