package au.net.causal.peach.mac.accessibility;

import au.net.causal.peach.mac.CFArrayRef;
import au.net.causal.peach.mac.CFDictionaryRef;
import au.net.causal.peach.mac.CFStringRef;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;
import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.PointerType;
import com.sun.jna.ptr.LongByReference;
import com.sun.jna.ptr.NativeLongByReference;

public interface AXUIElement extends Library
{
    public static final AXUIElement INSTANCE = (AXUIElement)Native.loadLibrary("ApplicationServices", AXUIElement.class);

    public boolean AXAPIEnabled();
    public boolean AXIsProcessTrustedWithOptions(CFDictionaryRef options);
    public boolean AXIsProcessTrusted();

    public Pointer AXUIElementCreateSystemWide();
    public Pointer AXUIElementCreateApplication(NativeLong pid);
    public int AXUIElementCopyActionNames(Pointer element, CFArrayRef[] names);
    public int AXUIElementCopyAttributeNames(Pointer element, CFArrayRef[] names);
    public int AXUIElementCopyAttributeValue(Pointer element, CFStringRef attribute, CFTypeRef[] value);
    public int AXUIElementCopyAttributeValues(Pointer element, CFStringRef attribute, int index, int maxValues, CFArrayRef[] values);
    public int AXUIElementGetAttributeValueCount(Pointer element, CFStringRef attribute, NativeLongByReference count);

    public int AXUIElementSetAttributeValue(Pointer element, CFStringRef attribute, Pointer value);
    public int AXUIElementSetAttributeValue(Pointer element, CFStringRef attribute, CFTypeRef value);

    public boolean AXValueGetValue(Pointer value, long theType, Pointer valuePtr);

    public int AXUIElementGetPid(Pointer element, LongByReference pid);

    public int AXUIElementPerformAction(Pointer element, CFStringRef action);

    public Pointer AXValueCreate(long type, Pointer valuep);

    public int AXUIElementSetMessagingTimeout(Pointer element, float timeoutInSeconds);

    public int AXUIElementPostKeyboardEvent(Pointer application, short keyChar, short virtualKey, boolean keyDown);

    public static class CFTypeRef extends PointerType
    {
        public CFTypeRef()
        {
        }

        public CFTypeRef(Pointer pointer)
        {
            super(pointer);
        }
    }

    public static final class Constants
    {
        private static final NativeLibrary AX_NATIVE_LIBRARY = NativeLibrary.getInstance("ApplicationServices");

        public static final Pointer kAXTrustedCheckOptionPrompt = getKeyCFStringRef("kAXTrustedCheckOptionPrompt");

        public static final int kAXErrorNoValue = -25212;
        public static final int kAXErrorCannotComplete = -25204;
        public static final int kAXErrorAttributeUnsupported = -25205;
        public static final int kAXErrorActionUnsupported = -25206;


        public static final long kAXValueCGPointType = 1;
        public static final long kAXValueCGSizeType = 2;
        public static final long kAXValueCGRectType = 3;
        public static final long kAXValueCFRangeType = 4;
        public static final long kAXValueAXErrorType = 5;
        public static final long kAXValueIllegalType = 0;

        private static Pointer getKeyCFStringRef(String key)
        {
            return(AX_NATIVE_LIBRARY.getGlobalVariableAddress(key).getPointer(0L));
        }
    }
}
