package au.net.causal.peach.mac.accessibility;

import java.awt.geom.Point2D;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import au.net.causal.peach.mac.CFArrayRef;
import au.net.causal.peach.mac.CFStringRef;
import au.net.causal.peach.mac.CGFloat;
import au.net.causal.peach.mac.CGPoint;
import au.net.causal.peach.mac.CGSize;
import au.net.causal.peach.mac.CoreFoundation;
import au.net.causal.peach.mac.Size;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.LongByReference;
import com.sun.jna.ptr.NativeLongByReference;

public class AccessibleObject
{
    private static final AXUIElement ax = AXUIElement.INSTANCE;
    private static final CoreFoundation coreFoundation = CoreFoundation.INSTANCE;

    private final Pointer element;

    protected AccessibleObject(Pointer element)
    {
        Objects.requireNonNull(element, "element == null");
        this.element = element;
    }

    public static AccessibleObject global()
    {
        Pointer axUiElement = ax.AXUIElementCreateSystemWide();
        if (axUiElement == null)
            throw new RuntimeException("Error creating global ax object.");

        return(new AccessibleObject(axUiElement));
    }

    public static AccessibleObject forProcess(long processId)
    {
        Pointer axUiElement = ax.AXUIElementCreateApplication(new NativeLong(processId));
        if (axUiElement == null)
            throw new RuntimeException("Error creating object for " + processId);

        return(new AccessibleObject(axUiElement));
    }

    public void setMessagingTimeout(float timeoutInSeconds)
    {
        int result = ax.AXUIElementSetMessagingTimeout(element, timeoutInSeconds);
        if (result != 0)
            throw new RuntimeException("Error setting messaging timeout: " + result);
    }

    public void performAction(String actionName)
    {
        Objects.requireNonNull(actionName, "actionName == null");
        CFStringRef actionNameRef = createCFString(actionName);
        try
        {
            int result = ax.AXUIElementPerformAction(element, actionNameRef);
            if (result == AXUIElement.Constants.kAXErrorActionUnsupported)
                throw new RuntimeException("Action unsupported.");
            else if (result != 0)
                throw new RuntimeException("Error performing action " + actionName + ": " + result);
        }
        finally
        {
            disposeCFString(actionNameRef);
        }
    }

    public boolean isAttributePresent(AXAttribute<?> attribute)
    {
        Objects.requireNonNull(attribute, "attribute == null");
        return(getAttributeNames().contains(attribute.getKey()));
    }

    public <T> T getAttribute(AXAttribute<T> attribute)
    {
        Objects.requireNonNull(attribute, "attribute == null");
        return(attribute.read(this));
    }

    public <T> void setAttribute(WritableAXAttribute<T> attribute, T value)
    {
        Objects.requireNonNull(attribute, "attribute == null");
        Objects.requireNonNull(value, "value == null");
        attribute.write(this, value);
    }

    public AccessibleObject getAttributeAccessibleObject(String key)
    {
        Objects.requireNonNull(key, "key == null");
        AXUIElement.CFTypeRef raw = getAttributeRaw(key);
        if (raw == null)
            return(null);
        Pointer result = raw.getPointer();
        return(new AccessibleObject(result));
    }

    public List<? extends AccessibleObject> getAttributeAccessibleObjects(String key)
    {
        CFStringRef keyRef = createCFString(key);

        try
        {
            CFArrayRef[] windowValuesHolder = new CFArrayRef[1];
            NativeLongByReference countHolder = new NativeLongByReference();
            int err = ax.AXUIElementGetAttributeValueCount(element, keyRef, countHolder);
            if (err == AXUIElement.Constants.kAXErrorAttributeUnsupported)
                return(null);

            if (countHolder.getValue().longValue() == 0L)
                return(Collections.<AccessibleObject>emptyList());

            err = ax.AXUIElementCopyAttributeValues(element, keyRef, 0, 4096, windowValuesHolder);
            if (err == AXUIElement.Constants.kAXErrorNoValue || err == AXUIElement.Constants.kAXErrorAttributeUnsupported)
                return(null);
            else if (err != 0)
                throw new RuntimeException("Error " + err + " retrieving attribute value for " + key);

            int count = windowValuesHolder[0].getCount();
            ImmutableList.Builder<AccessibleObject> builder = ImmutableList.builder();

            for (int i = 0; i < count; i++)
            {
                Pointer windowRefAx = windowValuesHolder[0].get(i);
                builder.add(new AccessibleObject(windowRefAx));
            }

            return(builder.build());
        }
        finally
        {
            disposeCFString(keyRef);
        }
    }

    public String getAttributeString(String key)
    {
        Objects.requireNonNull(key, "key == null");
        AXUIElement.CFTypeRef raw = getAttributeRaw(key);
        if (raw == null)
            return null;
        CFStringRef stringRef = new CFStringRef(raw.getPointer());
        String stringContent = stringRef.getStringValue();
        return(stringContent);
    }

    public void setAttributeString(String key, String value)
    {
        Objects.requireNonNull(key, "key == null");

        CFStringRef keyRef = createCFString(key);
        try
        {
            CFStringRef valueRef = createCFString(value);
            try
            {
                int err = ax.AXUIElementSetAttributeValue(element, keyRef, valueRef.getPointer());
                if (err != 0)
                    throw new RuntimeException("Error " + err + " writing attribute value for " + key);
            }
            finally
            {
                disposeCFString(valueRef);
            }
        }
        finally
        {
            disposeCFString(keyRef);
        }
    }

    public String getAttributeURL(String key)
    {
        Objects.requireNonNull(key, "key == null");
        AXUIElement.CFTypeRef raw = getAttributeRaw(key);
        if (raw == null)
            return null;
        CFURLRef urlRef = new CFURLRef(raw.getPointer());
        String stringContent = urlRef.getStringValue();
        return(stringContent);
    }

    private AXUIElement.CFTypeRef getAttributeRaw(String key)
    {
        CFStringRef cfKey = createCFString(key);
        try
        {
            AXUIElement.CFTypeRef[] vResult = new AXUIElement.CFTypeRef[1];
            int err = ax.AXUIElementCopyAttributeValue(element, cfKey, vResult);
            if (err == AXUIElement.Constants.kAXErrorNoValue || err == AXUIElement.Constants.kAXErrorAttributeUnsupported)
                return(null);
            else if (err != 0)
                throw new RuntimeException("Error " + err + " retrieving attribute value for " + key);

            return(vResult[0]);
        }
        finally
        {
            disposeCFString(cfKey);
        }
    }

    public Point2D.Double getAttributePosition(String key)
    {
        Objects.requireNonNull(key, "key == null");
        AXUIElement.CFTypeRef raw = getAttributeRaw(key);
        CGPoint pos = new CGPoint();
        ax.AXValueGetValue(raw.getPointer(), AXUIElement.Constants.kAXValueCGPointType, pos.getPointer());
        pos.read();

        Point2D.Double point = new Point2D.Double(pos.x.doubleValue(), pos.y.doubleValue());
        return(point);
    }

    public Size getAttributeSize(String key)
    {
        Objects.requireNonNull(key, "key == null");
        AXUIElement.CFTypeRef raw = getAttributeRaw(key);
        CGSize size = new CGSize();
        ax.AXValueGetValue(raw.getPointer(), AXUIElement.Constants.kAXValueCGSizeType, size.getPointer());
        size.read();

        Size outSize = new Size(size.width.doubleValue(), size.height.doubleValue());

        return(outSize);
    }

    public Boolean getAttributeBoolean(String key)
    {
        Objects.requireNonNull(key, "key == null");
        AXUIElement.CFTypeRef raw = getAttributeRaw(key);
        if (raw == null)
            return(null);

        return(raw.getPointer() == CoreFoundation.Constants.kCFBooleanTrue);
    }

    public void setAttributeBoolean(String key, boolean value)
    {
        Objects.requireNonNull(key, "key == null");

        CFStringRef keyRef = createCFString(key);
        try
        {
            Pointer bValue;
            if (value)
                bValue = CoreFoundation.Constants.kCFBooleanTrue;
            else
                bValue = CoreFoundation.Constants.kCFBooleanFalse;

            int err = ax.AXUIElementSetAttributeValue(element, keyRef, bValue);
            if (err != 0)
                throw new RuntimeException("Error " + err + " writing attribute value for " + key);
        }
        finally
        {
            disposeCFString(keyRef);
        }
    }

    public void setAttributeSize(String key, Size value)
    {
        Objects.requireNonNull(key, "key == null");
        Objects.requireNonNull(value, "value == null");

        CFStringRef keyRef = createCFString(key);
        try
        {

            CGSize cgSize = new CGSize();
            cgSize.width = new CGFloat(value.getWidth());
            cgSize.height = new CGFloat(value.getHeight());
            cgSize.write();

            Pointer typedValue = ax.AXValueCreate(AXUIElement.Constants.kAXValueCGSizeType, cgSize.getPointer());

            int err = ax.AXUIElementSetAttributeValue(element, keyRef, typedValue);
            if (err != 0)
                throw new RuntimeException("Error " + err + " writing attribute value for " + key);
        }
        finally
        {
            disposeCFString(keyRef);
        }
    }

    public void setAttributePosition(String key, Point2D.Double value)
    {
        Objects.requireNonNull(key, "key == null");
        Objects.requireNonNull(value, "value == null");

        CFStringRef keyRef = createCFString(key);
        try
        {
            CGPoint cgPoint = new CGPoint();
            cgPoint.x = new CGFloat(value.getX());
            cgPoint.y = new CGFloat(value.getY());
            cgPoint.write();

            Pointer typedValue = ax.AXValueCreate(AXUIElement.Constants.kAXValueCGPointType, cgPoint.getPointer());

            int err = ax.AXUIElementSetAttributeValue(element, keyRef, typedValue);
            if (err != 0)
                throw new RuntimeException("Error " + err + " writing attribute value for " + key);
        }
        finally
        {
            disposeCFString(keyRef);
        }
    }

    public Set<String> getAttributeNames()
    {
        CFArrayRef[] names = new CFArrayRef[] {new CFArrayRef()};

        int result = ax.AXUIElementCopyAttributeNames(element, names);
        if (result != 0)
            throw new RuntimeException("Error getting attribute names: " + result);

        try
        {
            int n = names[0].getCount();
            Set<String> results = new LinkedHashSet<>(n);
            for (int i = 0; i < n; i++)
            {
                CFStringRef s = new CFStringRef(names[0].get(i));
                results.add(s.getStringValue());
            }

            return (results);
        }
        finally
        {
            coreFoundation.CFRelease(names[0]);
        }
    }

    public Set<String> getActionNames()
    {
        CFArrayRef[] names = new CFArrayRef[] {new CFArrayRef()};

        int result = ax.AXUIElementCopyActionNames(element, names);
        if (result != 0)
            throw new RuntimeException("Error getting action names: " + result);

        try
        {
            int n = names[0].getCount();
            Set<String> results = new LinkedHashSet<>(n);
            for (int i = 0; i < n; i++)
            {
                CFStringRef s = new CFStringRef(names[0].get(i));
                results.add(s.getStringValue());
            }

            return (results);
        }
        finally
        {
            coreFoundation.CFRelease(names[0]);
        }
    }

    public long getPid()
    {
        LongByReference pidHolder = new LongByReference();
        int result = ax.AXUIElementGetPid(element, pidHolder);
        if (result != 0)
            throw new RuntimeException("Error retrieving PID: " + result);

        return(pidHolder.getValue());
    }

    public void postKeyboardEvent(short keyChar, short virtualKey, boolean keyDown)
    {
        int result = ax.AXUIElementPostKeyboardEvent(element, keyChar, virtualKey, keyDown);
        if (result != 0)
            throw new RuntimeException("Error sending keyboard event: " + result);
    }

    private static CFStringRef createCFString(String s)
    {
        return coreFoundation.CFStringCreateWithCharacters(null, s.toCharArray(), s.length());
    }

    private static void disposeCFString(CFStringRef s)
    {
        coreFoundation.CFRelease(s);
    }

    public static final class Attributes
    {
        private static ImmutableMap.Builder<String, AXAttribute<?>> ATTRIBUTE_MAP_BUILDER = ImmutableMap.builder();

        public static final AccessibleObjectAXAttribute AXFocusedApplication = createAccessibleObjectAttribute("AXFocusedApplication");
        public static final StringAXAttribute AXTitle = createStringAttribute("AXTitle");
        public static final StringAXAttribute AXRole = createStringAttribute("AXRole");
        public static final StringAXAttribute AXRoleDescription = createStringAttribute("AXRoleDescription");
        public static final StringAXAttribute AXSubrole = createStringAttribute("AXSubrole");
        public static final StringAXAttribute AXHelp = createStringAttribute("AXHelp");
        public static final StringAXAttribute AXDescription = createStringAttribute("AXDescription");
        public static final BooleanAXAttribute AXHidden = createBooleanAttribute("AXHidden");
        public static final BooleanAXAttribute AXMinimized = createBooleanAttribute("AXMinimized");
        public static final AccessibleObjectAXAttribute AXMainWindow = createAccessibleObjectAttribute("AXMainWindow");
        public static final AccessibleObjectAXAttribute AXTopLevelUIElement = createAccessibleObjectAttribute("AXTopLevelUIElement");
        public static final BooleanAXAttribute AXFrontmost = createBooleanAttribute("AXFrontmost");
        public static final AccessibleObjectAXAttribute AXFocusedWindow = createAccessibleObjectAttribute("AXFocusedWindow");
        public static final AccessibleObjectAXAttribute AXFocusedUIElement = createAccessibleObjectAttribute("AXFocusedUIElement");
        public static final AccessibleObjectAXAttribute AXMenuBar = createAccessibleObjectAttribute("AXMenuBar");
        public static final AccessibleObjectAXAttribute AXExtrasMenuBar = createAccessibleObjectAttribute("AXExtrasMenuBar");
        public static final PositionAXAttribute AXPosition = createPositionAttribute("AXPosition");
        public static final SizeAXAttribute AXSize = createSizeAttribute("AXSize");
        public static final AccessibleObjectListAXAttribute AXWindows = createAccessibleObjectListAttribute("AXWindows");
        public static final BooleanAXAttribute AXFocused = createBooleanAttribute("AXFocused");
        public static final BooleanAXAttribute AXMain = createBooleanAttribute("AXMain");
        public static final AccessibleObjectAXAttribute AXParent = createAccessibleObjectAttribute("AXParent");
        public static final AccessibleObjectListAXAttribute AXChildren = createAccessibleObjectListAttribute("AXChildren");
        public static final BooleanAXAttribute AXModal = createBooleanAttribute("AXModal");
        public static final BooleanAXAttribute AXFullScreen = createBooleanAttribute("AXFullScreen");
        public static final BooleanAXAttribute AXEnabled = createBooleanAttribute("AXEnabled");
        public static final URLAXAttribute AXURL = createURLAttribute("AXURL");

        public static final AccessibleObjectAXAttribute AXCloseButton = createAccessibleObjectAttribute("AXCloseButton");
        public static final AccessibleObjectAXAttribute AXZoomButton = createAccessibleObjectAttribute("AXZoomButton");
        public static final AccessibleObjectAXAttribute AXMinimizeButton = createAccessibleObjectAttribute("AXMinimizeButton");
        public static final AccessibleObjectAXAttribute AXToolbarButton = createAccessibleObjectAttribute("AXToolbarButton");
        public static final AccessibleObjectAXAttribute AXFullScreenButton = createAccessibleObjectAttribute("AXFullScreenButton");

        public static final AccessibleObjectAXAttribute AXTitleUIElement = createAccessibleObjectAttribute("AXTitleUIElement");

        public static final StringAXAttribute AXValue = createStringAttribute("AXValue");
        public static final StringAXAttribute AXIdentifier = createStringAttribute("AXIdentifier");

        public static final Map<String, ? extends AXAttribute<?>> ATTRIBUTE_MAP = ATTRIBUTE_MAP_BUILDER.build();

        static
        {
            ATTRIBUTE_MAP_BUILDER = null;
        }

        private static StringAXAttribute createStringAttribute(String key)
        {
            StringAXAttribute attribute = new StringAXAttribute(key);
            ATTRIBUTE_MAP_BUILDER.put(key, attribute);
            return(attribute);
        }

        private static URLAXAttribute createURLAttribute(String key)
        {
            URLAXAttribute attribute = new URLAXAttribute(key);
            ATTRIBUTE_MAP_BUILDER.put(key, attribute);
            return(attribute);
        }

        private static BooleanAXAttribute createBooleanAttribute(String key)
        {
            BooleanAXAttribute attribute = new BooleanAXAttribute(key);
            ATTRIBUTE_MAP_BUILDER.put(key, attribute);
            return(attribute);
        }

        private static AccessibleObjectAXAttribute createAccessibleObjectAttribute(String key)
        {
            AccessibleObjectAXAttribute attribute = new AccessibleObjectAXAttribute(key);
            ATTRIBUTE_MAP_BUILDER.put(key, attribute);
            return(attribute);
        }

        private static AccessibleObjectListAXAttribute createAccessibleObjectListAttribute(String key)
        {
            AccessibleObjectListAXAttribute attribute = new AccessibleObjectListAXAttribute(key);
            ATTRIBUTE_MAP_BUILDER.put(key, attribute);
            return(attribute);
        }

        private static PositionAXAttribute createPositionAttribute(String key)
        {
            PositionAXAttribute attribute = new PositionAXAttribute(key);
            ATTRIBUTE_MAP_BUILDER.put(key, attribute);
            return(attribute);
        }

        private static SizeAXAttribute createSizeAttribute(String key)
        {
            SizeAXAttribute attribute = new SizeAXAttribute(key);
            ATTRIBUTE_MAP_BUILDER.put(key, attribute);
            return(attribute);
        }

    }
}
