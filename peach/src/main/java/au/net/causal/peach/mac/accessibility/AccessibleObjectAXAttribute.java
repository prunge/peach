package au.net.causal.peach.mac.accessibility;

public class AccessibleObjectAXAttribute extends AXAttribute<AccessibleObject>
{
    public AccessibleObjectAXAttribute(String key)
    {
        super(key);
    }

    @Override
    public AccessibleObject read(AccessibleObject from)
    {
        return(from.getAttributeAccessibleObject(getKey()));
    }
}
