package au.net.causal.peach.mac.accessibility;

import java.util.List;

public class AccessibleObjectListAXAttribute extends AXAttribute<List<? extends AccessibleObject>>
{
    public AccessibleObjectListAXAttribute(String key)
    {
        super(key);
    }

    @Override
    public List<? extends AccessibleObject> read(AccessibleObject from)
    {
        return(from.getAttributeAccessibleObjects(getKey()));
    }
}
