package au.net.causal.peach.mac.accessibility;

public class BooleanAXAttribute extends WritableAXAttribute<Boolean>
{
    public BooleanAXAttribute(String key)
    {
        super(key);
    }

    @Override
    public Boolean read(AccessibleObject from)
    {
        return(from.getAttributeBoolean(getKey()));
    }

    @Override
    public void write(AccessibleObject to, Boolean value)
    {
        to.setAttributeBoolean(getKey(), value);
    }
}
