package au.net.causal.peach.mac.accessibility;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

import au.net.causal.peach.mac.CFStringRef;
import au.net.causal.peach.mac.CoreFoundation;

public class CFURLRef extends PointerByReference
{
    public CFURLRef()
    {
    }

    public CFURLRef(Pointer pointer)
    {
        super(pointer);
    }

    public CFStringRef toStringRef()
    {
        CFStringRef ref = CoreFoundation.INSTANCE.CFURLGetString(this.getValue());
        return(new CFStringRef(ref.getPointer()));
    }

    public String getStringValue()
    {
        CFStringRef string = toStringRef();
        return(string.getStringValue());
    }
}
