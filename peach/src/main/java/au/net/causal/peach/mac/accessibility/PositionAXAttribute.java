package au.net.causal.peach.mac.accessibility;

import java.awt.geom.Point2D;

public class PositionAXAttribute extends WritableAXAttribute<Point2D.Double>
{
    public PositionAXAttribute(String key)
    {
        super(key);
    }

    @Override
    public Point2D.Double read(AccessibleObject from)
    {
        return(from.getAttributePosition(getKey()));
    }

    @Override
    public void write(AccessibleObject to, Point2D.Double value)
    {
        to.setAttributePosition(getKey(), value);
    }
}
