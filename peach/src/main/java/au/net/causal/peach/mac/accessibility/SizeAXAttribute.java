package au.net.causal.peach.mac.accessibility;

import au.net.causal.peach.mac.Size;

public class SizeAXAttribute extends WritableAXAttribute<Size>
{
    public SizeAXAttribute(String key)
    {
        super(key);
    }

    @Override
    public Size read(AccessibleObject from)
    {
        return(from.getAttributeSize(getKey()));
    }

    @Override
    public void write(AccessibleObject to, Size value)
    {
        to.setAttributeSize(getKey(), value);
    }
}
