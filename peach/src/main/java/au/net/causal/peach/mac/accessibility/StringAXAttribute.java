package au.net.causal.peach.mac.accessibility;

public class StringAXAttribute extends WritableAXAttribute<String>
{
    public StringAXAttribute(String key)
    {
        super(key);
    }

    @Override
    public String read(AccessibleObject from)
    {
        return(from.getAttributeString(getKey()));
    }

    @Override
    public void write(AccessibleObject to, String value)
    {
        to.setAttributeString(getKey(), value);
    }
}
