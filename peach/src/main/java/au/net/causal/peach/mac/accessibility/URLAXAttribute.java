package au.net.causal.peach.mac.accessibility;

public class URLAXAttribute extends AXAttribute<String>
{
    public URLAXAttribute(String key)
    {
        super(key);
    }

    @Override
    public String read(AccessibleObject from)
    {
        return(from.getAttributeURL(getKey()));
    }
}
