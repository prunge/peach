package au.net.causal.peach.mac.accessibility;

public abstract class WritableAXAttribute<T> extends AXAttribute<T>
{
    protected WritableAXAttribute(String key)
    {
        super(key);
    }

    public abstract void write(AccessibleObject to, T value);

}
