package au.net.causal.peach.nashorn;

import java.lang.invoke.MethodHandles;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import jdk.nashorn.internal.objects.NativeRegExp;
import au.net.causal.peach.Desktop;
import au.net.causal.peach.NativeWindow;

@SuppressWarnings("restriction")
public interface NashornDesktop extends Desktop
{
	public static NashornDesktop wrap(final Desktop desktop)
	{
		List<Class<?>> list = new ArrayList<>(Arrays.asList(desktop.getClass().getInterfaces()));
		list.add(NashornDesktop.class);
		Class<?>[] iArray = list.toArray(new Class<?>[list.size()]);
		return((NashornDesktop)Proxy.newProxyInstance(desktop.getClass().getClassLoader(), iArray, 
				new InvocationHandler()
				{
					@Override
					public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
					{
						if (NashornDesktop.class == method.getDeclaringClass())
						{
							return(MethodHandles.lookup()
							  .in(NashornDesktop.class)
							  .unreflectSpecial(method, NashornDesktop.class)
							  .bindTo(proxy)
							  .invokeWithArguments(args));
						}
						else
							return(method.invoke(desktop, args));
					}
				}));
	}
	
	public default List<? extends NativeWindow> findWindowsWithTitleMatching(NativeRegExp pattern)
	{
		Objects.requireNonNull(pattern, "pattern == null");
		return(findWindows(w -> 
					{ 
						String t = w.getTitle();
						if (t == null)
							return(false);
						
						return((Boolean)pattern.test(w.getTitle()));
					}));
	}
	
	public default NativeWindow findWindowWithTitleMatching(NativeRegExp pattern)
	{
		Objects.requireNonNull(pattern, "pattern == null");
		return(findWindowsWithTitleMatching(pattern).stream().findFirst().orElse(null));
	}
}
