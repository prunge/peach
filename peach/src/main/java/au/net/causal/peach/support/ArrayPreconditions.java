package au.net.causal.peach.support;

import java.util.Objects;

public final class ArrayPreconditions
{
	private ArrayPreconditions()
	{
	}
	
	public static <T> T[] requireElementsNonNull(T[] values, String variableName)
	{
		Objects.requireNonNull(values, variableName + " == null");
        for (int i = 0; i < values.length; i++)
        {
        	Object value = values[i];
        	Objects.requireNonNull(value, variableName + "[" + i + "] == null");
        }
        return(values);
	}

}
