package au.net.causal.peach.support;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class CompositeObjectMaker implements ObjectMaker
{
	private final List<? extends ObjectMaker> makers;
	
	public CompositeObjectMaker(List<? extends ObjectMaker> makers)
	{
		Objects.requireNonNull(makers, "makers == null");
		this.makers = Collections.unmodifiableList(new ArrayList<>(makers));
	}
	
	public CompositeObjectMaker(ObjectMaker... makers)
	{
		this(Arrays.asList(makers));
	}
	
	@Override
	public <T> T make(Map<String, ?> map, Class<T> type)
	{
		Objects.requireNonNull(map, "map == null");
		Objects.requireNonNull(type, "type == null");
		
		for (ObjectMaker maker : makers)
		{
			T result = maker.make(map, type);
			if (result != null)
				return(result);
		}
		
		return(null);
	}
}
