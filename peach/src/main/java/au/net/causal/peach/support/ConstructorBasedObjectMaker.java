package au.net.causal.peach.support;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public abstract class ConstructorBasedObjectMaker implements ObjectMaker
{
	private final TypeConverter typeConverter;
	
	protected ConstructorBasedObjectMaker(TypeConverter typeConverter)
	{
		Objects.requireNonNull(typeConverter, "typeConverter == null");
		this.typeConverter = typeConverter;
	}
	
	protected Set<String> readAliases(String parameterName, Parameter parameter)
	{
		Alias aliasAnnotation = parameter.getAnnotation(Alias.class);
		if (aliasAnnotation == null)
			return(Collections.singleton(parameterName));
		else
		{
			Set<String> aliases = new LinkedHashSet<>();
			aliases.add(parameterName);
			aliases.addAll(Arrays.asList(aliasAnnotation.value()));
			return(aliases);
		}
	}
	
	private <V> V findMappedValue(Map<String, V> map, AliasedProperty ap, Set<String> unprocessedMapKeys)
	{
		for (String alias : ap.getAllNames())
		{
			if (map.containsKey(alias))
			{
				V value = map.get(alias);
				unprocessedMapKeys.remove(alias);
				return(value);
			}
		}
		
		return(null);
	}
	
	private <T> T makeByConstructor(MappedConstructor<T> mappedConstructor, Map<String, ?> map)
	{
		Constructor<T> constructor = mappedConstructor.getConstructor();
		List<? extends AliasedProperty> constructorPropertyNames = mappedConstructor.getPropertyNames();
		
		Object[] parameterValues = new Object[constructorPropertyNames.size()];
		
		int i = 0;
		Set<String> unprocessedMapKeys = new HashSet<>(map.keySet());
		for (AliasedProperty ap : constructorPropertyNames)
		{
			Object parameterValue = findMappedValue(map, ap, unprocessedMapKeys);
			Type expectedType = constructor.getGenericParameterTypes()[i];
			Class<?> expectedRawType = constructor.getParameterTypes()[i];
			parameterValues[i] = typeConverter.convert(parameterValue, expectedRawType, expectedType);
			i++;
		}
		
		//Validate that all map keys are processed
		if (!unprocessedMapKeys.isEmpty())
			throw new NoSuchPropertyException(constructor.getDeclaringClass(), unprocessedMapKeys);
		
		try
		{
			return(constructor.newInstance(parameterValues));
		}
		catch (IllegalAccessException e)
		{
			IllegalAccessError err = new IllegalAccessError(e.getMessage());
			err.initCause(e);
			throw err;
		}
		catch (InstantiationException e)
		{
			InstantiationError err = new InstantiationError(e.getMessage());
			err.initCause(e);
			throw err;
		}
		catch (InvocationTargetException e)
		{
			if (e.getCause() instanceof RuntimeException)
				throw ((RuntimeException)e.getCause());
			else if (e.getCause() instanceof Error)
				throw ((Error)e.getCause());
			else
				throw new RuntimeException(e.getCause());
		}
	}
	
	protected abstract <T> MappedConstructor<T> findConstructor(Class<T> type, Set<String> parameterNames);
	
	@Override
	public <T> T make(Map<String, ?> map, Class<T> type)
	{
		MappedConstructor<T> constructor = findConstructor(type, map.keySet());
		if (constructor == null)
			return(null);
		
		return(makeByConstructor(constructor, map));
	}
	
	protected static class AliasedProperty
	{
		private final String realPropertyName;
		private final Set<String> allNames;
		
		public AliasedProperty(String realPropertyName, Set<String> allNames)
		{
			this.realPropertyName = realPropertyName;
			this.allNames = Collections.unmodifiableSet(new LinkedHashSet<>(allNames));
		}
		
		public String getRealPropertyName()
		{
			return(realPropertyName);
		}
		
		public Set<String> getAllNames()
		{
			return(allNames);
		}
		
		@Override
		public String toString()
		{
			return(realPropertyName + ":" + allNames.toString());
		}
	}
	
	protected static class MappedConstructor<T>
	{
		private final Constructor<T> constructor;
		private final List<? extends AliasedProperty> propertyNames;
		
		public MappedConstructor(Constructor<T> constructor, List<? extends AliasedProperty> propertyNames)
		{
			this.constructor = constructor;
			this.propertyNames = Collections.unmodifiableList(new ArrayList<>(propertyNames));
		}

		public Constructor<T> getConstructor()
		{
			return(constructor);
		}

		public List<? extends AliasedProperty> getPropertyNames()
		{
			return(propertyNames);
		}

		@Override
		public String toString()
		{
			return "MappedConstructor [constructor=" + constructor + ", propertyNames=" + propertyNames + "]";
		}
	}
}
