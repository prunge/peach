package au.net.causal.peach.support;

import java.lang.reflect.Constructor;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ConstructorNamedParameterObjectMaker extends ConstructorBasedObjectMaker
{
	public ConstructorNamedParameterObjectMaker(TypeConverter typeConverter)
	{
		super(typeConverter);
	}

	private List<AliasedProperty> readParameterNames(Constructor<?> constructor)
	{
		Parameter[] parameters = constructor.getParameters();
		
		List<AliasedProperty> names = new ArrayList<>(parameters.length);
		
		//TODO better streamification
		
		for (int i = 0; i < parameters.length; i++)
		{
			Parameter parameter = parameters[i];
			
			//If no real names present just get out
			if (!parameter.isNamePresent())
				return(null);
			
			names.add(new AliasedProperty(parameter.getName(), readAliases(parameter.getName(), parameter)));
		}
		
		return(names);
	}
	
	private <T> Constructor<T> findLongestConstructor(Class<T> type)
	{
		Constructor<T> longest = null;
		int longestLength = -1;
		
		for (@SuppressWarnings("unchecked") Constructor<T> constructor : (Constructor<T>[])type.getConstructors())
		{
			if (constructor.getParameterCount() > longestLength)
			{
				longest = constructor;
				longestLength = constructor.getParameterCount();
			}
		}
		
		return(longest);
	}
	
	@Override
	protected <T> MappedConstructor<T> findConstructor(Class<T> type, Set<String> specifiedPropertyNames)
	{
		Constructor<T> constructor = findLongestConstructor(type);
		if (constructor == null)
			return(null);
		
		List<AliasedProperty> constructorPropertyNames = readParameterNames(constructor);
		if (constructorPropertyNames == null)
			return(null);
		
		return(new MappedConstructor<>(constructor, constructorPropertyNames));
	}

}
