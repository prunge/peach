package au.net.causal.peach.support;

import java.beans.ConstructorProperties;
import java.lang.reflect.Constructor;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ConstructorPropertiesObjectMaker extends ConstructorBasedObjectMaker
{
	public ConstructorPropertiesObjectMaker(TypeConverter typeConverter)
	{
		super(typeConverter);
	}

	@Override
	protected <T> MappedConstructor<T> findConstructor(Class<T> type, Set<String> specifiedPropertyNames)
	{
		for (@SuppressWarnings("unchecked") Constructor<T> constructor : (Constructor<T>[])type.getConstructors())
		{
			ConstructorProperties cp = constructor.getAnnotation(ConstructorProperties.class);
			if (cp != null)
			{
				String[] constructorPropertyNames = cp.value();
				Parameter[] parameters = constructor.getParameters();
				List<AliasedProperty> apList = new ArrayList<>(constructorPropertyNames.length);
				
				//TODO validate sizes are the same?
				
				//Read aliases
				for (int i = 0; i < constructorPropertyNames.length; i++)
				{
					String constructorPropertyName = constructorPropertyNames[i];
					Set<String> aliases = readAliases(constructorPropertyName, parameters[i]);
					AliasedProperty ap = new AliasedProperty(constructorPropertyName, aliases);
					apList.add(ap);
				}
				
				return(new MappedConstructor<>(constructor, apList));
			}
		}
		
		return(null);
	}

}
