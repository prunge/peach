package au.net.causal.peach.support;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Occurs when in script an attempt is made to set a property that does not exist through map initialization.
 * <p>
 * 
 * For example, if we have a <code>Bird</code> with <code>name</code> and <code>age</code> properties, and a script attempts the following:
 * <pre>
nest.addBird({name: 'Squinky', age: 13, someOtherProperty: 'whatever'});
</pre>
 * then a <code>NoSuchPropertyException</code> will be thrown because there is no 'someOtherProperty' property the <code>Bird</code> class.
 */
public class NoSuchPropertyException extends RuntimeException
{
	private final Class<?> target;
	private final Set<String> properties;
	
	/**
	 * Creates a new <code>NoSuchPropertyException</code> with the default detail message.
	 * 
	 * @param target the target class where properties could not be found.
	 * @param properties the names of the properties that could not be found.
	 */
	public NoSuchPropertyException(Class<?> target, Set<String> properties)
	{
		this(target, properties, createDefaultMessage(target, properties));
	}

	/**
	 * Generates a default exception message.
	 * 
	 * @param target the target class where properties could not be found.
	 * @param properties the names of the properties that could not be found.
	 * 
	 * @return the default exception message.
	 */
	private static String createDefaultMessage(Class<?> target, Set<String> properties)
	{
		return(target.getCanonicalName() + " does not have properties: " + properties);
	}

	/**
	 * Creates a new <code>NoSuchPropertyException</code> with the specified detail message.
	 * 
	 * @param target the target class where properties could not be found.
	 * @param properties the names of the properties that could not be found.
	 * @param message the detail message.
	 */
	public NoSuchPropertyException(Class<?> target, Set<String> properties, String message)
	{
		this(target, properties, message, null);		
	}

	/**
	 * Creates a new <code>NoSuchPropertyException</code> with a default detail message and the specified cause.
	 * 
	 * @param target the target class where properties could not be found.
	 * @param properties the names of the properties that could not be found.
	 * @param cause the cause exception.
	 */
	public NoSuchPropertyException(Class<?> target, Set<String> properties, Throwable cause)
	{
		this(target, properties, createDefaultMessage(target, properties), cause);
	}

	/**
	 * Creates a new <code>NoSuchPropertyException</code> with the specified detail message and cause.
	 * 
	 * @param target the target class where properties could not be found.
	 * @param properties the names of the properties that could not be found.
	 * @param message the detail message.
	 * @param cause the cause exception.
	 */
	public NoSuchPropertyException(Class<?> target, Set<String> properties, String message, Throwable cause)
	{
		super(message, cause);
		this.target = target;
		this.properties = Collections.unmodifiableSet(new LinkedHashSet<>(properties));
	}

	/**
	 * Creates a new <code>NoSuchPropertyException</code> with the specified detail message and cause and with configurable suppression and stack trace
	 * writability.
	 * 
	 * @param target the target class where properties could not be found.
	 * @param properties the names of the properties that could not be found.
	 * @param message the detail message.
	 * @param cause the cause exception.
	 * @param enableSuppression true to enable suppression, false otherwise.
	 * @param writableStackTrace true to allow stack trace to be writable, false otherwise.
	 */
	public NoSuchPropertyException(Class<?> target, Set<String> properties,  String message, Throwable cause, 
			boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause);
		this.target = target;
		this.properties = new LinkedHashSet<>(properties);
	}

	/**
	 * @return the class whose properties could not be found.
	 */
	public Class<?> getTarget()
	{
		return(target);
	}

	/**
	 * @return the set of properties that could not be found on the target class.  The returned set is unmodifiable.
	 */
	public Set<String> getProperties()
	{
		return(properties);
	}
}
