package au.net.causal.peach.support;

import java.util.Map;

public interface ObjectMaker
{
	public <T> T make(Map<String, ?> map, Class<T> type);
}
