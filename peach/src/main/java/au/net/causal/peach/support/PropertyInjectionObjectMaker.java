package au.net.causal.peach.support;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class PropertyInjectionObjectMaker implements ObjectMaker
{
	private final TypeConverter typeConverter;
	
	public PropertyInjectionObjectMaker(TypeConverter typeConverter)
	{
		Objects.requireNonNull(typeConverter, "typeConverter == null");
		this.typeConverter = typeConverter;
	}

	@Override
	public <T> T make(Map<String, ?> map, Class<T> type)
	{
		//- Attempt to find zero-arg constructor and use setters
		try
		{
			T obj = type.getConstructor().newInstance();
			configureObject(obj, map);
			return(obj);
		}
		catch (IllegalAccessException e)
		{
			IllegalAccessError err = new IllegalAccessError(e.getMessage());
			err.initCause(e);
			throw err;
		}
		catch (InstantiationException e)
		{
			InstantiationError err = new InstantiationError(e.getMessage());
			err.initCause(e);
			throw err;
		}
		catch (InvocationTargetException e)
		{
			if (e.getCause() instanceof RuntimeException)
				throw ((RuntimeException)e.getCause());
			else if (e.getCause() instanceof Error)
				throw ((Error)e.getCause());
			else
				throw new RuntimeException(e.getCause());
		}
		catch (NoSuchMethodException e)
		{
			//No zero-arg constructor
			return(null);
		}
	}
	
	private void configureObject(Object obj, Map<String, ?> map)
	{
		BeanInfo beanInfo;
		try
		{
			beanInfo = Introspector.getBeanInfo(obj.getClass());
		}
		catch (IntrospectionException e)
		{
			throw new RuntimeException("Cannot introspect bean: " + obj.getClass(), e);
		}
		
		Map<String, PropertyDescriptor> propertyDescriptorMap = new HashMap<>();
		for (PropertyDescriptor pd : beanInfo.getPropertyDescriptors()) //TODO handle nulls?
		{
			propertyDescriptorMap.put(pd.getName(), pd);
		}
		
		for (Map.Entry<String, ?> entry : map.entrySet())
		{
			String propertyName = entry.getKey();
			PropertyDescriptor pd = propertyDescriptorMap.get(propertyName);
			if (pd != null)
			{
				Method propertyWriteMethod = pd.getWriteMethod();
				if (propertyWriteMethod == null) //TODO
					throw new RuntimeException("Unsettable property " + propertyName + " on " + obj.getClass().getCanonicalName());
				
				Object propertyValue = entry.getValue();
				propertyValue = typeConverter.convert(propertyValue, propertyWriteMethod.getParameterTypes()[0], propertyWriteMethod.getGenericParameterTypes()[0]);
				
				try
				{
					propertyWriteMethod.invoke(obj, propertyValue);
				}
				catch (InvocationTargetException e)
				{
					if (e.getCause() instanceof RuntimeException)
						throw ((RuntimeException)e.getCause());
					else if (e.getCause() instanceof Error)
						throw ((Error)e.getCause());
					else
						throw new RuntimeException(e.getCause());
				}
				catch (IllegalAccessException e)
				{
					IllegalAccessError err = new IllegalAccessError(e.getMessage());
					err.initCause(e);
					throw err;
				}
			}
			else
			{
				//Ignore the property?
				throw new RuntimeException("Unknown property " + propertyName + " on " + obj.getClass().getCanonicalName());
			}
		}

	}
}
