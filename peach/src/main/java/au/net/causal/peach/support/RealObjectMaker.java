package au.net.causal.peach.support;

import java.util.Map;

public final class RealObjectMaker
{
	private static final TypeConverter TYPE_CONVERTER = new StandardTypeConverter();
	
	private static final CompositeObjectMaker MAKER = new CompositeObjectMaker(
														new ConstructorPropertiesObjectMaker(TYPE_CONVERTER),
														new PropertyInjectionObjectMaker(TYPE_CONVERTER),
														new ConstructorNamedParameterObjectMaker(TYPE_CONVERTER));
	
	private RealObjectMaker()
	{
	}
	
	public static <T> T make(Map<String, ?> map, Class<T> type)
	{
		T result = MAKER.make(map, type);
		if (result == null)
			throw new RuntimeException("Could not create instance of " + type.getCanonicalName() + " using any available generation methods.");
			
		return(result);
	}
}
