package au.net.causal.peach.support;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import jdk.nashorn.api.scripting.ScriptObjectMirror;
import jdk.nashorn.api.scripting.ScriptUtils;

@SuppressWarnings("restriction")
public class StandardTypeConverter implements TypeConverter
{
	private static final Map<Class<?>, Function<Object[], Collection<?>>> COLLECTION_MAKERS;
	static
	{
		Map<Class<?>, Function<Object[], Collection<?>>> collectionMakers = new HashMap<>();
		
		collectionMakers.put(List.class, array -> Arrays.asList(array));
		collectionMakers.put(Set.class, array -> new HashSet<>(Arrays.asList(array)));
		collectionMakers.put(Collection.class, array -> Arrays.asList(array));
		
		COLLECTION_MAKERS = Collections.unmodifiableMap(collectionMakers);
	}
	
	private static final Map<Class<?>, Object> PRIMITIVE_DEFAULTS;
	static
	{
		Map<Class<?>, Object> primitiveDefaults = new HashMap<>();
		
		primitiveDefaults.put(Long.TYPE, 0L);
		primitiveDefaults.put(Integer.TYPE, 0);
		primitiveDefaults.put(Short.TYPE, Short.valueOf((short)0));
		primitiveDefaults.put(Byte.TYPE, Byte.valueOf((byte)0));
		primitiveDefaults.put(Boolean.TYPE, Boolean.FALSE);
		primitiveDefaults.put(Character.TYPE, '\0');
		primitiveDefaults.put(Double.TYPE, 0.0);
		primitiveDefaults.put(Float.TYPE, 0.0f);
		
		PRIMITIVE_DEFAULTS = Collections.unmodifiableMap(primitiveDefaults);
	}
	
	@Override
	public Object convert(Object value, Class<?> toRawType, Type toGenericType)
	{
		//Unwrapping script objects
		//value = unwrapScriptObject(value);			
		
		//If we are receiving a string, scan the target type for public static final fields and do case-insensitive search
		if (value instanceof CharSequence && !toRawType.isArray() && !toRawType.isPrimitive() && 
				!Collection.class.isAssignableFrom(toRawType) && !Number.class.isAssignableFrom(toRawType))
		{
			value = convertStringToConstantField((CharSequence)value, toRawType);
		}
		
		//We can convert some collections to arrays
		Object col = convertToCollection(value, toRawType, toGenericType);
		if (col != null)
			return(col);
		
		//Array conversion
		if (value != null && toRawType.isArray())
		{
			if (value instanceof ScriptObjectMirror)
			{
				ScriptObjectMirror som = (ScriptObjectMirror)value;
				if (som.isArray())
				{
					int length = ((Number)som.get("length")).intValue();
					Class<?> elementType = toRawType.getComponentType();
					Object array = Array.newInstance(elementType, length);
					for (int i = 0; i < length; i++)
					{
						Object elementValue = som.getSlot(i);
						Object convertedElementValue = convert(elementValue, elementType, elementType);
						Array.set(array, i, convertedElementValue);
					}
					value = array;
				}
			}
			
			
			if (!(value instanceof Object[]))
				value = new Object[] {value};
			
			Object converted = convertArray((Object[])value, toRawType);
			if (converted != null)
				return(converted);
		}

		//Default values for primitives
		if (toRawType.isPrimitive() && value == null)
			value = primitiveDefault(toRawType);
		
		//Numeric conversion / other conversion / classcastexception if conversion fails
		value = ScriptUtils.convert(value, toRawType);
		
		return(value);
	}
	
	private Object primitiveDefault(Class<?> primitiveType)
	{
		return(PRIMITIVE_DEFAULTS.get(primitiveType));
	}
	
	private Object convertToCollection(Object value, Class<?> toRawType, Type toGenericType)
	{
		if (value == null)
			return(null);
		if (!COLLECTION_MAKERS.keySet().contains(toRawType) || !(toGenericType instanceof ParameterizedType))
			return(null);
		
		ParameterizedType pType = (ParameterizedType)toGenericType;
		Type elementType = pType.getActualTypeArguments()[0];
		if (elementType instanceof ParameterizedType)
			elementType = ((ParameterizedType)elementType).getRawType();
		
		if (!(elementType instanceof Class<?>))
			return(null);
		
		Class<?> elementRawType = (Class<?>)elementType;
		Class<?> elementArrayType = Array.newInstance(elementRawType, 0).getClass();
		
		Object[] arrayValue = (Object[])convert(value, elementArrayType, elementArrayType);
		if (arrayValue == null)
			return(null);
		
		//Convert array to collection
		Collection<?> convertedValue = COLLECTION_MAKERS.get(toRawType).apply(arrayValue);
		
		return(convertedValue);
	}
	
	private Object convertStringToConstantField(CharSequence value, Class<?> toRawType)
	{
		//Static/final field with same type as target type and same name (case insensitive)
		Optional<Field> foundField = Arrays.stream(toRawType.getFields())
												.filter(field -> Modifier.isStatic(field.getModifiers()) && 
																Modifier.isFinal(field.getModifiers()) && 
																toRawType.equals(field.getType()) && 
																field.getName().toUpperCase(Locale.ENGLISH).equals(value.toString().toUpperCase(Locale.ENGLISH)))
												.findAny();
		if (foundField.isPresent())
		{
			try
			{
				return(foundField.get().get(null));
			}
			catch (IllegalAccessException e)
			{
				IllegalAccessError err = new IllegalAccessError(e.getMessage());
				err.initCause(e);
				throw err;
			}
		}
		else
			return(value);
	}
	
	private Object convertArray(Object[] value, Class<?> targetType)
	{
		Class<?> targetElementType = targetType.getComponentType();
		Object targetArray = Array.newInstance(targetElementType, value.length);
		for (int i = 0; i < value.length; i++)
		{
			Object convertedValue = convert(value[i], targetElementType, targetElementType);
			Array.set(targetArray, i, convertedValue);
		}
		
		return(targetArray);
	}

}
