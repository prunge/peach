package au.net.causal.peach.support;

import java.lang.reflect.Type;

@FunctionalInterface
public interface TypeConverter
{
	public Object convert(Object value, Class<?> toRawType, Type toGenericType);
}
