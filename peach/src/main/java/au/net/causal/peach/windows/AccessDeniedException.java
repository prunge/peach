package au.net.causal.peach.windows;

public class AccessDeniedException extends Win32Exception
{
	public AccessDeniedException(int errorCode, String message)
	{
		super(errorCode, message);
	}
}
