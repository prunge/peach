package au.net.causal.peach.windows;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.win32.W32APIOptions;

public interface Kernel32X extends Kernel32
{
	Kernel32X INSTANCE = (Kernel32X) Native.loadLibrary("kernel32", Kernel32X.class, W32APIOptions.DEFAULT_OPTIONS);
	
	
}
