package au.net.causal.peach.windows;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.WinDef.HMODULE;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.win32.StdCallLibrary;
import com.sun.jna.win32.W32APIOptions;

public interface PsApi extends StdCallLibrary 
{
	PsApi INSTANCE = (PsApi)Native.loadLibrary("psapi", PsApi.class, W32APIOptions.DEFAULT_OPTIONS);
	
	int PROCESS_QUERY_INFORMATION = 0x0400;
	int PROCESS_QUERY_LIMITED_INFORMATION = 0x1000;
	int PROCESS_VM_READ = 0x0010;
	
	int GetProcessImageFileName(HANDLE hProcess, char[] lpFilename, int nSize);
	int GetModuleFileNameEx(HANDLE hProcess, HMODULE hModule, char[] lpFilename, int nSize);
}
