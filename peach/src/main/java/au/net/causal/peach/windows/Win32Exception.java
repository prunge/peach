package au.net.causal.peach.windows;

public class Win32Exception extends RuntimeException
{
	private final int errorCode;
	
	public Win32Exception(int errorCode, String message)
	{
		super(errorCode + ": " + message);
		this.errorCode = errorCode;
	}
	
	public int getErrorCode()
	{
		return(errorCode);
	}
}
