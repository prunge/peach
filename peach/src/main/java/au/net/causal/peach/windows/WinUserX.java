package au.net.causal.peach.windows;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.platform.win32.BaseTSD.ULONG_PTR;
import com.sun.jna.platform.win32.WinUser;

public interface WinUserX extends WinUser
{
	int WH_GETMESSAGE = 3;
	int WH_CALLWNDPROC = 4;
	int WH_CALLWNDPROCRET = 12;
	int WH_JOURNALRECORD = 0; 
	int WH_SHELL = 10;
	
	int VK_LWIN = 0x5b;
	int VK_RWIN = 0x5c;
	int VK_APPS = 0x5d;
	
	int VK_BACK = 0x08; 
	int VK_TAB = 0x09; 
	int VK_CLEAR = 0x0c;
	int VK_RETURN = 0x0d;
	int VK_PAUSE = 0x13;
	int VK_CAPITAL = 0x14;
	int VK_KANA = 0x15;
	int VK_ESCAPE = 0x1b;
	int VK_SPACE = 0x20; 
	int VK_PRIOR = 0x21;
	int VK_NEXT = 0x22; 
	int VK_END = 0x23;
	int VK_HOME = 0x24;
	
	int VK_LEFT = 0x25;
	int VK_UP = 0x26;
	int VK_RIGHT = 0x27;
	int VK_DOWN = 0x28;
	
	int VK_SELECT = 0x29;
	int VK_SNAPSHOT = 0x2c;
	
	int VK_INSERT = 0x2d;
	int VK_DELETE = 0x2e;
	
	int VK_HELP = 0x2f;
	int VK_SLEEP = 0x5f;
	
	int VK_NUMPAD0 = 0x60; 
	int VK_NUMPAD1 = 0x61;
	int VK_NUMPAD2 = 0x62;
	int VK_NUMPAD3 = 0x63;
	int VK_NUMPAD4 = 0x64;
	int VK_NUMPAD5 = 0x65;
	int VK_NUMPAD6 = 0x66;
	int VK_NUMPAD7 = 0x67;
	int VK_NUMPAD8 = 0x68;
	int VK_NUMPAD9 = 0x69;
	
	int VK_MULTIPLY = 0x6a; 
	int VK_ADD = 0x6b;
	int VK_SEPARATOR = 0x6c;
	int VK_SUBTRACT = 0x6d;
	int VK_DECIMAL = 0x6e;
	int VK_DIVIDE = 0x6f;
	
	int VK_F1 = 0x70;
	int VK_F2 = 0x71;
	int VK_F3 = 0x72;
	int VK_F4 = 0x73;
	int VK_F5 = 0x74;
	int VK_F6 = 0x75;
	int VK_F7 = 0x76;
	int VK_F8 = 0x77;
	int VK_F9 = 0x78;
	int VK_F10 = 0x79;
	int VK_F11 = 0x7a;
	int VK_F12 = 0x7b;
	int VK_F13 = 0x7c;
	int VK_F14 = 0x7d;
	int VK_F15 = 0x7e;
	int VK_F16 = 0x7f;
	int VK_F17 = 0x80;
	int VK_F18 = 0x81;
	int VK_F19 = 0x82;
	int VK_F20 = 0x83;
	int VK_F21 = 0x84;
	int VK_F22 = 0x85;
	int VK_F23 = 0x86;
	int VK_F24 = 0x87;
	
	int VK_NUMLOCK = 0x90;
	int VK_SCROLL = 0x91;
	
	int VK_OEM_1 = 0xba;
	int VK_OEM_PLUS = 0xbb;
	int VK_OEM_COMMA = 0xbc;
	int VK_OEM_MINUS = 0xbd;
	int VK_OEM_PERIOD = 0xbe;
	int VK_OEM_2 = 0xbf;
	int VK_OEM_3 = 0xc0;
	int VK_OEM_4 = 0xdb; 
	int VK_OEM_5 = 0xdc;
	int VK_OEM_6 = 0xdd;
	int VK_OEM_7 = 0xde;
	
	int VK_LBUTTON = 0x01;
	int VK_RBUTTON = 0x02;
	int VK_MBUTTON = 0x04;
	int VK_XBUTTON1 = 0x05;
	int VK_XBUTTON2 = 0x06;
	
	int WM_MOUSEMOVE = 0x0200;
	int WM_LBUTTONDOWN = 0x0201;
	int WM_LBUTTONUP = 0x0202;
	int WM_LBUTTONDBLCLK = 0x203;
	int WM_RBUTTONDOWN = 0x0204;
	int WM_RBUTTONUP = 0x0205;
	int WM_RBUTTONDBLCLK = 0x206;
	int WM_MBUTTONDOWN = 0x207;
	int WM_MBUTTONUP = 0x208;
	int WM_MBUTTONDBLCLK = 0x209;
	int WM_MOUSEWHEEL = 0x20A;
	int WM_XBUTTONDOWN = 0x20B;
	int WM_XBUTTONUP = 0x20C;
	int WM_XBUTTONDBLCLK = 0x20D;
	int WM_MOUSEHWHEEL = 0x20E;
	
	int SWP_NOMOVE= 0x0002;
	int SWP_NOSIZE = 0x0001;
	
	HWND HWND_TOPMOST = new HWND(Pointer.createConstant(-1));
	HWND HWND_NOTOPMOST = new HWND(Pointer.createConstant(-2));
	HWND HWND_MESSAGE = new HWND(Pointer.createConstant(-3));
	
	short GA_PARENT = 1;
	short GA_ROOT = 2;
	short GA_ROOTOWNER = 3;
	
	int WM_GETICON = 0x007F;
	int WM_SETICON = 0x0080;

	int ICON_BIG = 1;
	int ICON_SMALL = 0;
	int ICON_SMALL2 = 2;
	
	public int GCL_HICONSM = -34;
	public int GCL_HICON = -14;
	
	int DCX_WINDOW = 0x00000001;
	int DCX_CACHE = 0x00000002;
	int DCX_NORESETATTRS = 0x00000004;
	int DCX_CLIPCHILDREN = 0x00000008;
	int DCX_CLIPSIBLINGS = 0x00000010;
	int DCX_PARENTCLIP = 0x00000020;
	int DCX_EXCLUDERGN = 0x00000040;
	int DCX_INTERSECTRGN = 0x00000080;
	int DCX_EXCLUDEUPDATE = 0x00000100;
	int DCX_INTERSECTUPDATE = 0x00000200;
	int DCX_LOCKWINDOWUPDATE = 0x00000400;
	int DCX_VALIDATE = 0x00200000;
	
	public interface CallWndProc extends HOOKPROC
	{
		LRESULT callback(int nCode, WPARAM wParam, CWPSTRUCT lParam);
	}
	
	public interface ShellProc extends HOOKPROC
	{
		LRESULT callback(int nCode, WPARAM wParam, Pointer lParam);
	}	
	
	public class CWPSTRUCT extends Structure
	{
		public LPARAM lParam;
		public WPARAM wParam;
		public short message;
		public HWND hwnd;
		
		@Override
		protected List<String> getFieldOrder() 
		{
			return(Arrays.asList("lParam", "wParam", "message", "hwnd"));
		}
	}
	
	public interface GetMessageProc extends HOOKPROC
	{
		LRESULT callback(int nCode, WPARAM wParam, MSG lParam);
	}
	
	public interface LowLevelMouseProc extends HOOKPROC 
	{
		LRESULT callback(int nCode, WPARAM wParam, MSLLHOOKSTRUCT lParam);
    }
	
	public interface JournalRecordProc extends HOOKPROC
	{
		LRESULT callback(int nCode, WPARAM wParam, EVENTMSG lParam);
	}
	
	public class EVENTMSG extends Structure
	{
		public short message;
		public short paramL;
		public short paramH;
		public int time;
		public HWND hwnd;
		
		@Override
		protected List<String> getFieldOrder()
		{
			return(Arrays.asList("message", "paramL", "paramH", "time", "hwnd"));
		}
	}
	
	public class MSLLHOOKSTRUCT extends Structure 
	{
		public POINT point;
		public int mouseData;
		public int flags;
		public int time;
		public ULONG_PTR dwExtraInfo;
		
		@Override
		protected List<String> getFieldOrder() 
		{
			return(Arrays.asList("point", "mouseData", "flags", "time", "dwExtraInfo"));
		}
	}
	
	public class WINDOWPLACEMENT extends Structure 
	{
        public int length = size();
        public int flags;
        public int showCmd;
        public POINT ptMinPosition;
        public POINT ptMaxPosition;
        public RECT rcNormalPosition;
        
        @Override
        protected List<String> getFieldOrder() 
        {
            return(Arrays.asList("length", "flags", "showCmd", "ptMinPosition", "ptMaxPosition", "rcNormalPosition")); 
        }
	}
	
	public class WNDCLASS extends Structure 
	{
		public short style;
		public WindowProc lpfnWndProc;
		public int cbClsExtra;
		public int cbWndExtra;
		public HINSTANCE hInstance;
		public HICON hIcon;
		public HCURSOR hCursor;
		public HBRUSH    hbrBackground;
		public String   lpszMenuName;
		public String   lpszClassName;
		  
		@Override
		protected List<String> getFieldOrder()
		{
			 return(Arrays.asList("style", "lpfnWndProc", "cbClsExtra", "cbWndExtra", "hInstance", "hIcon", "hCursor", "hbrBackground", "lpszMenuName", "lpszClassName"));
		}

	}
	
	public interface WindowProc
	{
		public LRESULT callback(HWND hwnd, short uMsg, WPARAM wParam, LPARAM lParam);
	}	

	public class HBRUSH extends HINSTANCE 
	{

	}
		
}
