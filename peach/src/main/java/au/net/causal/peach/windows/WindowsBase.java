package au.net.causal.peach.windows;

import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.Kernel32Util;
import com.sun.jna.platform.win32.WinError;
import com.sun.jna.platform.win32.WinNT.HRESULT;

final class WindowsBase
{
	public static final Kernel32X kernel32 = Kernel32X.INSTANCE;
	public static final User32X user32 = User32X.INSTANCE;
	public static final PsApi psApi = PsApi.INSTANCE;
	
	private WindowsBase()
	{
	}
	
	public static RuntimeException createExceptionFromLastError()
	{
		int lastError = Kernel32.INSTANCE.GetLastError();
		String errorMessage = Kernel32Util.formatMessageFromLastErrorCode(lastError);
		
		if (lastError == WinError.ERROR_ACCESS_DENIED)
			throw new AccessDeniedException(lastError, errorMessage);
		
		throw new Win32Exception(lastError, errorMessage);
	}
	
	public static RuntimeException createExceptionFromError(HRESULT error)
	{
		String errorMessage = Kernel32Util.formatMessageFromLastErrorCode(error.intValue());
		
		if (error.intValue() == WinError.ERROR_ACCESS_DENIED)
			throw new AccessDeniedException(error.intValue(), errorMessage);
		
		throw new Win32Exception(error.intValue(), errorMessage);
	}

}
