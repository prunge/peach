package au.net.causal.peach.windows;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Logger;

import au.net.causal.peach.Key;
import au.net.causal.peach.event.ConstrainedKeyDownAdapter;
import au.net.causal.peach.event.ConstrainedKeyUpAdapter;
import au.net.causal.peach.event.ConstrainedMouseDownAdapter;
import au.net.causal.peach.event.ConstrainedMouseUpAdapter;
import au.net.causal.peach.event.ConstrainedMouseWheelAdapter;
import au.net.causal.peach.event.KeyDownListener;
import au.net.causal.peach.event.KeyEvent;
import au.net.causal.peach.event.KeyEventConstraints;
import au.net.causal.peach.event.KeyUpListener;
import au.net.causal.peach.event.MouseButton;
import au.net.causal.peach.event.MouseButtonEvent;
import au.net.causal.peach.event.MouseDownListener;
import au.net.causal.peach.event.MouseEventConstraints;
import au.net.causal.peach.event.MouseUpListener;
import au.net.causal.peach.event.MouseWheelEvent;
import au.net.causal.peach.event.MouseWheelEventConstraints;
import au.net.causal.peach.event.MouseWheelListener;
import au.net.causal.peach.event.StringifiedKeyEventConstraints;
import au.net.causal.peach.windows.WinUserX.LowLevelMouseProc;
import au.net.causal.peach.windows.WinUserX.MSLLHOOKSTRUCT;

import com.google.common.collect.ImmutableSet;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.WinDef.HMODULE;
import com.sun.jna.platform.win32.WinDef.LRESULT;
import com.sun.jna.platform.win32.WinDef.WPARAM;
import com.sun.jna.platform.win32.WinUser;
import com.sun.jna.platform.win32.WinUser.HHOOK;
import com.sun.jna.platform.win32.WinUser.KBDLLHOOKSTRUCT;
import com.sun.jna.platform.win32.WinUser.LowLevelKeyboardProc;
import com.sun.jna.platform.win32.WinUser.MSG;

import static au.net.causal.peach.windows.WindowsBase.*;

public class WindowsDesktopEventsImpl implements WindowsDesktopEvents
{
	private static final Logger log = Logger.getLogger(WindowsDesktopEventsImpl.class.getName());
	
	private final User32X user32 = User32X.INSTANCE;
	
	private final WindowsKeyboard keyboard;
	private final WindowsMouse mouse;
	
    private HHOOK hhk;
    private HHOOK hhkMouse;
    //private HWINEVENTHOOK hhkEvent;
    private LowLevelKeyboardProc keyboardHook;
    private LowLevelMouseProc mouseHook;
	
	private Thread hookThread;
	
	private final List<MouseDownListener> mouseDownListenerList = new CopyOnWriteArrayList<>();
	private final List<MouseUpListener> mouseUpListenerList = new CopyOnWriteArrayList<>();
	private final List<MouseWheelListener> mouseWheelListenerList = new CopyOnWriteArrayList<>();
	
	private final List<KeyDownListener> keyDownListenerList = new CopyOnWriteArrayList<>();
	private final List<KeyUpListener> keyUpListenerList = new CopyOnWriteArrayList<>();
	
	//Keep track of which keys are down so we can eliminate repeat events
	private final boolean[] unextendedKeyDowns = new boolean[256];
	private final boolean[] extendedKeyDowns = new boolean[256];
	
	public WindowsDesktopEventsImpl(WindowsKeyboard keyboard, WindowsMouse mouse)
	{
		Objects.requireNonNull(keyboard, "keyboard == null");
		Objects.requireNonNull(mouse, "mouse == null");
		
		this.keyboard = keyboard;
		this.mouse = mouse;
	}
	
	@Override
	public void clear()
	{
		mouseDownListenerList.clear();
		mouseUpListenerList.clear();
		mouseWheelListenerList.clear();
		keyDownListenerList.clear();
		keyUpListenerList.clear();
	}
	
	@Override
	public void mouseDown(MouseEventConstraints constraints, MouseDownListener listener)
	{
		Objects.requireNonNull(listener, "listener == null");
		
		if (!isStarted())
    		start();
		
		mouseDownListenerList.add(new ConstrainedMouseDownAdapter(listener, constraints));
	}
	
	@Override
	public void mouseUp(MouseEventConstraints constraints, MouseUpListener listener)
	{
		Objects.requireNonNull(listener, "listener == null");
		
		if (!isStarted())
    		start();
		
		mouseUpListenerList.add(new ConstrainedMouseUpAdapter(listener, constraints));
	}
	
	@Override
	public void mouseWheelMove(MouseWheelEventConstraints constraints, MouseWheelListener listener)
	{
		Objects.requireNonNull(listener, "listener == null");
		
		if (!isStarted())
    		start();
		
		mouseWheelListenerList.add(new ConstrainedMouseWheelAdapter(listener, constraints));
	}
	
	@Override
	public void keyDown(KeyEventConstraints constraints, KeyDownListener listener)
	{
		Objects.requireNonNull(listener, "listener == null");
		
		if (!isStarted())
    		start();
		
		keyDownListenerList.add(new ConstrainedKeyDownAdapter(listener, constraints));
	}
	
	@Override
	public void keyUp(KeyEventConstraints constraints, KeyUpListener listener)
	{
		Objects.requireNonNull(listener, "listener == null");
		
		if (!isStarted())
    		start();
		
		keyUpListenerList.add(new ConstrainedKeyUpAdapter(listener, constraints));
	}
	
	@Override
	public void keyDown(StringifiedKeyEventConstraints constraints, KeyDownListener listener)
	{
		KeyEventConstraints keyConstraints;
		if (constraints == null)
			keyConstraints = new KeyEventConstraints(ImmutableSet.of());
		else
			keyConstraints = constraints.toKeyEventConstraints(keyboard);
		
		if (keyConstraints != null)
			keyDown(keyConstraints, listener);
	}

	@Override
	public void keyUp(StringifiedKeyEventConstraints constraints, KeyUpListener listener)
	{
		KeyEventConstraints keyConstraints;
		if (constraints == null)
			keyConstraints = new KeyEventConstraints(ImmutableSet.of());
		else
			keyConstraints = constraints.toKeyEventConstraints(keyboard);
		
		if (keyConstraints != null)
			keyUp(keyConstraints, listener);
	}
	
	protected boolean fireMouseMoved(int x, int y, int timestamp)
    {
		//TODO
    	return(true);
    }
    
    protected boolean fireMouseDown(int x, int y, int timestamp, MouseButton button)
    {
    	MouseButtonEvent event = new MouseButtonEvent(this, x, y, button, timestamp, keyboard.getCurrentState(), mouse.getCurrentState());
    	
    	for (MouseDownListener listener : mouseDownListenerList)
    	{
    		listener.mouseDown(event);
    	}
    	
    	return(!event.isConsumed());
    }
    
    protected boolean fireMouseUp(int x, int y, int timestamp, MouseButton button)
    {
    	MouseButtonEvent event = new MouseButtonEvent(this, x, y, button, timestamp, keyboard.getCurrentState(), mouse.getCurrentState());
    	
    	for (MouseUpListener listener : mouseUpListenerList)
    	{
    		listener.mouseUp(event);
    	}
    	
    	return(!event.isConsumed());
    }
    
    protected boolean fireMouseWheelMoved(int x, int y, int timestamp, int steps, int pixels)
    {
    	MouseWheelEvent event = new MouseWheelEvent(this, x, y, steps, pixels, timestamp, keyboard.getCurrentState(), mouse.getCurrentState());
    	
    	for (MouseWheelListener listener : mouseWheelListenerList)
    	{
    		listener.mouseWheelMoved(event);
    	}
    	
    	return(!event.isConsumed());
    }
    
    protected boolean fireKeyPressed(int nativeKeyCode, int timestamp, boolean extended)
    {
    	//If already down, don't repeat events
    	boolean[] alreadyDownToUse;
    	if (extended)
    		alreadyDownToUse = extendedKeyDowns;
    	else
    		alreadyDownToUse = unextendedKeyDowns;
    	    	
    	boolean alreadyDown = alreadyDownToUse[nativeKeyCode];
    	if (!alreadyDown)
    	{
    		alreadyDownToUse[nativeKeyCode] = true;
    		
	    	Key key = keyboard.forCode(nativeKeyCode, extended);
	    	KeyEvent event = new KeyEvent(this, key, timestamp, keyboard.getCurrentState(), mouse.getCurrentState());
	    	
	    	for (KeyDownListener listener : keyDownListenerList)
	    	{
	    		listener.keyDown(event);
	    	}
	    	
	    	return(!event.isConsumed());
    	}
    	else
    		return(true); //ignored, not consumed
    }
    
    protected boolean fireKeyReleased(int nativeKeyCode, int timestamp, boolean extended)
    {
    	Key key = keyboard.forCode(nativeKeyCode, extended);
    	KeyEvent event = new KeyEvent(this, key, timestamp, keyboard.getCurrentState(), mouse.getCurrentState());
    	
    	if (extended)
    		extendedKeyDowns[nativeKeyCode] = false;
    	else
    		unextendedKeyDowns[nativeKeyCode] = false;
    	
    	for (KeyUpListener listener : keyUpListenerList)
    	{
    		listener.keyUp(event);
    	}
    	
    	return(!event.isConsumed());
    }
	
	private boolean isStarted()
    {
    	return(hookThread != null);
    }
	
	private void start()
    {
    	log.info("Start hook.");
    	
    	final HMODULE hMod = Kernel32.INSTANCE.GetModuleHandle(null);
    	
    	if (keyboardHook == null)
    	{
	        keyboardHook = new LowLevelKeyboardProc() 
	        {
	        	@Override
	            public LRESULT callback(int nCode, WPARAM wParam, KBDLLHOOKSTRUCT info) 
	        	{
	        		boolean consumed = false;
	                if (nCode >= 0) 
	                {
	                    switch(wParam.intValue()) 
	                    {
		                    case WinUser.WM_KEYUP:
		                    case WinUser.WM_KEYDOWN:
		                    case WinUser.WM_SYSKEYUP:
		                    case WinUser.WM_SYSKEYDOWN:
		                    {
		                    	boolean extended = ((info.flags & User32X.LLKHF_EXTENDED) != 0);
	                    		if (wParam.intValue() == WinUser.WM_KEYDOWN || wParam.intValue() == WinUser.WM_SYSKEYDOWN)
	                    			consumed = !fireKeyPressed(info.vkCode, info.time, extended);
	                    		else
	                    			consumed = !fireKeyReleased(info.vkCode, info.time, extended);
	                    		break;
		                    }
	                    }
	                }
	                
	                if (!consumed)
	                	return user32.CallNextHookEx(hhk, nCode, wParam, info.getPointer());
	                else
	                	return(new LRESULT(1L));
	            }
	        };
    	}
    	
    	if (mouseHook == null)
    	{
    		mouseHook = new LowLevelMouseProc()
			{
				@Override
				public LRESULT callback(int nCode, WPARAM wParam, MSLLHOOKSTRUCT info)
				{
					boolean consumed = false;
					if (nCode >= 0)
					{
						switch (wParam.intValue())
						{
							case WinUserX.WM_MOUSEMOVE:
								consumed = !fireMouseMoved(info.point.x, info.point.y, info.time);
								break;
							case WinUserX.WM_LBUTTONDOWN:
								consumed = !fireMouseDown(info.point.x, info.point.y, info.time, MouseButton.LEFT);
								break;
							case WinUserX.WM_RBUTTONDOWN:
								consumed = !fireMouseDown(info.point.x, info.point.y, info.time, MouseButton.RIGHT);
								break;								
							case WinUserX.WM_MBUTTONDOWN:
								consumed = !fireMouseDown(info.point.x, info.point.y, info.time, MouseButton.MIDDLE);
								break;																
							case WinUserX.WM_LBUTTONUP:
								consumed = !fireMouseUp(info.point.x, info.point.y, info.time, MouseButton.LEFT);
								break;
							case WinUserX.WM_RBUTTONUP:
								consumed = !fireMouseUp(info.point.x, info.point.y, info.time, MouseButton.RIGHT);
								break;
							case WinUserX.WM_MBUTTONUP:
								consumed = !fireMouseUp(info.point.x, info.point.y, info.time, MouseButton.MIDDLE);
								break;
							case WinUserX.WM_MOUSEWHEEL:
								int pixels = -(info.mouseData >> 16);
								int steps = (pixels < 0 ? -1 : 1); //Only ever receive one step at a time through hooks
								consumed = !fireMouseWheelMoved(info.point.x, info.point.y, info.time, steps, pixels);
								break;
							case WinUserX.WM_XBUTTONDOWN:
							{
								int xButton = info.mouseData >> 16;
								MouseButton mouseButton = new MouseButton(MouseButton.RIGHT.getId() + xButton);
								consumed = !fireMouseDown(info.point.x, info.point.y, info.time, mouseButton);
								break;
							}
							case WinUserX.WM_XBUTTONUP:
							{
								int xButton = info.mouseData >> 16;
								MouseButton mouseButton = new MouseButton(MouseButton.RIGHT.getId() + xButton);
								consumed = !fireMouseUp(info.point.x, info.point.y, info.time, mouseButton);
								break;
							}
							default:
								log.fine("Received other event: " + wParam.intValue() + " " + info.mouseData);
						}
					}
					
					if (!consumed)
						return user32.CallNextHookEx(hhkMouse, nCode, wParam, info.getPointer());
					else
						return(new LRESULT(1L));
				}
			};
    	}
        
    	if (hookThread == null)
    	{
	        hookThread = new Thread("NativeHook Thread")
	        {
	        	@Override
	        	public void run()
	        	{
	        		hhk = user32.SetWindowsHookEx(WinUser.WH_KEYBOARD_LL, keyboardHook, hMod, 0);
	        		if (hhk == null)
	        			throw createExceptionFromLastError();
	        		
	        		hhkMouse = user32.SetWindowsHookEx(WinUser.WH_MOUSE_LL, mouseHook, hMod, 0);
	        		if (hhkMouse == null)
	        			throw createExceptionFromLastError();
	        		
	        		/*
	        		hhkEvent = user32.SetWinEventHook(User32X.EVENT_MIN, User32X.EVENT_MAX, hMod, eventHook, 0, 0, User32X.WINEVENT_OUTOFCONTEXT | User32X.WINEVENT_SKIPOWNTHREAD);
	        		if (hhkEvent == null)
	        			throw createExceptionFromLastError();
	        		*/
	        		
	        		mainLoop();
	        	}
	        };
	        hookThread.start();
    	}
        
    }
	
	public void mainLoop()
    {
    	 //This bit never returns from GetMessage
		int result;
        MSG msg = new MSG();
        
        log.info("Start main loop.");
        
        while ((result = user32.GetMessage(msg, null, 0, 0)) != 0) 
        {
            if (result == -1) 
            {
                //Error retrieving message, try again
            	log.warning("Error retrieving message: " + result);
                break;
            }
            else 
            {
                user32.TranslateMessage(msg);
                user32.DispatchMessage(msg);
            }
        }
        
        log.info("Out of main event loop");
        
        //If we get here something went wrong
        close();
    }
	
	private void close()
	{
		//TODO cleanup
	}
}
