package au.net.causal.peach.windows;

import java.util.ArrayList;
import java.util.List;

import au.net.causal.peach.DesktopEvents;
import au.net.causal.peach.Keyboard;
import au.net.causal.peach.Mouse;
import au.net.causal.peach.NativeWindow;

import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinUser.WNDENUMPROC;

import static au.net.causal.peach.windows.WindowsBase.*;

public class WindowsDesktopImpl implements WindowsDesktop
{	
	private final WindowsKeyboard keyboard = new WindowsKeyboard();
	private final WindowsMouse mouse = new WindowsMouse();
	private final WindowsDesktopEvents windowsDesktopEvents = new WindowsDesktopEventsImpl(keyboard, mouse);
	
	@Override
	public List<? extends NativeWindow> getWindows()
	{
		final List<WindowsNativeWindowImpl> windows = new ArrayList<>();
		
		boolean ok = user32.EnumWindows(new WNDENUMPROC()
		{
			@Override
			public boolean callback(HWND hWnd, Pointer data)
			{
				//TODO what about proxy wrapping?
				windows.add(new WindowsNativeWindowImpl(hWnd));
				return(true);
			}
		}, null);
		
		if (!ok)
			throw createExceptionFromLastError();
		
		return(windows);
	}

	@Override
	public DesktopEvents getOn()
	{
		return(windowsDesktopEvents);
	}
	
	@Override
	public Keyboard getKeyboard()
	{
		return(keyboard);
	}
	
	@Override
	public Mouse getMouse()
	{
		return(mouse);
	}
}
