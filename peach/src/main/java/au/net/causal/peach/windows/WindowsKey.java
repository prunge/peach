package au.net.causal.peach.windows;

import au.net.causal.peach.Key;
import au.net.causal.peach.KeyPosition;

public class WindowsKey extends Key
{
	private final boolean extended;
	
	public WindowsKey(KeyPosition position, String name, int code)
	{
		this(position, name, code, false, null);
	}

	public WindowsKey(KeyPosition position, String name, int code, Character character)
	{
		this(position, name, code, false, character);
	}
	
	public WindowsKey(KeyPosition position, String name, int code, boolean extended)
	{
		this(position, name, code, extended, null);
	}

	public WindowsKey(KeyPosition position, String name, int code, boolean extended, Character character)
	{
		super(position, name, code, character);
		this.extended = extended;
	}

	public boolean isExtended()
	{
		return(extended);
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + getCode();
		result = prime * result + (extended ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		WindowsKey other = (WindowsKey)obj;
		if (extended != other.extended)
			return false;
		if (getCode() != other.getCode())
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "WindowsKey [extended=" + extended + ", getPosition()=" + getPosition() + ", getName()=" + getName() + ", getCode()=" + Integer.toHexString(getCode())
				+ ", getCharacter()=" + getCharacter() + "]";
	}

	
	
	
}
