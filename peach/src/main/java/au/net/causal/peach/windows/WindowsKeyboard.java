package au.net.causal.peach.windows;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import au.net.causal.peach.Key;
import au.net.causal.peach.KeyPosition;
import au.net.causal.peach.Keyboard;
import au.net.causal.peach.KeyboardControl;
import au.net.causal.peach.KeyboardLocks;
import au.net.causal.peach.KeyboardState;
import au.net.causal.peach.KeyboardStateSnapshot;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Table;

import static au.net.causal.peach.KeyPosition.*;
import static au.net.causal.peach.windows.WindowsBase.*;

public class WindowsKeyboard implements Keyboard
{
	private final Table<String, String, Key> namePositionTable = HashBasedTable.create();
	private final SetMultimap<String, Key> nameMap = HashMultimap.create();
	private final Map<Integer, Key> codeMap = new HashMap<>();
	private final Map<Integer, Key> extendedCodeMap = new HashMap<>();
	
	private final Map<String, KeyPosition> positionMap = new HashMap<>();
	
	private final KeyboardControl control = new WindowsKeyboardControl(this);
	private final KeyboardLocks locks;
	
	public WindowsKeyboard()
	{
		//a-z keys
		for (char c = 'A'; c <= 'Z'; c++)
		{
			char lc = Character.toLowerCase(c);
			register(new WindowsKey(MAIN, String.valueOf(lc), c, lc));
		}
		
		//numbers
		for (char c = '0'; c <= '9'; c++)
		{
			register(new WindowsKey(MAIN, String.valueOf(c), c, c));
		}

		//Numeric keypad numbers
		for (int i = 0; i <= 9; i++)
		{
			char c = (char)('0' + i);
			register(new WindowsKey(KEYPAD, String.valueOf(c), 0x60 + i, c));
		}
		
		//F1-F24
		for (int i = 1; i <= 24; i++)
		{
			String name = "f" + i;
			register(new WindowsKey(MAIN, name, 0x70 + (i - 1)));
		}

		//And the rest
		register(new WindowsKey(MAIN, "backspace", 0x08));
		register(new WindowsKey(MAIN, "tab", 0x09, '\t'));
		register(new WindowsKey(MAIN, "clear", 0x0c));
		register(new WindowsKey(KEYPAD, "enter", 0x0d, true, '\n'), "return");
		register(new WindowsKey(MAIN, "enter", 0x0d, false, '\n'), "return");
		register(new WindowsKey(MAIN, "pause", 0x13), "break");
		register(new WindowsKey(MAIN, "capslock", 0x14));
		
		register(new WindowsKey(MAIN, "kana", 0x15), "hangul", "hanguel", "kanalock");
		register(new WindowsKey(MAIN, "junja", 0x17));
		register(new WindowsKey(MAIN, "final", 0x18));
		register(new WindowsKey(MAIN, "hanja", 0x19), "kanji");
		register(new WindowsKey(MAIN, "escape", 0x1b), "esc");
		register(new WindowsKey(MAIN, "convert", 0x1c));
		register(new WindowsKey(MAIN, "nonconvert", 0x1d));
		register(new WindowsKey(MAIN, "accept", 0x1e));
		register(new WindowsKey(MAIN, "modechange", 0x1f));
		
		register(new WindowsKey(MAIN, "space", 0x20, ' '), "spacebar");
		register(new WindowsKey(MAIN, "pageup", 0x21), "pgup");
		register(new WindowsKey(MAIN, "pagedown", 0x22), "pgdn");
		register(new WindowsKey(MAIN, "end", 0x23));
		register(new WindowsKey(MAIN, "home", 0x24));
		register(new WindowsKey(MAIN, "left", 0x25));
		register(new WindowsKey(MAIN, "up", 0x26));
		register(new WindowsKey(MAIN, "right", 0x27));
		register(new WindowsKey(MAIN, "down", 0x28));
		register(new WindowsKey(MAIN, "select", 0x29));
		register(new WindowsKey(MAIN, "print", 0x2a));
		register(new WindowsKey(MAIN, "execute", 0x2b));
		register(new WindowsKey(MAIN, "printscreen", 0x2c), "printscrn", "prtsc", "systemrequest", "sysreq", "sysrq");
		register(new WindowsKey(MAIN, "insert", 0x2d), "ins");
		register(new WindowsKey(MAIN, "delete", 0x2e), "del");
		register(new WindowsKey(MAIN, "help", 0x2f));
		
		register(new WindowsKey(MAIN, "sleep", 0x5f));
		
		register(new WindowsKey(KEYPAD, "*", 0x6a, '*'));
		register(new WindowsKey(KEYPAD, "+", 0x6b, '+'));
		register(new WindowsKey(KEYPAD, ",", 0x6c, ','));
		register(new WindowsKey(KEYPAD, "-", 0x6d, '-'));
		register(new WindowsKey(KEYPAD, ".", 0x6e, '.'));
		register(new WindowsKey(KEYPAD, "/", 0x6f, '/'));
		
		register(new WindowsKey(KEYPAD, "numlock", 0x90));
		register(new WindowsKey(MAIN, "scrolllock", 0x91));
		
		register(new WindowsKey(MAIN, "back", 0xa6));
		register(new WindowsKey(MAIN, "forward", 0xa7));
		register(new WindowsKey(MAIN, "refresh", 0xa8));
		register(new WindowsKey(MAIN, "stop", 0xa9));
		register(new WindowsKey(MAIN, "search", 0xaa));
		register(new WindowsKey(MAIN, "favorites", 0xab));
		register(new WindowsKey(MAIN, "homepage", 0xac));
		
		register(new WindowsKey(MAIN, "`", 0xc0, '`'));
		
		register(new WindowsKey(LEFT, "shift", 0xa0));
		register(new WindowsKey(RIGHT, "shift", 0xa1));
		register(new WindowsKey(LEFT, "control", 0xa2), "ctrl");
		register(new WindowsKey(RIGHT, "control", 0xa3), "ctrl");
		register(new WindowsKey(LEFT, "alt", 0xa4));
		register(new WindowsKey(RIGHT, "alt", 0xa5));
		register(new WindowsKey(LEFT, "windows", 0x5b), "window");
		register(new WindowsKey(RIGHT, "windows", 0x5c), "window");
		register(new WindowsKey(MAIN, "application", 0x5d), "apps", "app");
		
		register(new WindowsKey(MAIN, ";", 0xba, ';'));
		register(new WindowsKey(MAIN, "=", 0xbb, '='));
		register(new WindowsKey(MAIN, ",", 0xbc, ','));
		register(new WindowsKey(MAIN, "-", 0xbd, '-'));
		register(new WindowsKey(MAIN, ".", 0xbe, '.'));
		register(new WindowsKey(MAIN, "/", 0xbf, '/'));
		register(new WindowsKey(MAIN, "[", 0xdb, '['));
		register(new WindowsKey(MAIN, "\\", 0xdc, '\\'));
		register(new WindowsKey(MAIN, "]", 0xdd, ']'));
		register(new WindowsKey(MAIN, "'", 0xde, '\''));
		
		locks = new WindowsKeyboardLocks(this);
	}
	
	private void register(WindowsKey key, String... otherNames)
	{
		KeyPosition position = key.getPosition();
		String positionName = position.getName().toLowerCase(Locale.ENGLISH);
		positionMap.put(positionName, position);

		namePositionTable.put(key.getName().toLowerCase(Locale.ENGLISH), positionName, key);
		nameMap.put(key.getName().toLowerCase(Locale.ENGLISH), key);

		//Allow user to specify code as key, but only if key with this name doesn't exist already (e.g. '5' key should not be overridden with key with code 5)
		if (!namePositionTable.containsRow(String.valueOf(key.getCode())))
			namePositionTable.put(String.valueOf(key.getCode()), positionName, key);
		if (!nameMap.containsKey(String.valueOf(key.getCode())))
			nameMap.put(String.valueOf(key.getCode()), key);
		
		codeMap.put(key.getCode(), key);
		if (key.isExtended())
			extendedCodeMap.put(key.getCode(), key);
		
		for (String otherName : otherNames)
		{
			otherName = otherName.toLowerCase(Locale.ENGLISH);
			namePositionTable.put(otherName, position.getName(), key);
			nameMap.put(otherName, key);
		}
	}
	
	@Override
	public Set<? extends Key> getAll(KeyPosition position, String name)
	{		
		name = name.toLowerCase(Locale.ENGLISH);
		
		if (position == null)
		{
			List<String> tokens = Splitter.on(CharMatcher.WHITESPACE).omitEmptyStrings().splitToList(name);
			if (tokens.size() == 2)
			{
				position = getPosition(tokens.get(0));
				name = tokens.get(1).toLowerCase(Locale.ENGLISH);
			}
		}
		
		if (position == null)
			return(nameMap.get(name));
		else
		{
			Key key = namePositionTable.get(name, position.getName());
			if (key == null)
				return(Collections.emptySet());
			else
				return(Collections.singleton(key));
		}
	}
	
	public KeyPosition getPosition(String name)
	{
		return(positionMap.get(name.toLowerCase(Locale.ENGLISH)));
	}
	
	@Override
	public Key forCode(int code)
	{
		return(forCode(code, false));
	}
	
	public Key forCode(int code, boolean extended)
	{
		Key key = null;
		if (extended)
			key = extendedCodeMap.get(code);
		if (key == null)
			key = codeMap.get(code);
		if (key == null)
			key = new WindowsKey(KeyPosition.MAIN, "#" + code, code);
		
		return(key);
	}
	
	@Override
	public KeyboardState getCurrentState()
	{
		//256 calls, yuck, but what's the alternative?
		ImmutableSet.Builder<Key> keySetBuilder = ImmutableSet.builder();		
		
		//Ignore first 7 items because those codes are for mouse buttons
		//Ignore 0x10, 0x11, 0x12 since on Windows they are the common (non-positioned) shift, control and alt keys
		//and we handle thier states through the left/right positioned codes anyway
		for (int i = 0x08; i < 0x10; i++)
		{
			int result = user32.GetAsyncKeyState(i);
			boolean keyDown = ((result & 32768) != 0);
			if (keyDown)
				keySetBuilder.add(forCode(i));
		}
		for (int i = 0x13; i < 255; i++) //intentionally end on 254
		{
			int result = user32.GetAsyncKeyState(i);
			boolean keyDown = ((result & 32768) != 0);
			if (keyDown)
				keySetBuilder.add(forCode(i));
		}


		return(new KeyboardStateSnapshot(this, keySetBuilder.build()));
	}
	
	@Override
	public KeyboardControl getControl()
	{
		return(control);
	}
	
	@Override
	public KeyboardLocks getLocks()
	{
		return(locks);
	}
}
