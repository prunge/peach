package au.net.causal.peach.windows;

import java.util.Objects;

import au.net.causal.peach.AbstractKeyboardControl;
import au.net.causal.peach.Key;
import au.net.causal.peach.Keyboard;

import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinDef.WORD;
import com.sun.jna.platform.win32.WinUser.INPUT;
import com.sun.jna.platform.win32.WinUser.KEYBDINPUT;

import static au.net.causal.peach.windows.WindowsBase.*;

public class WindowsKeyboardControl extends AbstractKeyboardControl
{
	public WindowsKeyboardControl(Keyboard keyboard)
	{
		super(keyboard);
	}
	
	@Override
	public void press(Key key)
	{
		Objects.requireNonNull(key, "key == null");

		INPUT input = createInput(key, false);
		DWORD result = user32.SendInput(new DWORD(1), new INPUT[] {input}, input.size());
		
		if (result.intValue() == 0)
			throw createExceptionFromLastError();
	}

	@Override
	public void release(Key key)
	{
		Objects.requireNonNull(key, "key == null");
		
		INPUT input = createInput(key, true);
		DWORD result = user32.SendInput(new DWORD(1), new INPUT[] {input}, input.size());
		
		if (result.intValue() == 0)
			throw createExceptionFromLastError();
	}
	
	private INPUT createInput(Key key, boolean up)
	{
		int flags = 0;
		if (key instanceof WindowsKey && ((WindowsKey)key).isExtended())
			flags |= KEYBDINPUT.KEYEVENTF_EXTENDEDKEY;
		if (up)
			flags |= KEYBDINPUT.KEYEVENTF_KEYUP;
		
		INPUT input = new INPUT();
		input.type = new DWORD(INPUT.INPUT_KEYBOARD);
		input.input.setType("ki");
		input.input.ki = new KEYBDINPUT();
		input.input.ki.wVk = new WORD(key.getCode());
		input.input.ki.wScan = new WORD(0L);
		input.input.ki.time = new DWORD(0L);
		input.input.ki.dwFlags = new DWORD(flags);
		
		return(input);
	}
}
