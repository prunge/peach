package au.net.causal.peach.windows;

import java.util.Objects;

import javax.annotation.Nonnull;

import au.net.causal.peach.Key;
import au.net.causal.peach.Keyboard;
import au.net.causal.peach.KeyboardControl;
import au.net.causal.peach.KeyboardLocks;
import static au.net.causal.peach.windows.WindowsBase.*;

public class WindowsKeyboardLocks implements KeyboardLocks
{
	private final @Nonnull KeyboardControl control;
	
	private final @Nonnull Key numLockKey;
	private final @Nonnull Key capsLockKey;
	private final @Nonnull Key scrollLockKey;
	private final @Nonnull Key kanaLockKey;
	
	public WindowsKeyboardLocks(@Nonnull Keyboard keyboard)
	{
		Objects.requireNonNull(keyboard, "keyboard == null");
		this.control = keyboard.getControl();
		
		numLockKey = keyboard.getAny("numlock");
		capsLockKey = keyboard.getAny("capslock");
		scrollLockKey = keyboard.getAny("scrolllock");
		kanaLockKey = keyboard.getAny("kana");
	}
	
	@Override
	public boolean isNumLock()
	{
		return(keyStatus(WinUserX.VK_NUMLOCK));
	}
	
	@Override
	public boolean isCapsLock()
	{
		return(keyStatus(WinUserX.VK_CAPITAL));
	}
	
	@Override
	public boolean isScrollLock()
	{
		return(keyStatus(WinUserX.VK_SCROLL));
	}
	
	@Override
	public boolean isKanaLock()
	{
		return(keyStatus(WinUserX.VK_KANA));
	}
	
	@Override
	public void setCapsLock(boolean capsLock)
	{
		if (capsLock != isCapsLock())
			toggleCapsLock();
	}
	
	@Override
	public void setKanaLock(boolean kanaLock)
	{
		if (kanaLock != isKanaLock())
			toggleKanaLock();
	}
	
	@Override
	public void setNumLock(boolean numLock)
	{
		if (numLock != isNumLock())
			toggleNumLock();
	}
	
	@Override
	public void setScrollLock(boolean scrollLock)
	{
		if (scrollLock != isScrollLock())
			toggleScrollLock();
	}

	@Override
	public void toggleCapsLock()
	{
		control.type(capsLockKey);
	}
	
	@Override
	public void toggleKanaLock()
	{
		control.type(kanaLockKey);
	}
	
	@Override
	public void toggleNumLock()
	{
		control.type(numLockKey);
	}
	
	@Override
	public void toggleScrollLock()
	{
		control.type(scrollLockKey);
	}
	
	private boolean keyStatus(int code)
	{
		return((user32.GetKeyState(code) & 0x01) != 0);
	}
}
