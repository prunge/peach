package au.net.causal.peach.windows;

import java.awt.AWTException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import au.net.causal.peach.Mouse;
import au.net.causal.peach.MouseControl;
import au.net.causal.peach.MouseState;
import au.net.causal.peach.MouseStateSnapshot;
import au.net.causal.peach.RobotMouseControl;
import au.net.causal.peach.event.MouseButton;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import static au.net.causal.peach.windows.WindowsBase.*;

public class WindowsMouse implements Mouse
{
	private final MouseControl control;
	
	private final Map<String, ? extends MouseButton> namedButtons;
	private final Set<? extends MouseButton> buttons;
	
	public WindowsMouse()
	{
		try
		{
			this.control = new RobotMouseControl(this);
		}
		catch (AWTException e)
		{
			throw new RuntimeException("Error creating mouse control: " + e, e);
		}
		
		ImmutableMap.Builder<String, MouseButton> namedButtonBuilder = ImmutableMap.builder();
		
		registerButton(namedButtonBuilder, MouseButton.LEFT);
		registerButton(namedButtonBuilder, MouseButton.MIDDLE);
		registerButton(namedButtonBuilder, MouseButton.RIGHT);
		registerButton(namedButtonBuilder, new MouseButton(4, "x1"));
		registerButton(namedButtonBuilder, new MouseButton(5, "x2"));
		
		namedButtons = namedButtonBuilder.build();
		buttons = ImmutableSet.copyOf(namedButtons.values());
	}
	
	private void registerButton(ImmutableMap.Builder<String, MouseButton> b, MouseButton button)
	{
		b.put(button.getName().toLowerCase(Locale.ENGLISH), button);
		b.put(String.valueOf(button.getId()), button);
	}
	
	@Override
	public MouseState getCurrentState()
	{
		Point position = MouseInfo.getPointerInfo().getLocation();
		
		ImmutableSet.Builder<MouseButton> sb = ImmutableSet.builder();
		
		if ((user32.GetAsyncKeyState(WinUserX.VK_LBUTTON) & 32768) != 0)
			sb.add(MouseButton.LEFT);
		if ((user32.GetAsyncKeyState(WinUserX.VK_RBUTTON) & 32768) != 0)
			sb.add(MouseButton.RIGHT);
		if ((user32.GetAsyncKeyState(WinUserX.VK_MBUTTON) & 32768) != 0)
			sb.add(MouseButton.MIDDLE);
		if ((user32.GetAsyncKeyState(WinUserX.VK_XBUTTON1) & 32768) != 0)
			sb.add(new MouseButton(4));
		if ((user32.GetAsyncKeyState(WinUserX.VK_XBUTTON2) & 32768) != 0)
			sb.add(new MouseButton(5));
		
		return(new MouseStateSnapshot(this, sb.build(), position));
	}
	
	@Override
	public MouseControl getControl()
	{
		return(control);
	}
	
	@Override
	public Set<? extends MouseButton> getButtons()
	{
		return(buttons);
	}
	
	@Override
	public MouseButton button(String name)
	{
		Objects.requireNonNull(name, "name == null");
		return(namedButtons.get(name.toLowerCase(Locale.ENGLISH)));
	}
	
}
