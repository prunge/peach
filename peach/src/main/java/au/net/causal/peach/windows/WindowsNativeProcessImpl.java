package au.net.causal.peach.windows;

import java.nio.file.Path;
import java.nio.file.Paths;

import com.sun.jna.platform.win32.WinNT.HANDLE;

import static au.net.causal.peach.windows.WindowsBase.*;

public class WindowsNativeProcessImpl implements WindowsNativeProcess
{	
	private final int processId;
	
	public WindowsNativeProcessImpl(int processId)
	{
		this.processId = processId;
	}
	
	@Override
	public Long getId()
	{
		return(Long.valueOf(processId));
	}

	@Override
	public Path getExecutable()
	{
		HANDLE processHandle = kernel32.OpenProcess(PsApi.PROCESS_QUERY_INFORMATION | PsApi.PROCESS_QUERY_LIMITED_INFORMATION | PsApi.PROCESS_VM_READ, false, processId);
		if (processHandle == null)
			throw createExceptionFromLastError();
		
		try
		{
			char[] buf = new char[65536];
			int charCount = psApi.GetModuleFileNameEx(processHandle, null, buf, buf.length);
			if (charCount < 0)
				throw createExceptionFromLastError();
			
			String s = new String(buf, 0, charCount);
			Path path = Paths.get(s);
			return(path);
		}
		finally
		{
			kernel32.CloseHandle(processHandle);
		}
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + processId;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WindowsNativeProcessImpl other = (WindowsNativeProcessImpl)obj;
		if (processId != other.processId)
			return false;
		return true;
	}
	
	@Override
	public String toString()
	{
		return("WindowsNativeProcessImpl[processId=" + processId + "]");
	}
}
