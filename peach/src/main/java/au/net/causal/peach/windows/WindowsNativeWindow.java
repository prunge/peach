package au.net.causal.peach.windows;

import au.net.causal.peach.NativeWindow;

import com.sun.jna.platform.win32.WinDef.HWND;

public interface WindowsNativeWindow extends NativeWindow
{
	public HWND getHwnd();

}