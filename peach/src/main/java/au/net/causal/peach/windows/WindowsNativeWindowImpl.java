package au.net.causal.peach.windows;

import java.awt.Rectangle;
import java.util.Objects;

import au.net.causal.peach.NativeWindowState;

import com.sun.jna.platform.win32.WinUser;
import com.sun.jna.platform.win32.WinDef.HWND;
import com.sun.jna.platform.win32.WinDef.RECT;
import com.sun.jna.ptr.IntByReference;

import static au.net.causal.peach.windows.WindowsBase.*;

public class WindowsNativeWindowImpl implements WindowsNativeWindow
{
	private final HWND hwnd;
	
	public WindowsNativeWindowImpl(HWND hwnd)
	{
		if (hwnd == null)
			throw new NullPointerException("hwnd == null");
		
		this.hwnd = hwnd;
	}
	
	@Override
	public HWND getHwnd()
	{
		return(hwnd);
	}
	
	@Override
	public String getTitle()
	{
		int length = user32.GetWindowTextLength(hwnd);
		if (length < 0)
			throw createExceptionFromLastError();
		
		char[] buf = new char[length + 1];
		int actualLength = user32.GetWindowText(hwnd, buf, buf.length);
		if (actualLength >= 0)
			return(new String(buf, 0, actualLength));
		else
			throw createExceptionFromLastError();
	}
	
	public void setTitle(String title)
	{
		if (title == null)
			throw new NullPointerException("title == null");
		
		boolean ok = user32.SetWindowText(hwnd, title);
		if (!ok)
			throw createExceptionFromLastError();
	}
	
	@Override
	public Rectangle getBounds()
	{
		RECT rect = new RECT();
		boolean ok = user32.GetWindowRect(hwnd, rect);
		if (!ok)
			throw createExceptionFromLastError();
		
		int x = rect.left;
		int y = rect.top;
		int width = rect.right - rect.left;
		int height = rect.bottom - rect.top;
		
		return(new Rectangle(x, y, width, height));
	}
	
	public void setBounds(Rectangle bounds)
	{
		if (bounds == null)
			throw new NullPointerException("bounds == null");
		
		boolean ok = user32.MoveWindow(hwnd, bounds.x, bounds.y, bounds.width, bounds.height, true);
		if (!ok)
			throw createExceptionFromLastError();
	}
	
	@Override
	public WindowsNativeProcess getProcess()
	{
		IntByReference processIdHolder = new IntByReference();
		int result = user32.GetWindowThreadProcessId(hwnd, processIdHolder);
		if (result == 0)
			throw createExceptionFromLastError();
		
		return(new WindowsNativeProcessImpl(processIdHolder.getValue()));
	}
	
	@Override
	public NativeWindowState getState()
	{
		if (user32.IsIconic(hwnd))
			return(NativeWindowState.MINIMIZED);
		if (user32.IsZoomed(hwnd))
			return(NativeWindowState.MAXIMIZED);
		
		return(NativeWindowState.NORMAL);
	}
	
	@Override
	public void setState(NativeWindowState state)
	{
		Objects.requireNonNull(state, "state == null");
		
		int nCmdShow;
		switch (state)
		{
			case MINIMIZED:
				nCmdShow = WinUser.SW_SHOWMINNOACTIVE;
				break;
			case MAXIMIZED:
				nCmdShow = WinUser.SW_MAXIMIZE;
				break;
			case NORMAL:
				nCmdShow = WinUser.SW_SHOWNOACTIVATE;
				break;
			default:
				throw new Error("Unsupported window state: " + state);
		}
		user32.ShowWindow(hwnd, nCmdShow);
	}
	
	
	@Override
	public String toString()
	{
		return("WindowsNativeWindow [hwnd=" + hwnd + "]");
	}

	@Override
	public int hashCode()
	{
		return(hwnd.hashCode());
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WindowsNativeWindowImpl other = (WindowsNativeWindowImpl)obj;
		if (!hwnd.equals(other.hwnd))
			return false;
		return true;
	}
	
	
	
}
