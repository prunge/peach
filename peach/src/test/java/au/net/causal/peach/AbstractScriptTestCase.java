package au.net.causal.peach;

import java.io.IOError;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import jdk.nashorn.api.scripting.URLReader;

import org.junit.Before;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Tests that execute javascript.
 */
@SuppressWarnings("restriction")
public abstract class AbstractScriptTestCase
{
	private ScriptEngine engine;
	
	@Before
	public void setUpScripting()
	{
		engine = new ScriptEngineManager(AbstractScriptTestCase.class.getClassLoader()).getEngineByExtension("js");
	}
	
	protected void putScriptVariable(String name, Object value)
	{
		engine.getBindings(ScriptContext.GLOBAL_SCOPE).put(name, value);
	}
	
	protected void runScriptCode(String code)
	{
		try
		{
			engine.eval(code);
		}
		catch (ScriptException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	protected void runScriptFile(String resourceName)
	{
		@SuppressFBWarnings
		URL t1 = getClass().getResource(resourceName);
		try
		{
			try (URLReader r = new URLReader(t1, StandardCharsets.UTF_8))
			{
				engine.eval(r);
			}
		}
		catch (IOException e)
		{
			throw new IOError(e);
		}
		catch (ScriptException e)
		{
			throw new RuntimeException(e);
		}
	}
}
