package au.net.causal.peach;

import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.script.ScriptEngine;

import jdk.nashorn.api.scripting.URLReader;

import org.junit.Ignore;
import org.junit.Test;

@SuppressWarnings("restriction")
public class TestEventHooks
{
	private Peach peach = new Peach();
	
	@Ignore("only for user-interactive testing")
	@Test
	public void testMouse()
	throws Exception
	{
		ScriptEngine jsEngine = peach.createJavascriptEngine();

		URL t1 = TestEventHooks.class.getResource("e1.js");
		try (URLReader r = new URLReader(t1, StandardCharsets.UTF_8))
		{
			jsEngine.eval(r);
		}
		
		Thread.sleep(10000L);
	}
	
	//@Ignore("only for user-interactive testing")
	@Test	
	public void testKeyboard()
	throws Exception
	{
		ScriptEngine jsEngine = peach.createJavascriptEngine();

		URL t1 = TestEventHooks.class.getResource("e2.js");
		try (URLReader r = new URLReader(t1, StandardCharsets.UTF_8))
		{
			jsEngine.eval(r);
		}
		
		Thread.sleep(10000L);
	}
}
