package au.net.causal.peach;

import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.script.ScriptEngine;

import jdk.nashorn.api.scripting.URLReader;

import org.junit.Test;

@SuppressWarnings("restriction")
public class TestPeachEngine
{
	private Peach peach = new Peach();
	
	@Test	
	public void test()
	throws Exception
	{
		ScriptEngine jsEngine = peach.createJavascriptEngine();

		URL t1 = TestPeachEngine.class.getResource("t1.js");
		try (Reader r = new URLReader(t1, StandardCharsets.UTF_8))
		{
			jsEngine.eval(r);
		}
	}
}
