package au.net.causal.peach.event;

import java.beans.ConstructorProperties;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import au.net.causal.peach.support.RealObjectMaker;

import static org.junit.Assert.*;

public class TestRealObjectMaker
{
	@Test
	public void testPropertySetters()
	{
		Galah galah = RealObjectMaker.make(Collections.singletonMap("name", "Squinky"), Galah.class);
		assertEquals("Wrong name.", "Squinky", galah.getName());
	}
	
	@Test
	public void testConstructorProperties()
	{
		Cockatoo cockatoo = RealObjectMaker.make(Collections.singletonMap("name", "Whiteman"), Cockatoo.class);
		assertEquals("Wrong name.", "Whiteman", cockatoo.getName());
	}
	
	@Test
	public void testConstructorParametersByName()
	{
		Parrot parrot = RealObjectMaker.make(Collections.singletonMap("name", "Mcfly"), Parrot.class);
		assertEquals("Wrong name.", "Mcfly", parrot.getName());
	}
	
	@Test
	public void testArraySupport()
	{
		Map<String, Object> map = new HashMap<>();
		map.put("name", "Fluffy");
		map.put("nestLocations", new String[] {"one", "two"});
		
		Magpie magpie = RealObjectMaker.make(map, Magpie.class);
		assertEquals("Wrong name.", "Fluffy", magpie.getName());
		assertArrayEquals("Wrong nest locations.", new String[] {"one", "two"}, magpie.getNestLocations());
	}
	
	@Test
	public void testCollectionSupport()
	{
		Map<String, Object> map = new HashMap<>();
		map.put("name", "Fluffy");
		map.put("nestLocations", new String[] {"one", "two"});
		
		Magpie2 magpie = RealObjectMaker.make(map, Magpie2.class);
		assertEquals("Wrong name.", "Fluffy", magpie.getName());
		assertEquals("Wrong nest locations.", Arrays.asList("one", "two"), magpie.getNestLocations());
	}
	
	//TODO also need tests from scripts
	
	public static class Cockatoo
	{
		private final String name;
		
		@ConstructorProperties({"name"})
		public Cockatoo(String name)
		{
			this.name = name;
		}
		
		public String getName()
		{
			return(name);
		}
	}

	public static class Galah
	{
		private String name;

		public String getName()
		{
			return(name);
		}

		public void setName(String name)
		{
			this.name = name;
		}
	}
	
	public static class Parrot
	{
		private final String name;
		
		public Parrot(String name)
		{
			this.name = name;
		}
		
		public String getName()
		{
			return(name);
		}
	}
	
	public static class Magpie
	{
		private final String name;
		private final String[] nestLocations;
		
		@ConstructorProperties({"name", "nestLocations"})
		public Magpie(String name, String[] nestLocations)
		{
			this.name = name;
			this.nestLocations = nestLocations;
		}
		
		public String getName()
		{
			return(name);
		}
		
		public String[] getNestLocations()
		{
			return(nestLocations);
		}
	}
	
	public static class Magpie2
	{
		private final String name;
		private final List<String> nestLocations;
		
		@ConstructorProperties({"name", "nestLocations"})
		public Magpie2(String name, List<String> nestLocations)
		{
			this.name = name;
			this.nestLocations = nestLocations;
		}
		
		public String getName()
		{
			return(name);
		}
		
		public List<String> getNestLocations()
		{
			return(nestLocations);
		}
	}
}
