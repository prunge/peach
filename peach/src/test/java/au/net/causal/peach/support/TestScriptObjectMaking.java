package au.net.causal.peach.support;

import java.beans.ConstructorProperties;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jdk.internal.dynalink.beans.StaticClass;

import org.junit.Test;

import au.net.causal.peach.AbstractScriptTestCase;

import static org.junit.Assert.*;

/**
 * Execute scripts to give the object maker support classes some testing.
 * <p>
 * 
 * The goal here is to test whether script objects, especially those nested within arrays and nested structures can be converted successfully.
 */
@SuppressWarnings("restriction")
public class TestScriptObjectMaking extends AbstractScriptTestCase
{
	@Test
	public void testStringProperty()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Squinky'});");
		
		Bird bird = RealObjectMaker.make(cb.getParameter(), Bird.class);
		assertEquals("Wrong name.", "Squinky", bird.getName());
	}
	
	@Test
	public void testStringNotSetProperty()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({});");
		
		Bird bird = RealObjectMaker.make(cb.getParameter(), Bird.class);
		assertNull("Wrong name.", bird.getName());
	}
	
	@Test
	public void testPrimitiveIntProperty()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({age: 10, name: 'Squinky'});");
		
		Galah bird = RealObjectMaker.make(cb.getParameter(), Galah.class);
		assertEquals("Wrong name.", "Squinky", bird.getName());
		assertEquals("Wrong age.", 10, bird.getAge());
	}
	
	@Test
	public void testPrimitiveIntPropertyDefaultsWhenNotSpecified()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Squinky'});");
		
		Galah bird = RealObjectMaker.make(cb.getParameter(), Galah.class);
		assertEquals("Wrong name.", "Squinky", bird.getName());
		assertEquals("Wrong age.", 0, bird.getAge());
	}
	
	@Test
	public void testWrappedIntProperty()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({age: 10, name: 'Squinky'});");
		
		Galah2 bird = RealObjectMaker.make(cb.getParameter(), Galah2.class);
		assertEquals("Wrong name.", "Squinky", bird.getName());
		assertEquals("Wrong age.", Integer.valueOf(10), bird.getAge());
	}
	
	@Test
	public void testWrappedIntPropertyDefaultsWhenNotSpecified()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Squinky'});");
		
		Galah2 bird = RealObjectMaker.make(cb.getParameter(), Galah2.class);
		assertEquals("Wrong name.", "Squinky", bird.getName());
		assertEquals("Wrong age.", null, bird.getAge());
	}
	
	@Test
	public void testArrayProperty()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Whiteman', otherNames: ['Whitey', 'Noisy']});");
		
		Cockatoo bird = RealObjectMaker.make(cb.getParameter(), Cockatoo.class);
		assertEquals("Wrong name.", "Whiteman", bird.getName());
		assertArrayEquals("Wrong other names.", new String[] {"Whitey", "Noisy"}, bird.getOtherNames());
	}
	
	@Test
	public void testArrayPropertyNotSpecified()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Whiteman'});");
		
		Cockatoo bird = RealObjectMaker.make(cb.getParameter(), Cockatoo.class);
		assertEquals("Wrong name.", "Whiteman", bird.getName());
		assertNull("Wrong other names.", bird.getOtherNames());
	}
	
	@Test
	public void testSingleElementArrayProperty()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Whiteman', otherNames: 'Whitey'});");
		
		Cockatoo bird = RealObjectMaker.make(cb.getParameter(), Cockatoo.class);
		assertEquals("Wrong name.", "Whiteman", bird.getName());
		assertArrayEquals("Wrong other names.", new String[] {"Whitey"}, bird.getOtherNames());
	}
	
	@Test
	public void testListProperty()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Dozey', otherNames: ['Flapsy', 'Dozer']});");
		
		Pelican bird = RealObjectMaker.make(cb.getParameter(), Pelican.class);
		assertEquals("Wrong name.", "Dozey", bird.getName());
		assertEquals("Wrong other names.", Arrays.asList("Flapsy", "Dozer"), bird.getOtherNames());
	}
	
	@Test
	public void testListPropertyNotSpecified()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Dozey'});");
		
		Pelican bird = RealObjectMaker.make(cb.getParameter(), Pelican.class);
		assertEquals("Wrong name.", "Dozey", bird.getName());
		assertNull("Wrong other names.", bird.getOtherNames());
	}
	
	@Test
	public void testSingleElementListProperty()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Dozey', otherNames: 'Dozer'});");
		
		Pelican bird = RealObjectMaker.make(cb.getParameter(), Pelican.class);
		assertEquals("Wrong name.", "Dozey", bird.getName());
		assertEquals("Wrong other names.", Arrays.asList("Dozer"), bird.getOtherNames());
	}
	
	@Test
	public void testListPropertyOtherType()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Dozey', wingSize: [30, 31]});");
		
		Pelican2 bird = RealObjectMaker.make(cb.getParameter(), Pelican2.class);
		assertEquals("Wrong name.", "Dozey", bird.getName());
		assertEquals("Wrong other names.", Arrays.asList(30, 31), bird.getOtherNames());
	}
	
	@Test
	public void testListPropertyNumericTypeConversion()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Dozey', wingSize: [30.0, 31.0]});");
		
		Pelican2 bird = RealObjectMaker.make(cb.getParameter(), Pelican2.class);
		assertEquals("Wrong name.", "Dozey", bird.getName());
		assertEquals("Wrong other names.", Arrays.asList(30, 31), bird.getOtherNames());
	}
	
	@Test
	public void testSingleAlias()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Fluffy', years: 2});");
		
		Magpie bird = RealObjectMaker.make(cb.getParameter(), Magpie.class);
		assertEquals("Wrong name.", "Fluffy", bird.getName());
		assertEquals("Wrong age.", 2, bird.getAge());
	}
	
	@Test
	public void testUseOriginalPropertyWithAlias()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Fluffy', age: 2});");
		
		Magpie bird = RealObjectMaker.make(cb.getParameter(), Magpie.class);
		assertEquals("Wrong name.", "Fluffy", bird.getName());
		assertEquals("Wrong age.", 2, bird.getAge());
	}
	
	@Test
	public void testDontUsePropertyWithAlias()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Fluffy'});");
		
		Magpie bird = RealObjectMaker.make(cb.getParameter(), Magpie.class);
		assertEquals("Wrong name.", "Fluffy", bird.getName());
		assertEquals("Wrong age.", 0, bird.getAge());
	}
	
	@Test
	public void testMultiAlias()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Fluffy', numberOfYears: 2});");
		
		Magpie2 bird = RealObjectMaker.make(cb.getParameter(), Magpie2.class);
		assertEquals("Wrong name.", "Fluffy", bird.getName());
		assertEquals("Wrong age.", 2, bird.getAge());
	}
	
	@Test
	public void testConstantExplicitlySpecified()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		putScriptVariable("BeakType", StaticClass.forClass(BeakType.class));
		runScriptCode("cb.call({name: 'Miller', beakType: BeakType.CURVED});");
		
		Parrot bird = RealObjectMaker.make(cb.getParameter(), Parrot.class);
		assertEquals("Wrong name.", "Miller", bird.getName());
		assertEquals("Wrong beak type.", BeakType.CURVED, bird.getBeakType());
	}
	
	@Test
	public void testConstantByName()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Miller', beakType: 'CURVED'});");
		
		Parrot bird = RealObjectMaker.make(cb.getParameter(), Parrot.class);
		assertEquals("Wrong name.", "Miller", bird.getName());
		assertEquals("Wrong beak type.", BeakType.CURVED, bird.getBeakType());
	}
	
	@Test
	public void testConstantByNameDifferentCase()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Miller', beakType: 'straight'});");
		
		Parrot bird = RealObjectMaker.make(cb.getParameter(), Parrot.class);
		assertEquals("Wrong name.", "Miller", bird.getName());
		assertEquals("Wrong beak type.", BeakType.STRAIGHT, bird.getBeakType());
	}
	
	@Test
	public void testCollectionOfConstantsByName()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Miller', beakTypes: ['Straight', 'Curved']});");
		
		Parrot2 bird = RealObjectMaker.make(cb.getParameter(), Parrot2.class);
		assertEquals("Wrong name.", "Miller", bird.getName());
		assertEquals("Wrong beak type.", new HashSet<>(Arrays.asList(BeakType.STRAIGHT, BeakType.CURVED)), bird.getBeakTypes());
	}
	
	@Test(expected=ClassCastException.class)
	public void testMisnamedConstant()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Miller', beakType: 'somethingelse'});");
		
		RealObjectMaker.make(cb.getParameter(), Parrot.class);
	}
	
	@Test
	public void testInvalidProperty()
	{
		Callback cb = new Callback();
		putScriptVariable("cb", cb);
		runScriptCode("cb.call({name: 'Fluffy', iDoNotExist: 'five'});");

		try
		{
			RealObjectMaker.make(cb.getParameter(), Magpie2.class);
			fail("Should have thrown " + NoSuchPropertyException.class.getCanonicalName());
		}
		catch (NoSuchPropertyException e)
		{
			assertEquals("Wrong target.", Magpie2.class, e.getTarget());
			assertEquals("Wrong properties.", Collections.singleton("iDoNotExist"), e.getProperties());
		}
	}
	
	public static class Callback
	{
		private Map<String, ?> parameter;
		
		public void call(Map<String, ?> parameter)
		{
			this.parameter = parameter;
		}
		
		public Map<String, ?> getParameter()
		{
			return(parameter);
		}
	}
	
	public static class Bird
	{
		private final String name;
		
		@ConstructorProperties("name")
		public Bird(String name)
		{
			this.name = name;
		}
		
		public String getName()
		{
			return(name);
		}
	}
	
	public static class Galah
	{
		private final String name;
		private final int age;
		
		@ConstructorProperties({"name", "age"})
		public Galah(String name, int age)
		{
			this.name = name;
			this.age = age;
		}
		
		public String getName()
		{
			return(name);
		}
		
		public int getAge()
		{
			return(age);
		}
	}

	public static class Galah2
	{
		private final String name;
		private final Integer age;
		
		@ConstructorProperties({"name", "age"})
		public Galah2(String name, Integer age)
		{
			this.name = name;
			this.age = age;
		}
		
		public String getName()
		{
			return(name);
		}
		
		public Integer getAge()
		{
			return(age);
		}
	}
	
	private static class Cockatoo
	{
		private final String name;
		private final String[] otherNames;
		
		@ConstructorProperties({"name", "otherNames"})
		public Cockatoo(String name, String[] otherNames)
		{
			this.name = name;
			this.otherNames = otherNames;
		}
		
		public String getName()
		{
			return(name);
		}
		
		public String[] getOtherNames()
		{
			return(otherNames);
		}
	}
	
	public static class Magpie
	{
		private final String name;
		private final int age;
		
		@ConstructorProperties({"name", "age"})
		public Magpie(String name, @Alias("years") int age)
		{
			this.name = name;
			this.age = age;
		}
		
		public String getName()
		{
			return(name);
		}
		
		public int getAge()
		{
			return(age);
		}
	}
	
	public static class Magpie2
	{
		private final String name;
		private final int age;
		
		@ConstructorProperties({"name", "age"})
		public Magpie2(String name, @Alias({"years", "numberOfYears"}) int age)
		{
			this.name = name;
			this.age = age;
		}
		
		public String getName()
		{
			return(name);
		}
		
		public int getAge()
		{
			return(age);
		}
	}
	
	public static class Pelican
	{
		private final String name;
		private final List<String> otherNames;
		
		@ConstructorProperties({"name", "otherNames"})
		public Pelican(String name, List<String> otherNames)
		{
			this.name = name;
			this.otherNames = otherNames;
		}
		
		public String getName()
		{
			return(name);
		}
		
		public List<String> getOtherNames()
		{
			return(otherNames);
		}
	}
	
	public static class Pelican2
	{
		private final String name;
		private final List<Integer> wingSize;
		
		@ConstructorProperties({"name", "wingSize"})
		public Pelican2(String name, List<Integer> wingSize)
		{
			this.name = name;
			this.wingSize = wingSize;
		}
		
		public String getName()
		{
			return(name);
		}
		
		public List<Integer> getOtherNames()
		{
			return(wingSize);
		}
	}
	
	public static class Parrot
	{
		private final String name;
		private final BeakType beakType;
		
		public Parrot(String name, BeakType beakType)
		{
			this.name = name;
			this.beakType = beakType;
		}

		public String getName()
		{
			return(name);
		}

		public BeakType getBeakType()
		{
			return(beakType);
		}
	}
	
	public static class Parrot2
	{
		private final String name;
		private final Set<BeakType> beakTypes;
		
		public Parrot2(String name, Set<BeakType> beakTypes)
		{
			this.name = name;
			this.beakTypes = beakTypes;
		}

		public String getName()
		{
			return(name);
		}

		public Set<BeakType> getBeakTypes()
		{
			return(beakTypes);
		}
	}
	
	public static class BeakType
	{
		public static final BeakType CURVED = new BeakType("curved");
		public static final BeakType STRAIGHT = new BeakType("straight");
		
		private final String name;
		
		public BeakType(String name)
		{
			this.name = name;
		}
		
		public String getName()
		{
			return(name);
		}

		@Override
		public int hashCode()
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			BeakType other = (BeakType)obj;
			if (name == null)
			{
				if (other.name != null)
					return false;
			}
			else if (!name.equals(other.name))
				return false;
			return true;
		}
	}
}
