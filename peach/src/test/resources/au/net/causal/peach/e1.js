print('Windows: ' + desktop.win32);

//desktop.on.click(new MouseEventConstraints(), function(ev)
desktop.on.mouseDown({buttons: [MouseButton.LEFT, MouseButton.MIDDLE]}, function(ev)
{
	//print("Event: " + ev);
	if (ev.button == MouseButton.LEFT)
		print("Left click: " + ev.x + "," + ev.y);
	else
		print("Other click: " + ev.x + "," + ev.y);
});

desktop.on.event("mouseDown", function(ev)
{
	if (ev.button == MouseButton.LEFT)
		print("!!Left click: " + ev.x + "," + ev.y);
	else
		print("!!Other click: " + ev.x + "," + ev.y + " " + ev.button);
});

desktop.on.mouseDown({button: 'middle'}, function(ev)
{
	print("Middle click (down): " + ev.x + "," + ev.y);
	ev.consume();
});

desktop.on.mouseUp({buttons: MouseButton.MIDDLE}, function(ev)
{
	print("Middle click (up): " + ev.x + "," + ev.y);
	ev.consume();
});

desktop.on.mouseDown({button: new MouseButton(4)}, function(ev)
{
	print("4th click (down): " + ev.x + "," + ev.y);
	if (ev.mouseState.isDown(5))
		print("5th is also down!");
	
	desktop.mouse.control.click("left");
});

desktop.on.mouseWheelMove(function(ev)
{
	print("Mouse wheel moved: " + ev.x + "," + ev.y + ": " + ev.steps + " (" + ev.pixels + ")");
	if (ev.steps < 0)
		ev.consume();
});
