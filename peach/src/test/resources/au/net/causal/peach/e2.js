desktop.on.keyDown(function(ev)
{
	print("Keydown: " + ev.key);
});

desktop.on.keyUp(function(ev)
{
	print("Keyup: " + ev.key);
});

//lots of work to get this working!
desktop.on.keyDown({modifier: 'shift', key: 'down'}, function(ev)
{
	if (ev.keyboardState.isDown('left control'))
		print("Down we go, with some control!");
	else
		print("Down we go!");
	
	print("... and the mouse position is (" + ev.mouseState.x + ", " + ev.mouseState.y + ")");
});

var toggle = false;

desktop.on.keyDown("clear", function(ev)
{
	print("numlock pressed");
	//var s = desktop.keyboard.getAny("1");
	//desktop.keyboard.control.type(s);
	
	toggle = !toggle;
	
	if (toggle)
		desktop.keyboard.control.typeCombo("shift", "1");
	else
		desktop.keyboard.control.typeCombo("1");
});

desktop.on.keyDown("f14", function(ev)
{
	print("home pressed");
	print("Caps lock: " + desktop.keyboard.locks.capsLock);
	desktop.keyboard.control.type("capslock");
	print("Caps lock: " + desktop.keyboard.locks.capsLock);
});

/*
desktop.on.keyDown("shift left", function(ev)
{
	print("Go way left!");
	print(ev.mouseState);
	
});



desktop.on.keyDown("numlock", function(ev)
{
	print("numlock pressed");
	var s = desktop.keyboard.getAny("1");
	desktop.keyboard.control.type(s);
});

desktop.on.keyDown("f12", function(ev)
{
	print("F12: " + desktop.keyboard.locks);
	var scroll = desktop.keyboard.locks.scrollLock;
	print("Scroll Lock: " + scroll);
	//desktop.keyboard.locks.scrollLock = !scroll;
});

desktop.on.keyDown("f9", function(ev)
{
	desktop.keyboard.locks.toggleScrollLock();
});
*/